import 'package:flutter/material.dart';

class EmptyListPlaceHolder extends StatelessWidget {
  const EmptyListPlaceHolder({Key key, this.text}) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Text(
        this.text,
        overflow: TextOverflow.clip,
        style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.black38),
      ),
    );
  }
}

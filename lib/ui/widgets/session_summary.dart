import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/Location.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:maps_launcher/maps_launcher.dart';

abstract class LocalConstants {
  static const double RING_STROKE_WIDTH = 32;
}

class SessionSummary extends StatelessWidget {
  final int sessionID;
  final List<Map<String, dynamic>> boulderList = [];

  SessionSummary(this.sessionID);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container(
              child: CircularProgressIndicator(),
            );
          }
          return _createInfoContainer(context, snapshot.data.first);
        });
  }

  Widget _createInfoContainer(BuildContext context, var session) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(Constants.STANDARD_PADDING),
          child: Wrap(runSpacing: Constants.STANDARD_PADDING, children: [
            _createSessionSummary(context, session),
            Divider(thickness: 2.0),
            Text("Ascends:"),
          ]),
        ),
        _createBoulderList(),
        SizedBox(height: Constants.STANDARD_PADDING),
      ],
    );
  }

  Widget _createSessionSummary(BuildContext context, var session) {
    Map<String, dynamic> bestAscend = _getBestAscend();
    return Align(
      alignment: Alignment.center,
      child: Column(children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
                flex: 3,
                fit: FlexFit.tight,
                child: InkWell(
                    onTap: () => _showLocationOnMap(context, session[DBController.sessionLocation]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Location:"),
                        Text(session[DBController.sessionLocation],
                            style: TextStyle(color: AppColors.color1), maxLines: 2, overflow: TextOverflow.ellipsis)
                      ],
                    ))),
            SizedBox(width: 30.0),
            Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Total Time:"),
                    Text(
                        session[DBController.sessionTimeEnd] == null
                            ? "---"
                            : _getDuration(
                                DateTime.parse(session[DBController.sessionTimeStart]),
                                DateTime.parse(session[DBController.sessionTimeEnd]),
                              ),
                        style: TextStyle(color: AppColors.color1))
                  ],
                ))
          ],
        ),
        SizedBox(height: 30),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
                flex: 3,
                fit: FlexFit.tight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Most difficult Boulder:"),
                    Text(Util.getBoulderName(bestAscend[DBController.name]),
                        style: TextStyle(color: AppColors.color1), overflow: TextOverflow.ellipsis),
                    bestAscend == null
                        ? Container()
                        : Text(
                            Util.getSummary(
                              bestAscend[DBController.gradeId],
                              bestAscend[DBController.styleId],
                              bestAscend[DBController.gradeSystem],
                            ),
                            style: TextStyle(
                              color: AppColors.color1,
                              fontSize: 14,
                            ),
                          ),
                  ],
                )),
            SizedBox(width: 24.0),
            Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Total Ascends:"),
                    Text(boulderList.length.toString().padLeft(2, '0'), style: TextStyle(color: AppColors.color1))
                  ],
                ))
          ],
        ),
        SizedBox(height: Constants.STANDARD_PADDING),
        Divider(thickness: 2.0),
        _createDonutChart(context),
        _createLegend(context),
      ]),
    );
  }

  Widget _createDonutChart(BuildContext context) {
    //TODO fix if no data
    int completed = 0;
    int flash = 0;
    for (var boulder in this.boulderList) {
      if (boulder[DBController.styleId] == 0) {
        flash++;
      }

      if (boulder[DBController.styleId] <= 1) {
        completed++;
      }
    }

    final Map<String, double> dataMap1 = {"Flash": flash.toDouble(), "1": (boulderList.length - flash).toDouble()};
    final Map<String, double> dataMap2 = {
      "completed": completed.toDouble(),
      "2": (boulderList.length - completed).toDouble()
    };

    if (completed <= 0) {}

    return Stack(alignment: Alignment.center, children: [
      _createPieChart(context, dataMap1, 600, [AppColors.color1, Colors.black12], 270, false, 0),
      _createPieChart(context, dataMap2, 800, [AppColors.color2, Colors.black12], 270, false,
          LocalConstants.RING_STROKE_WIDTH + 10.0),
      if (flash > 0)
        _createPieChart(context, {"1": dataMap1["Flash"]}, 600, [Colors.transparent], 135, true,
            -LocalConstants.RING_STROKE_WIDTH * 2.0),
      if (completed > 0)
        _createPieChart(context, {"1": dataMap2["completed"]}, 800, [Colors.transparent], 180, true, 0),
    ]);
  }

  Widget _createPieChart(BuildContext context, Map<String, double> dataMap, int duration, List<Color> colorList,
      double initialAngleInDegree, bool showChartValues, double offset) {
    return PieChart(
      dataMap: dataMap,
      animationDuration: Duration(milliseconds: duration),
      chartRadius: MediaQuery.of(context).size.width / 3.2 - offset,
      colorList: colorList,
      initialAngleInDegree: initialAngleInDegree,
      chartType: ChartType.ring,
      ringStrokeWidth: 16,
      legendOptions: LegendOptions(
        showLegends: false,
      ),
      chartValuesOptions: ChartValuesOptions(
        decimalPlaces: 0,
        showChartValueBackground: true,
        showChartValues: showChartValues,
        showChartValuesInPercentage: false,
        showChartValuesOutside: false,
      ),
    );
  }

  Widget _createLegend(BuildContext context) {
    final List<Color> colors = [AppColors.color1, AppColors.color2, AppColors.color3];
    final List<String> styles = ["FLASH", "COMPLETED"];
    return Wrap(
      direction: Axis.horizontal,
      spacing: 16,
      children: styles
          .map((item) => Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 2.0),
                    height: 12.0,
                    width: 12.0,
                    decoration: BoxDecoration(shape: BoxShape.circle, color: colors.elementAt(styles.indexOf(item))),
                  ),
                  SizedBox(width: 6.0),
                  Flexible(
                      fit: FlexFit.loose,
                      child: Opacity(
                          opacity: 0.75,
                          child: Text(item, style: Theme.of(context).textTheme.bodyText1, softWrap: true)))
                ],
              ))
          .toList(),
    );
  }

  _createBoulderList() {
    List<Widget> items = [];
    for (Map<String, dynamic> boulder in boulderList) {
      items.add(Padding(
          padding: EdgeInsets.only(
            left: Constants.STANDARD_PADDING,
            right: Constants.STANDARD_PADDING,
            bottom: 10.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Flexible(
                  child: Text(Util.getBoulderName(boulder[DBController.name]), overflow: TextOverflow.ellipsis),
                ),
                Util.getRating(boulder[DBController.rating]),
              ]),
              Text(
                Util.getSummary(
                  boulder[DBController.gradeId],
                  boulder[DBController.styleId],
                  boulder[DBController.gradeSystem],
                ),
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal),
              ),
            ],
          )));
    }
    return Column(
      children: items,
    );
  }

  void _showLocationOnMap(BuildContext context, String location) async {
    List<Map<String, dynamic>> result = await DBController().queryCoordinatesByLocation(location);
    if (result == null || result.isEmpty) {
      if (Location().saveLocation) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
              SnackBar(content: Text("No coordinates available for this location"), duration: Duration(seconds: 2)));
      }
    } else {
      MapsLauncher.launchCoordinates(
          result.first[DBController.latitude], result.first[DBController.longitude], location);
    }
  }

  Map<String, dynamic> _getBestAscend() {
    Map<String, dynamic> best = this.boulderList.first;
    for (Map<String, dynamic> boulder in this.boulderList) {
      if (boulder[DBController.gradeId] > best[DBController.gradeId]) {
        best = boulder;
      } else if (boulder[DBController.gradeId] == best[DBController.gradeId] &&
          boulder[DBController.styleId] < best[DBController.styleId]) {
        best = boulder;
      }
    }
    return best;
  }

  String _getDuration(DateTime start, DateTime end) {
    final int difference = end.difference(start).inMinutes.abs();
    final int hours = difference ~/ 60;
    final int minutes = difference % 60;
    return "${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}";
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    var q1 = await DBController().rawQuery(
      "SELECT * FROM ${DBController.boulderTable} "
      "WHERE ${DBController.sessionId} == (?)",
      [this.sessionID],
    );
    this.boulderList.addAll(q1);
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.sessionTable} "
      "WHERE ${DBController.sessionId} == (?)",
      [this.sessionID],
    );
  }
}

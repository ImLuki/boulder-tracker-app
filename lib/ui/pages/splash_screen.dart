import 'package:boulder_track/app/app_colors.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        child: Container(
            color: Colors.black.withOpacity(0.005),
            child: Stack(children: [
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Align(
                      child: Text("Loading...",
                          style: Theme.of(context)
                              .textTheme
                              .subtitle1
                              .copyWith(color: Colors.black54, fontWeight: FontWeight.normal)),
                      alignment: Alignment.bottomCenter)),
              Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: Image.asset("assets/images/boulder-tracker.png", height: 90, width: 90),
                  ),
                  SizedBox(height: 30),
                  CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(AppColors.accentColor))
                ],
              )),
            ])));
  }
}

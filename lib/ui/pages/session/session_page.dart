import 'dart:async';
import 'dart:io';

import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/dialog/confirm_dialog.dart';
import 'package:boulder_track/ui/pages/session/pages/active_session_widget.dart';
import 'package:boulder_track/ui/pages/session/pages/end_session_widget.dart';
import 'package:boulder_track/ui/pages/session/pages/start_session_widget.dart';
import 'package:boulder_track/ui/pages/session/widgets/save_button.dart';
import 'package:boulder_track/ui/pages/session/widgets/session_button.dart';
import 'package:boulder_track/ui/pages/session/widgets/timer_widget.dart';
import 'package:boulder_track/util/screenshot.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class LocalConstants {
  static const double RECTANGLE_HEIGHT_VALUE = 70;
}

class SessionPage extends StatefulWidget {
  SessionPage({Key key}) : super(key: key);

  @override
  _SessionPageState createState() => _SessionPageState();
}

class _SessionPageState extends State<SessionPage> {
  final SessionState state = SessionState();
  final ConfirmDialog confirmDialog = ConfirmDialog();
  final ScrollController controller = new ScrollController();
  final ScreenshotController _screenshotController = ScreenshotController();
  GlobalKey _fabKey = GlobalObjectKey("fab");
  var listener;

  @override
  void initState() {
    super.initState();
    listener = () {
      if (this.mounted) {
        setState(() {});
      }
    };
    state.addListener(listener);

    // unfocus TextFields when keyboard isn't visible anymore
    var keyboardVisibilityController = KeyboardVisibilityController();
    keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible && mounted && context != null) {
        FocusScope.of(context).unfocus();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool isKeyboardVisible = KeyboardVisibilityProvider.isKeyboardVisible(context);
    return getWidget(isKeyboardVisible);
  }

  Widget getWidget(bool isKeyboardVisible) {
    Widget pageWidget;
    EdgeInsetsGeometry padding = EdgeInsets.all(Constants.STANDARD_PADDING);
    Widget secondWidget = Container();
    switch (state.currentState) {
      case SessionStateEnum.none:
        return SafeArea(
            child: Stack(
          children: <Widget>[Align(alignment: Alignment.topCenter, child: _buildTitle()), SessionButton()],
        ));
      case SessionStateEnum.start:
        pageWidget = StartSessionWidget();
        break;
      case SessionStateEnum.active:
        padding = EdgeInsets.zero;
        pageWidget = ActiveSessionWidget(scrollController: this.controller);
        if (!isKeyboardVisible) {
          secondWidget = Padding(
              padding: EdgeInsets.only(
                bottom: Constants.STANDARD_PADDING,
              ),
              child: SaveButton(scrollController: this.controller));
        }
        break;
      case SessionStateEnum.end:
        pageWidget = EndSessionWidget(_screenshotController);
        padding = EdgeInsets.zero;
        break;
    }
    return Container(
        color: AppColors.accentColor,
        child: SafeArea(
            child: Column(children: <Widget>[
          createTopBar(),
          Expanded(
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: GestureDetector(
                    onVerticalDragUpdate: (dragDetails) {
                      FocusScope.of(context).unfocus();
                    },
                    onTapDown: (tapDownDetails) {
                      FocusScope.of(context).unfocus();
                    },
                    child: Stack(children: [
                      SingleChildScrollView(controller: controller, padding: padding, child: pageWidget),
                      secondWidget
                    ])),
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                )),
          )
        ])));
  }

  Widget _buildTitle() => Container(
      padding: EdgeInsets.fromLTRB(5, 20, 5, 20),
      child: Text(
        "Track your Bouldering",
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.subtitle1,
      ));

  Widget createTopBar() => Container(
        width: MediaQuery.of(context).size.width,
        height: LocalConstants.RECTANGLE_HEIGHT_VALUE,
        child: InkWell(
            onTap: () => onTapButton(),
            child: Center(
              child: getButtonFilling(),
            )),
        decoration: BoxDecoration(shape: BoxShape.rectangle, color: AppColors.accentColor),
      );

  Widget getButtonFilling() {
    switch (state.currentState) {
      case SessionStateEnum.none:
        return Container();
      case SessionStateEnum.start:
        return Align(
            alignment: Alignment.center,
            child: Text(
              "CREATE SESSION",
              style: Theme.of(context).textTheme.headline4,
            ));
      case SessionStateEnum.active:
        _showToolTipAsync();
        return Stack(children: <Widget>[
          Align(
              alignment: Alignment.center,
              child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                Text(
                  "SESSION-TIME",
                  style: Theme.of(context).textTheme.headline4,
                ),
                TimerWidget()
              ])),
          Align(
              alignment: Alignment.centerRight,
              child: Padding(
                  padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
                  child: Icon(Icons.wrong_location_outlined, key: _fabKey, color: Colors.white, size: 42)))
        ]);
      case SessionStateEnum.end:
        return Stack(children: [
          Align(
            alignment: Alignment.center,
            child: Text(
              "SESSION SUMMARY",
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                onPressed: () {
                  _screenshotController
                      .capture(
                          fileName:
                              "${DateFormat("yyyy-MM-dd").format(Data().getCurrentSession().startDate)}-Session-Summary")
                      .then((File image) {
                    Share.shareFiles([image.path]);
                  }).catchError((onError) {
                    print(onError);
                  });
                },
                icon: Icon(Icons.share, color: Colors.white),
              ))
        ]);
    }
    return Container();
  }

  void onTapButton() {
    switch (state.currentState) {
      case SessionStateEnum.none:
        this.state.nextState();
        return;
      case SessionStateEnum.start:
        if (Data().startSession()) {
          SessionState().nextState();
        } else {
          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(SnackBar(content: Text("Please enter a location.")));
          return;
        }
        break;
      case SessionStateEnum.active:
        confirmDialog.showEndSessionDialog(context);
        break;
      case SessionStateEnum.end:
        //Data().clearSession();
        this.state.nextState();
        break;
    }
    controller.jumpTo(0.0);
  }

  void _showToolTip() {
    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = _fabKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCircle(center: markRect.center, radius: markRect.longestSide * 0.65);

    coachMarkFAB.show(
        targetContext: _fabKey.currentContext,
        markRect: markRect,
        children: [
          Center(
              child: Text("Tap this button\nto close the session",
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontStyle: FontStyle.italic,
                    color: Colors.white,
                  )))
        ],
        duration: null);
  }

  Future<void> _showToolTipAsync() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("tool_tip_close_session") ?? true) {
      prefs.setBool("tool_tip_close_session", false);
      _showToolTip();
    }
  }
}

import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:flutter/material.dart';

class DropDownGrading extends StatefulWidget {
  @override
  _DropDownGradingState createState() => _DropDownGradingState();
}

class _DropDownGradingState extends State<DropDownGrading> {
  List<String> systems;
  String _value;

  @override
  void initState() {
    super.initState();
    systems = GradeManager().getGradeMapper().getGradeSystems();
    _value = GradeManager().getGradeMapper().gradePickerSystem;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      underline: Container(),
      onChanged: (String value) {
        setState(() {
          _value = value;
        });
        GradeManager().getGradeMapper().gradePickerSystem = value;
        Data().currentBoulder.gradeSystem = value;
      },
      value: _value,
      isDense: true,
      items: List<DropdownMenuItem<String>>.generate(
          systems.length,
          (index) => DropdownMenuItem(
              value: systems[index],
              child: Text(systems[index],
                  style: Theme.of(context).textTheme.bodyText1.copyWith(color: AppColors.accentColor2)))),
    );
  }
}

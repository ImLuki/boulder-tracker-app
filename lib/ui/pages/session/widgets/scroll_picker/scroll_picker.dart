import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/util/grade_mapper/abstract_mapper.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:flutter/material.dart';

/// This helper widget manages the scrollable content inside a picker widget.
class ScrollPicker extends StatefulWidget {
  ScrollPicker({
    Key key,
    @required this.initialValue,
    @required this.onChanged,
    this.showDivider: true,
    this.boulder,
  });

  // Events
  final ValueChanged<String> onChanged;

  // Variables
  final String initialValue;
  final bool showDivider;
  final Boulder boulder;

  @override
  _ScrollPickerState createState() => _ScrollPickerState(initialValue, boulder);
}

class _ScrollPickerState extends State<ScrollPicker> {
  _ScrollPickerState(this.selectedValue, this.boulder);

  // Constants
  static const double itemWidth = 60.0;

  // Variables
  double widgetHeight;
  int numberOfVisibleItems;
  int numberOfPaddingRows;
  double visibleItemsHeight;
  double offset;

  String selectedValue;
  Boulder boulder;

  ScrollController scrollController;
  AbstractGradeMapper gradeMapperWithListener;
  var listener;
  List<String> items;

  @override
  void initState() {
    super.initState();
    items = GradeManager().getGradeMapper().getGradesForPicker();

    int initialItem = items.indexOf(selectedValue);
    scrollController = FixedExtentScrollController(initialItem: initialItem);

    listener = () {
      if (this.mounted) {
        setState(() {
          scrollController.jumpTo(800);
          items = GradeManager().getGradeMapper().getGradesForPicker();
          selectedValue = GradeManager()
              .getGradeMapper()
              .getGradeFromId(boulder == null ? Data().getStandardGrade() : boulder.gradeID);
        });
      }
    };
    this.gradeMapperWithListener = GradeManager().getGradeMapper();
    this.gradeMapperWithListener.addListener(listener);
  }

  @override
  void dispose() {
    super.dispose();
    if (gradeMapperWithListener != null) {
      gradeMapperWithListener.removeListener(listener);
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    TextStyle defaultStyle = themeData.textTheme.bodyText2;
    TextStyle selectedStyle = themeData.textTheme.bodyText2.copyWith(fontWeight: FontWeight.bold);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        widgetHeight = constraints.maxHeight;

        return GestureDetector(
          onTapUp: _itemTapped,
          child: ListWheelScrollView.useDelegate(
            childDelegate: ListWheelChildBuilderDelegate(builder: (BuildContext context, int index) {
              if (index < 0 || index > items.length - 1) {
                return null;
              }

              var value = items[index];

              final TextStyle itemStyle = (value == selectedValue) ? selectedStyle : defaultStyle;

              return Center(
                child: RotatedBox(quarterTurns: 1, child: Text(value, style: itemStyle)),
              );
            }),
            controller: scrollController,
            itemExtent: itemWidth,
            onSelectedItemChanged: _onSelectedItemChanged,
            physics: FixedExtentScrollPhysics(),
          ),
        );
      },
    );
  }

  void _itemTapped(TapUpDetails details) {
    Offset position = details.localPosition;
    double center = widgetHeight / 2;
    double changeBy = position.dy - center;
    double newPosition = scrollController.offset + changeBy;

    // animate to and center on the selected item
    scrollController.animateTo(newPosition, duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

  void _onSelectedItemChanged(int index) {
    String newValue = items[index];
    if (newValue != selectedValue) {
      selectedValue = newValue;
      widget.onChanged(newValue);
      setState(() {});
    }
  }
}

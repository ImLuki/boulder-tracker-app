import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/ui/dialog/confirm_dialog.dart';
import 'package:flutter/material.dart';

class SaveButton extends StatelessWidget {
  final ConfirmDialog confirmDialog = ConfirmDialog();
  final ScrollController scrollController;

  SaveButton({Key key, this.scrollController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.bottomCenter,
        child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              elevation: 6.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0),
              ),
              padding: const EdgeInsets.only(left: 24.0, top: 12.0, right: 24.0, bottom: 12.0),
              primary: Colors.black,
            ),
            onPressed: () async {
              String boulderName = Util.getBoulderName(Data().currentBoulder.name);
              bool result = await confirmDialog.showBoulderDialog(context, Data().currentBoulder.copy());
              if (result != null && result) {
                this.scrollController.animateTo(0.0, duration: Duration(milliseconds: 500), curve: Curves.ease);
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(SnackBar(
                    content: Text('Added Boulder "$boulderName"'),
                    duration: Duration(seconds: 2),
                  ));
              }
            },
            icon: const Icon(Icons.save, color: Colors.white, size: 26),
            label: Text("SAVE",
                style:
                    Theme.of(context).textTheme.headline4.copyWith(fontWeight: FontWeight.w500, color: Colors.white))));
  }
}

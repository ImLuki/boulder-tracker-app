import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';

class DismissibleAscendList extends StatefulWidget {
  const DismissibleAscendList({Key key}) : super(key: key);

  @override
  _DismissibleAscendListState createState() => _DismissibleAscendListState();
}

class _DismissibleAscendListState extends State<DismissibleAscendList> {
  List<String> indexes;
  var listener;

  @override
  void initState() {
    super.initState();
    listener = () {
      if (this.mounted) {
        setState(() {});
      }
    };

    Data().getCurrentSession().addListener(listener);
  }

  @override
  void dispose() {
    super.dispose();
    var session = Data().getCurrentSession();
    if (session != null) {
      session.removeListener(listener);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container(
              child: SizedBox(height: 200, child: CircularProgressIndicator()),
            );
          }
          return createBoulderList(snapshot.data);
        });
  }

  Widget createBoulderList(List data) {
    List<Widget> items = [];
    indexes = List.generate(data.length, (index) => "$index");
    for (int index = 0; index < indexes.length; index++) {
      items.add(Dismissible(
          key: Key(indexes[index]),
          onDismissed: (direction) => removeEntryFunction(index, data[index]),
          child: createItem(data[index], index),
          direction: DismissDirection.startToEnd,
          background: Container(
            padding: EdgeInsets.only(left: Constants.STANDARD_PADDING),
            color: Colors.red,
            child: Icon(Icons.delete_forever_outlined, color: Colors.white),
            alignment: Alignment.centerLeft,
          )));
    }
    return Wrap(
      children: items,
    );
  }

  Widget createItem(dynamic boulder, int index) {
    return Padding(
        padding: EdgeInsets.only(
          left: Constants.STANDARD_PADDING,
          right: Constants.STANDARD_PADDING,
          bottom: 10.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Flexible(child: Text(Util.getBoulderName(boulder[DBController.name]), overflow: TextOverflow.ellipsis)),
              SizedBox(width: Constants.STANDARD_PADDING),
              Row(children: [
                InkWell(
                  onTap: () => removeEntryFunction(index, boulder),
                  child: Icon(Icons.delete, color: AppColors.accentColor2),
                ),
                SizedBox(width: 8.0),
                InkWell(
                    onTap: () => toggleMarked(index, boulder),
                    child: Icon(boulder[DBController.marked] == 1 ? Icons.favorite : Icons.favorite_border,
                        color: boulder[DBController.marked] == 1 ? Colors.red : AppColors.accentColor2)),
              ]),
            ]),
            Text(
                Util.getSummary(
                  boulder[DBController.gradeId],
                  boulder[DBController.styleId],
                  boulder[DBController.gradeSystem],
                ),
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal)),
          ],
        ));
  }

  void removeEntryFunction(int index, Map deletedEntry) {
    setState(() {
      indexes.removeAt(index);
      if (Data().removeAscendFromCurrentSession(deletedEntry)) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(SnackBar(
              content: Text("Deleted \"${Util.getBoulderName(deletedEntry[DBController.name])}\""),
              action: SnackBarAction(
                  textColor: Colors.lightBlue,
                  label: "UNDO",
                  onPressed: () => setState(() => {Data().insertAscendToCurrentSession(index, deletedEntry)}))));
      } else {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(SnackBar(content: Text("Couldn't delete entry")));
      }
    });
  }

  void toggleMarked(int index, Map ascend) {
    final DBController dbController = DBController();
    dbController.updateBoulder(
      {DBController.marked: ascend[DBController.marked] == 0 ? 1 : 0},
      ascend[DBController.boulderId],
    ).then((value) {
      setState(() {});
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(content: Text(ascend[DBController.marked] == 1 ? "Marked ascend" : "Umarked ascend")));
    });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    int sessionId = Data().getCurrentSession().sessionId;
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.boulderTable} "
      "WHERE ${DBController.sessionId} == (?)",
      [sessionId],
    );
  }
}

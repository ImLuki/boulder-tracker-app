import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/util/style_mapper.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';

class StylePicker extends StatefulWidget {
  final ValueChanged<int> onChanged;
  final int initialValue;

  const StylePicker({Key key, this.onChanged, this.initialValue}) : super(key: key);

  @override
  _StylePickerState createState() => _StylePickerState(initialValue);
}

class _StylePickerState extends State<StylePicker> {
  int selectedCategory;
  bool isBoulder;

  @override
  void initState() {
    super.initState();
  }

  _StylePickerState(this.selectedCategory);

  @override
  Widget build(BuildContext context) {
    return createStylePicker(context);
  }

  Widget createStylePicker(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Container(
          padding: EdgeInsets.only(top: 4.0, left: 0.0, right: 0.0, bottom: 6.0),
          child: Container(
              child: Center(
                  child: Column(children: <Widget>[
            SizedBox(height: 6.0),
            Container(
                child: Wrap(
              runSpacing: Constants.STANDARD_PADDING,
              spacing: Constants.STANDARD_PADDING,
              children: createButtons(),
            )),
            SizedBox(height: 6.0)
          ]))))
    ]);
  }

  List<Widget> createButtons() {
    var styles = StyleMapper().getBoulderMapping();
    List<Widget> widgets = [];
    for (var style in styles) {
      widgets.add(InkWell(
        splashColor: Colors.blue[100],
        onTap: () {
          selectedCategory = style.id;
          setState(() {});
          widget.onChanged(style.id);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 12.0),
          decoration: BoxDecoration(
            color: selectedCategory == style.id ? AppColors.accentColor : Colors.grey[300],
            borderRadius: BorderRadius.all(Radius.circular(48.0)),
          ),
          child: Text(style.style,
              style: selectedCategory == style.id
                  ? Theme.of(context).textTheme.bodyText1.copyWith(color: Colors.white)
                  : Theme.of(context).textTheme.bodyText1),
        ),
      ));
    }
    return widgets;
  }
}

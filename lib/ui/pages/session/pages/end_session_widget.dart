import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/widgets/session_summary.dart';
import 'package:boulder_track/util/screenshot.dart';
import 'package:flutter/material.dart';

class EndSessionWidget extends StatelessWidget {
  final ScreenshotController _screenshotController;

  EndSessionWidget(this._screenshotController);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Screenshot(
            controller: _screenshotController,
            child: Container(color: Colors.white, child: SessionSummary(Data().getCurrentSession().sessionId))),
        Divider(thickness: 2.0, indent: Constants.STANDARD_PADDING, endIndent: Constants.STANDARD_PADDING),
        SizedBox(height: Constants.STANDARD_PADDING),
        _createButton(context),
        SizedBox(height: 40)
      ],
    );
  }

  Widget _createButton(BuildContext context) {
    return ElevatedButton(

      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        padding: EdgeInsets.only(left: 24.0, top: 12.0, right: 24.0, bottom: 12.0),
        primary: AppColors.accentColor,
      ),
      onPressed: () => {Data().clearSession(), SessionState().nextState()},
      child: Text("CONFIRM", style: Theme.of(context).textTheme.headline4.copyWith(fontWeight: FontWeight.w500)),
    );
  }
}

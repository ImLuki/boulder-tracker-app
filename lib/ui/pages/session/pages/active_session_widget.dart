import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/dialog/confirm_dialog.dart';
import 'package:boulder_track/ui/dialog/info_dialogs.dart';
import 'package:boulder_track/ui/pages/session/widgets/dismissible_ascend_list.dart';
import 'package:boulder_track/ui/pages/session/widgets/scroll_picker/drop_down_grading.dart';
import 'package:boulder_track/ui/pages/session/widgets/save_button.dart';
import 'package:boulder_track/ui/pages/session/widgets/scroll_picker/scroll_picker.dart';
import 'package:boulder_track/ui/pages/session/widgets/style_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ActiveSessionWidget extends StatefulWidget {
  final ScrollController scrollController;

  const ActiveSessionWidget({Key key, this.scrollController}) : super(key: key);

  @override
  _ActiveSessionWidgetState createState() => _ActiveSessionWidgetState();
}

class _ActiveSessionWidgetState extends State<ActiveSessionWidget> {
  final InfoDialog infoDialog = InfoDialog();
  final ConfirmDialog confirmDialog = ConfirmDialog();
  TextEditingController boulderNameController = TextEditingController();
  TextEditingController commentController = TextEditingController();
  final Data data = Data();
  var listener;
  var sessionWithListener;

  @override
  void initState() {
    super.initState();
    listener = () {
      if (this.mounted) {
        setState(() {});
      }
    };
    this.sessionWithListener = data.getCurrentSession();
    this.sessionWithListener.addListener(listener);
  }

  @override
  void dispose() {
    super.dispose();
    if (sessionWithListener != null) {
      sessionWithListener.removeListener(listener);
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool isKeyboardVisible = KeyboardVisibilityProvider.isKeyboardVisible(context);
    boulderNameController.text = data.currentBoulder.name;
    commentController.text = data.currentBoulder.comment;
    return Column(children: [
      Padding(
          padding: EdgeInsets.all(Constants.STANDARD_PADDING),
          child: Wrap(
            alignment: WrapAlignment.center,
            direction: Axis.horizontal,
            runSpacing: Constants.STANDARD_PADDING,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text("Boulder-Grade:"),
                Padding(padding: EdgeInsets.only(right: Constants.STANDARD_PADDING), child: DropDownGrading())
              ]),
              createDifficultyPicker(context),
              Divider(thickness: 2.0),
              Align(child: Text("Boulder Name:"), alignment: Alignment.centerLeft),
              createNameTextField(context),
              Divider(thickness: 2.0),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text("Boulder-Style:"),
                Padding(
                    padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
                    child: InkWell(
                        onTap: () => infoDialog.showStyleInfoDialog(context),
                        child: IconTheme(
                            data: IconThemeData(color: AppColors.accentColor2), child: Icon(Icons.info_outline))))
              ]),
              StylePicker(
                initialValue: data.currentBoulder.styleID,
                onChanged: (newValue) => data.currentBoulder.styleID = newValue,
              ),
              Divider(thickness: 2.0),
              Align(child: Text("Rating:"), alignment: Alignment.centerLeft),
              createRatingBar(),
              Divider(thickness: 2.0),
              Align(child: Text("Comment:"), alignment: Alignment.centerLeft),
              createCommentField(context, (data.currentBoulder.comment ?? "").toString()),
              Divider(thickness: 2.0),
              if (isKeyboardVisible) ...[SaveButton(scrollController: widget.scrollController), Divider(thickness: 2.0)]
            ],
          )),
      Visibility(
          visible: Data().getCurrentSession().boulderCount > 0,
          child: Theme(
              data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
              child: ExpansionTile(
                  initiallyExpanded: true,
                  title: Text("Ascends:", style: Theme.of(context).textTheme.bodyText2),
                  children: [DismissibleAscendList()]))),
      SizedBox(height: 100.0),
    ]);
  }

  Widget createNameTextField(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: TextFormField(
            controller: boulderNameController,
            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            onChanged: (String value) => data.currentBoulder.name = value.trim(),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
            textCapitalization: TextCapitalization.sentences,
            decoration: new InputDecoration(
                isDense: true,
                labelText: "Enter Boulder Name",
                labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
                fillColor: AppColors.accentColor,
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                  borderSide: new BorderSide(),
                ),
                focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                    borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS)))));
  }

  Widget createCommentField(BuildContext context, String initValue) {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: TextFormField(
            maxLines: 5,
            controller: commentController,
            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            onChanged: (String value) => data.currentBoulder.comment = value.trim(),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
            textCapitalization: TextCapitalization.sentences,
            decoration: new InputDecoration(
                alignLabelWithHint: true,
                isDense: true,
                labelText: "Enter Comment",
                labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
                fillColor: AppColors.accentColor,
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                  borderSide: new BorderSide(),
                ),
                focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                    borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS)))));
  }

  Widget createDifficultyPicker(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        height: 40,
        width: MediaQuery.of(context).size.width,
        child: ShaderMask(
          shaderCallback: (Rect rect) {
            return LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: <Color>[Colors.black, Colors.transparent, Colors.transparent, Colors.black],
                stops: [0.001, 0.45, 0.55, 9.9]).createShader(rect);
          },
          blendMode: BlendMode.dstOut,
          child: RotatedBox(
              quarterTurns: 3,
              child: ScrollPicker(
                onChanged: (String value) {
                  data.currentBoulder.gradeID =
                      GradeManager().getGradeMapper().getIdFromGrade(value) ?? data.getStandardGrade();
                },
                initialValue: GradeManager().getGradeMapper().getGradeForGradePicker(data.currentBoulder.gradeID),
                boulder: data.currentBoulder,
              )),
        ),
      ),
    );
  }

  Widget createRatingBar() {
    return RatingBar(
        initialRating: data.currentBoulder.rating.toDouble(),
        minRating: 1,
        glow: false,
        direction: Axis.horizontal,
        allowHalfRating: false,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        onRatingUpdate: (rating) {
          data.currentBoulder.rating = rating.toInt();
        },
        ratingWidget: RatingWidget(
          empty: Constants.EMPTY_STAR,
          full: Constants.FULL_STAR,
          half: null,
        ));
  }
}

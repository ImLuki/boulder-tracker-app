import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/pages/session/widgets/filtered_text_field_with_list.dart';
import 'package:boulder_track/util/notifications_handler.dart';
import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';

class StartSessionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Wrap(
        alignment: WrapAlignment.center,
        direction: Axis.horizontal,
        runSpacing: Constants.STANDARD_PADDING,
        spacing: Constants.STANDARD_PADDING,
        children: [
          Align(child: Text("Environment:"), alignment: Alignment.centerLeft),
          createIndoorOutdoorSwitch(context),
          Divider(thickness: 2.0),
          Align(child: Text("Location:"), alignment: Alignment.centerLeft),
          createLocationInput(),
          Divider(thickness: 2.0),
          createButtons(context)
        ]);
  }

  Widget createIndoorOutdoorSwitch(BuildContext context) {
    return ToggleSwitch(
      initialLabelIndex: Data().getCurrentSession().outdoor ? 1 : 0,
      minWidth: MediaQuery.of(context).size.width * 0.35,
      minHeight: 45.0,
      cornerRadius: 20.0,
      activeBgColor: [Colors.green],
      activeFgColor: Colors.white,
      inactiveBgColor: AppColors.accentColor2,
      inactiveFgColor: Colors.white,
      labels: ['INDOOR', 'OUTDOOR'],
      onToggle: (index) {
        Data().getCurrentSession().outdoor = index == 1;
      },
      totalSwitches: 2,
    );
  }

  Widget createLocationInput() {
    return FilteredTextFieldWithList();
  }

  Widget createButtons(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.end, children: [
      MaterialButton(
        shape: ContinuousRectangleBorder(side: BorderSide(color: Colors.black12)),
        padding: EdgeInsets.fromLTRB(10, 8, 8, 10),
        highlightColor: AppColors.accentColor.withOpacity(0.3),
        onPressed: () => SessionState().setState(SessionStateEnum.none),
        child: Text("CANCEL",
            style: Theme.of(context)
                .textTheme
                .headline4
                .copyWith(fontSize: 24, fontWeight: FontWeight.w400, color: AppColors.accentColor)),
      ),
      SizedBox(width: Constants.STANDARD_PADDING),
      MaterialButton(
          padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
          color: AppColors.accentColor,
          onPressed: () => {startSession(context)},
          textColor: Colors.white,
          child: Text("START",
              style: Theme.of(context).textTheme.headline4.copyWith(fontSize: 24, fontWeight: FontWeight.w400)))
    ]);
  }

  void startSession(BuildContext context) {
    if (Data().startSession()) {
      NotificationHandler().scheduleNotification();
      SessionState().nextState();
    } else {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(content: Text("Please enter a location.")));
    }
  }
}

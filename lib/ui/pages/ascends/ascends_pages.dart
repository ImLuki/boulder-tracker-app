import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/search_settings.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/pages/ascends/expandable_boulder.dart';
import 'package:boulder_track/ui/pages/ascends/search_settings_page.dart';
import 'package:boulder_track/ui/widgets/empty_list_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class AscendsPage extends StatefulWidget {
  AscendsPage({Key key}) : super(key: key);

  @override
  _AscendsPageState createState() => _AscendsPageState();
}

class _AscendsPageState extends State<AscendsPage> {
  final DBController dbController = DBController();
  final SearchSettings settings = SearchSettings();
  final ItemScrollController _scrollController = ItemScrollController();
  final TextEditingController textController = TextEditingController();

  Future<List<Map<String, dynamic>>> data;

  int _expandedIndex = -1;
  String searchString = "";
  bool searchBarVisibility = false;

  @override
  void initState() {
    super.initState();
    this.data = fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: AppColors.accentColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: createSearchBar(),
        ),
        body: Center(child: createListView()));
  }

  Widget createListView() {
    return FutureBuilder(
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Align(
            alignment: Alignment.topCenter,
            child: PreferredSize(
              preferredSize: Size.fromHeight(6.0),
              child: LinearProgressIndicator(),
            ),
          );
        }
        var filteredData = [];
        if (snapshot.data != null) {
          filteredData = snapshot.data
              .where((item) => item[DBController.name].toString().toLowerCase().contains(searchString))
              .toList();
        }
        return filteredData.length == 0
            ? EmptyListPlaceHolder(text: "No Ascends Found")
            : Scrollbar(
                child: ScrollablePositionedList.separated(
                itemScrollController: _scrollController,
                itemCount: filteredData.length,
                itemBuilder: (context, index) {
                  return ExpandableAscend(
                    key: Key("${filteredData[index][DBController.boulderId]}"),
                    data: filteredData[index],
                    refresh: refresh,
                    index: index,
                    expandedIndex: _expandedIndex,
                  );
                },
                padding: const EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 8.0, Constants.STANDARD_PADDING, 8.0),
                separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 2.0),
              ));
      },
      future: this.data,
    );
  }

  void refresh(int index, int expandedIndex) {
    setState(() {
      this.data = fetchData();
      this._expandedIndex = expandedIndex;
    });
    // This is a hack, because without Delay this would be overwritten by the build update
    Future.delayed(Duration(milliseconds: 200)).then((value) => _scrollController..jumpTo(index: index));
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    Sorting sorting = SearchSettings().currentSorting;
    List<dynamic> conditions = SearchSettings().getSQLConditionString();
    return await dbController.rawQuery(
        "SELECT * FROM ${DBController.boulderTable} "
        "JOIN ${DBController.sessionTable} using (${DBController.sessionId}) "
        "WHERE ${conditions[0]} "
        "ORDER BY ${Enum.getSortingKey(sorting)}",
        conditions[1]);
  }

  Widget createSearchBar() {
    return Row(children: [
      Expanded(
          child: this.searchBarVisibility
              ? buildFloatingSearchBar()
              : Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Text("ASCEND LIST", style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.start),
                  IconButton(
                      icon: const Icon(Icons.search, color: Colors.white, size: 30),
                      onPressed: () {
                        setState(() {
                          this.searchBarVisibility = true;
                        });
                      })
                ])),
      Padding(
          padding: EdgeInsets.zero,
          child: IconButton(
              icon: const Icon(Icons.tune, color: Colors.white, size: 30),
              onPressed: () {
                Navigator.of(context).push(createSettingsRoute()).then((value) => setState(() {
                      this.data = fetchData();
                    }));
              })),
    ]);
  }

  Widget buildFloatingSearchBar() {
    return Container(
        height: AppBar().preferredSize.height * 0.70,
        child: TextFormField(
            autofocus: true,
            controller: textController,
            onChanged: (value) {
              setState(() => this.searchString = value.trim().toLowerCase());
            },
            textAlign: TextAlign.left,
            textAlignVertical: TextAlignVertical.center,
            textCapitalization: TextCapitalization.sentences,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.zero,
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(4.0),
                ),
                filled: true,
                fillColor: Colors.white,
                prefixIcon: InkWell(
                    onTap: () => {
                          setState(() {
                            this.searchBarVisibility = false;
                            textController.clear();
                            this.searchString = "";
                          })
                        },
                    child: Icon(Icons.arrow_back, color: Colors.black)),
                suffixIcon: InkWell(
                    onTap: () => setState(
                          () {
                            textController.clear();
                            this.searchString = "";
                          },
                        ),
                    child:
                        Icon(Icons.clear, color: this.textController.value.text == "" ? Colors.white : Colors.black)),
                hintText: "Search for Boulder",
                hintStyle: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.normal)),
            style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 20)));
  }

  Route createSettingsRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => SearchSettingsPage(),
    );
  }
}

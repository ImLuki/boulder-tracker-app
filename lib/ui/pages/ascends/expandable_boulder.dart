import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/ui/dialog/edit_dialog.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ExpandableAscend extends StatefulWidget {
  const ExpandableAscend({Key key, this.data, this.refresh, this.index, this.expandedIndex}) : super(key: key);
  final data;
  final Function(int, int) refresh;
  final int index;
  final int expandedIndex;

  @override
  _ExpandableAscendState createState() => _ExpandableAscendState(data);
}

class _ExpandableAscendState extends State<ExpandableAscend> {
  bool expanded = false;
  bool marked;
  var data;

  _ExpandableAscendState(this.data);

  @override
  void initState() {
    super.initState();
    this.expanded = widget.expandedIndex == widget.index;
    marked = data["marked"].isOdd;
  }

  @override
  Widget build(BuildContext context) {
    return getDataItemWidget();
  }

  Widget getDataItemWidget() {
    return expanded
        ? InkWell(
            onTap: () => setState(() => {expanded = false}),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(height: 4.0),
                  Row(children: [
                    Visibility(visible: marked, child: Icon(Icons.favorite, color: Colors.red)),
                    Visibility(visible: marked, child: const SizedBox(width: 4)),
                    Flexible(
                        child: Text(Util.getBoulderName(data[DBController.name]),
                            style: const TextStyle(fontSize: 24), overflow: TextOverflow.ellipsis, maxLines: 2))
                  ]),
                  const SizedBox(height: 20),
                  Row(children: [
                    const Icon(Icons.calendar_today),
                    const SizedBox(width: 20),
                    Text(DateFormat("dd.MM.yy").format(DateTime.parse(data[DBController.sessionTimeStart])),
                        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1)
                  ]),
                  const SizedBox(height: 10.0),
                  Row(children: [
                    Icon(Icons.location_on_outlined),
                    const SizedBox(width: 20),
                    Text(Util.getLocation(data[DBController.sessionLocation]),
                        style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1)
                  ]),
                  const SizedBox(height: 10),
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Icon(Icons.info_outline),
                    const SizedBox(width: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            "${Util.getGrade(
                              data[DBController.gradeId],
                              data[DBController.gradeSystem],
                            )} (${data[DBController.gradeSystem]})",
                            style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1),
                        if (data[DBController.gradeSystem] != GradeManager().getGradeMapper().currentGradingSystem)
                          Text(
                              "${Util.getGrade(
                                data[DBController.gradeId],
                                GradeManager().getGradeMapper().currentGradingSystem,
                              )} (${GradeManager().getGradeMapper().currentGradingSystem})",
                              style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1),
                        const SizedBox(height: 4.0),
                        Text(Util.getStyle(data[DBController.styleId]),
                            style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1),
                      ],
                    )
                  ]),
                  const SizedBox(height: 10),
                  Row(children: [
                    const Icon(Icons.stars),
                    const SizedBox(width: 20),
                    Util.getRating(
                      data[DBController.rating],
                    )
                  ]),
                  const SizedBox(height: 10),
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    const Icon(Icons.comment_outlined),
                    const SizedBox(width: 20),
                    Flexible(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(width: 20),
                        Text(Util.getComment(data[DBController.comment]),
                            style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                            overflow: TextOverflow.clip)
                      ],
                    ))
                  ]),
                  const SizedBox(height: 20),
                  Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                    TextButton(
                        onPressed: () {
                          EditDialog()
                              .showBoulderDialog(
                                  context,
                                  Boulder(
                                    data[DBController.gradeId],
                                    data[DBController.gradeSystem],
                                    data[DBController.styleId],
                                    data[DBController.rating],
                                    data[DBController.name],
                                    DateTime.parse(data[DBController.sessionTimeStart]),
                                    data[DBController.sessionLocation],
                                    data[DBController.sessionOutdoor] == 1,
                                    data[DBController.marked] == 1,
                                    comment: data[DBController.comment],
                                    boulderID: data[DBController.boulderId],
                                  ),
                                  data[DBController.sessionId],
                                  onChanged: (value) => setState(() {
                                        this.data = Map()..addAll(this.data);
                                        this.data[DBController.gradeId] = value[DBController.gradeId];
                                        this.data[DBController.gradeSystem] = value[DBController.gradeSystem];
                                        this.data[DBController.styleId] = value[DBController.styleId];
                                        this.data[DBController.rating] = value[DBController.rating];
                                        this.data[DBController.name] = value[DBController.name];
                                        this.data[DBController.marked] = value[DBController.marked];
                                        this.data[DBController.comment] = value[DBController.comment];
                                      }))
                              .then((value) => widget.refresh(widget.index, widget.index));
                        },
                        child: Column(children: <Widget>[
                          Icon(Icons.edit, color: Colors.black87),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2.0),
                          ),
                          Text('Edit', style: TextStyle(color: Colors.black87))
                        ])),
                    TextButton(
                        onPressed: () => toggleMarked(),
                        child: Column(children: <Widget>[
                          Icon(marked ? Icons.favorite : Icons.favorite_border,
                              color: marked ? Colors.red : Colors.black87),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2.0),
                          ),
                          Text('Mark', style: TextStyle(color: Colors.black87))
                        ])),
                  ])
                ]))
        : InkWell(
            onTap: () => setState(() => {expanded = true}),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                    fit: FlexFit.tight,
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Row(children: [
                        Visibility(visible: marked, child: Icon(Icons.favorite, color: Colors.red)),
                        Visibility(visible: marked, child: const SizedBox(width: 4)),
                        Flexible(
                            child: Text(Util.getBoulderName(data[DBController.name]),
                                overflow: TextOverflow.ellipsis, maxLines: 1))
                      ]),
                      const SizedBox(height: 4.0),
                      Text(
                          Util.getLocationDateString(
                            data[DBController.sessionTimeStart],
                            data[DBController.sessionLocation],
                          ),
                          style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1),
                    ])),
                const SizedBox(width: 14.0),
                Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
                  Text(
                      Util.getSummary(
                        data[DBController.gradeId],
                        data[DBController.styleId],
                        data[DBController.gradeSystem],
                      ),
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal)),
                  const SizedBox(height: 4.0),
                  Util.getRating(data[DBController.rating]),
                ])
              ],
            ));
  }

  void toggleMarked() {
    setState(() {
      this.marked = !this.marked;
    });
    final DBController dbController = DBController();
    dbController.updateBoulder({DBController.marked: this.marked ? 1 : 0}, data[DBController.boulderId]);
  }
}

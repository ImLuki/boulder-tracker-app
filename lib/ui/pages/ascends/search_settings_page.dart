import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/search_settings.dart';
import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SearchSettingsPage extends StatefulWidget {
  @override
  _SearchSettingsPageState createState() => _SearchSettingsPageState();
}

class _SearchSettingsPageState extends State<SearchSettingsPage> {
  SearchSettings settings = SearchSettings();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: AppColors.accentColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Search", style: TextStyle(color: Colors.white, fontSize: 22)),
            TextButton(
                onPressed: () {
                  settings.reset();
                  setState(() {});
                },
                child: Text("CLEAR", style: TextStyle(color: Colors.white)))
          ])),
      body: Center(
        child: createSettings(),
      ),
    );
  }

  Widget createSettings() {
    return ListView(
        padding: const EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 8.0, Constants.STANDARD_PADDING, 8.0),
        children: [
          Visibility(
              visible: SessionState().currentState == SessionStateEnum.active,
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text("Only current location"),
                Switch(
                    value: settings.onlyCurrentLocation,
                    onChanged: (value) {
                      settings.onlyCurrentLocation = value;
                      setState(() {});
                    })
              ])),
          Visibility(visible: SessionState().currentState == SessionStateEnum.active, child: Divider(thickness: 2.0)),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Indoor"),
            Checkbox(
                activeColor: Colors.blue,
                value: settings.indoor,
                onChanged: (value) {
                  settings.indoor = value;
                  setState(() {});
                })
          ]),
          Divider(thickness: 2.0),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Outdoor"),
            Checkbox(
                activeColor: Colors.blue,
                value: settings.outdoor,
                onChanged: (value) {
                  settings.outdoor = value;
                  setState(() {});
                })
          ]),
          Divider(thickness: 2.0),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Only marked ascends"),
            Switch(
                value: settings.onlyMarked,
                onChanged: (value) {
                  settings.onlyMarked = value;
                  setState(() {});
                })
          ]),
          Divider(thickness: 2.0),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Only Flashes"),
            Switch(
                value: settings.onlyFlashes,
                onChanged: (value) {
                  settings.onlyFlashes = value;
                  settings.onlyNotTopped = false;
                  setState(() {});
                })
          ]),
          Divider(thickness: 2.0),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Only not topped"),
            Switch(
                value: settings.onlyNotTopped,
                onChanged: (value) {
                  settings.onlyNotTopped = value;
                  settings.onlyFlashes = false;
                  setState(() {});
                })
          ]),
          Divider(thickness: 2.0),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Location"),
            DropdownButton(
                value: settings.currentLocation,
                underline: Container(),
                style: Theme.of(context).textTheme.bodyText2,
                onChanged: (value) {
                  setState(() {
                    settings.currentLocation = value;
                  });
                },
                items: List<DropdownMenuItem>.generate(
                  Data().getLocations().length,
                  (i) => DropdownMenuItem(
                      value: Data().getLocations()[i],
                      child: Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text(Util.getLocation(Data().getLocations()[i]), overflow: TextOverflow.ellipsis))),
                )..insert(0, DropdownMenuItem(value: "All Locations", child: Text("All Locations"))))
          ]),
          Divider(thickness: 2.0),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Sorting"),
            DropdownButton(
              value: settings.currentSorting.toString(),
              underline: Container(),
              style: Theme.of(context).textTheme.bodyText2,
              onChanged: (value) {
                setState(() {
                  settings.currentSorting = Enum.toEnum(value);
                });
              },
              items: Sorting.values.map((Sorting item) {
                return DropdownMenuItem<String>(
                  child: Text(Enum.name(item)),
                  value: item.toString(),
                );
              }).toList(),
            )
          ]),
          Divider(thickness: 2.0),
        ]);
  }
}

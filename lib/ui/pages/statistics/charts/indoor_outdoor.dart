import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/pie_chart.dart';
import 'package:flutter/material.dart';

class IndoorOutdoor extends StatelessWidget {
  const IndoorOutdoor({Key key, this.extended}) : super(key: key);

  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }

          Map<String, double> data = Map<String, double>();
          snapshot.data[0].forEach((key, value) => data[key] = value.toDouble());
          return CustomPieChart(
            this.extended,
            data,
            colors: [
              AppColors.chart_colors.elementAt(0),
              AppColors.chart_colors.elementAt(2),
            ],
          );
        });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery("SELECT "
        "COUNT(CASE WHEN ${DBController.sessionOutdoor} == 0 THEN 1 END) as indoor, "
        "COUNT(CASE WHEN ${DBController.sessionOutdoor} THEN 1 END) as outdoor "
        "FROM ${DBController.boulderTable} "
        "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) ");
  }
}

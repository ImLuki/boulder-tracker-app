import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/average_grade_chart.dart';
import 'package:flutter/material.dart';

class AverageGrades extends StatelessWidget {
  const AverageGrades({Key key, this.extended}) : super(key: key);
  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.wait([fetchData1(), fetchData2()]),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }
          var data = _calculateData(snapshot.data);
          if (data.first.isEmpty) {
            return Align(
              child: Text("No Data", style: TextStyle(color: Colors.black26, fontSize: 28)),
              alignment: Alignment.center,
            );
          }
          return AverageGradeChart(this.extended, data[0], data[1]);
        });
  }

  Future<List<Map<String, dynamic>>> fetchData1() async {
    return DBController().rawQuery(
        "SELECT ${DBController.sessionId}, ${DBController.gradeId} "
        "FROM ${DBController.boulderTable} "
        "JOIN ${DBController.sessionTable} using (${DBController.sessionId}) "
        "WHERE ${DBController.sessionId} IN "
        "(SELECT ${DBController.sessionId} FROM ${DBController.sessionTable} "
        "WHERE ${DBController.sessionStatus} != (?) "
        "ORDER BY ${DBController.sessionTimeStart} DESC LIMIT (?))"
        "ORDER BY ${DBController.sessionTimeStart} DESC",
        [Data.SESSION_ACTIVE_STATE, Data.MAX_SESSION_AMOUNT]);
  }

  Future<List<Map<String, dynamic>>> fetchData2() async {
    return DBController().rawQuery(
        "SELECT ${DBController.sessionId}, ${DBController.sessionTimeStart} FROM ${DBController.sessionTable} "
        "WHERE ${DBController.sessionStatus} != (?) "
        "ORDER BY ${DBController.sessionTimeStart} DESC LIMIT (?)",
        [Data.SESSION_ACTIVE_STATE, Data.MAX_SESSION_AMOUNT]);
  }

  List<List<dynamic>> _calculateData(List<List<Map<String, dynamic>>> data) {
    List<double> average;
    Map<int, double> amountMap = Map();
    Map<int, int> countMap = Map();
    Map<int, DateTime> dateMap = Map();
    for (Map<String, dynamic> session in data[1]) {
      amountMap[session[DBController.sessionId]] = 0;
      countMap[session[DBController.sessionId]] = 0;
      dateMap[session[DBController.sessionId]] = DateTime.parse(session[DBController.sessionTimeStart]);
    }

    for (Map<String, dynamic> element in data[0]) {
      amountMap[element[DBController.sessionId]] +=
          GradeManager().getGradeMapper().normalize(element[DBController.gradeId]);
      countMap[element[DBController.sessionId]] += 1;
    }

    average = amountMap.map((key, value) => MapEntry(key, value / countMap[key])).values.toList();
    List<DateTime> dates = dateMap.values.toList();
    return [average, dates];
  }
}

import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/date_time_bar_chart.dart';
import 'package:flutter/material.dart';

class LastSessionsBoulderCountChart extends StatelessWidget {
  const LastSessionsBoulderCountChart({Key key, this.extended}) : super(key: key);

  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }
          return DateTimeBarChart(calculateData(snapshot.data), this.extended);
        });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery("SELECT ${DBController.sessionTimeStart}, "
        "COUNT(${DBController.boulderId}) as amount "
        "FROM ${DBController.boulderTable} "
        "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
        "GROUP BY ${DBController.sessionId} "
        "ORDER BY ${DBController.sessionTimeStart}");
  }

  List<List<dynamic>> calculateData(List<Map<String, dynamic>> data) {
    List<List<dynamic>> lastSessions = [[], []];
    for (Map<String, dynamic> element in data) {
      lastSessions[0].add(DateTime.parse(element[DBController.sessionTimeStart]));
      lastSessions[1].add(element["amount"].toDouble());
    }
    return lastSessions;
  }
}

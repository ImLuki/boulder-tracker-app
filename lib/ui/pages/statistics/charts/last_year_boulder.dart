import 'dart:math';

import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/year_combined_chart.dart';
import 'package:flutter/material.dart';

class BoulderPastYearChart extends StatelessWidget {
  const BoulderPastYearChart({Key key, this.extended, this.from, this.to}) : super(key: key);

  final bool extended;
  final DateTime from;
  final DateTime to;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }
          DateTime now = DateTime.now();
          DateTime fromUpdated = this.from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
          DateTime toUpdated = this.to ?? now;
          return YearBarChart(
            this.extended,
            _calculateData(snapshot.data
                .where((element) =>
                    DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
                    DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
                .toList()),
            "Best:",
            "#Boulder:",
            reorderMonths: from == null,
          );
        });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController()
        .rawQuery("SELECT ${DBController.sessionTimeStart}, ${DBController.gradeId}, ${DBController.styleId} "
            "FROM ${DBController.boulderTable} "
            "JOIN ${DBController.sessionTable} using(${DBController.sessionId})");
  }

  List<List<double>> _calculateData(List<Map<String, dynamic>> data) {
    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
    ];
    for (Map<String, dynamic> element in data) {
      lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
      if (element[DBController.styleId] <= 101) {
        lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
          lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
          GradeManager().getGradeMapper().normalize(element[DBController.gradeId]).toDouble(),
        );
      }
    }
    return lastYear;
  }
}

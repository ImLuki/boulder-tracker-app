import 'dart:math';

import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/data_points.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/line_chart.dart';
import 'package:flutter/material.dart';

class SessionTimeChart extends StatelessWidget {
  const SessionTimeChart({Key key, this.extended}) : super(key: key);

  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }
          return LineChart(_limitDate(_calculateData(snapshot.data)), extended, "", measureFormatting: "Minutes");
        });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
        "SELECT ${DBController.sessionTimeStart}, ${DBController.sessionTimeEnd} "
        "FROM ${DBController.sessionTable} "
        "WHERE ${DBController.sessionStatus} != (?) "
        "ORDER BY ${DBController.sessionTimeStart}",
        [Data.SESSION_ACTIVE_STATE]);
  }

  List<DateTimeDataPoint> _calculateData(List<Map<String, dynamic>> data) {
    List<DateTimeDataPoint> data1 = [];
    for (Map<String, dynamic> element in data) {
      DateTime start = DateTime.parse(element[DBController.sessionTimeStart]);
      DateTime end = DateTime.parse(element[DBController.sessionTimeEnd]);
      data1.add(DateTimeDataPoint(start, end.difference(start).inMinutes.abs().toDouble()));
    }
    return data1;
  }

  List<DateTimeDataPoint> _limitDate(List<dynamic> data) {
    return Data().limitCharts
        ? data.getRange(max(0, data.length - Data.MAX_SESSION_AMOUNT), data.length).toList()
        : data;
  }
}

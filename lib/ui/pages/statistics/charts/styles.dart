import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/util/style_mapper.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/pie_chart.dart';
import 'package:flutter/material.dart';

class StylesChart extends StatelessWidget {
  const StylesChart({Key key, this.extended}) : super(key: key);

  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }

          return CustomPieChart(this.extended, calculateData(snapshot.data));
        });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery("SELECT ${DBController.styleId} FROM ${DBController.boulderTable}");
  }

  Map<String, double> calculateData(List<Map<String, dynamic>> data) {
    final StyleMapper _styleMapper = StyleMapper();
    Map<String, double> styles = Map();
    _styleMapper.mapping.forEach((key, value) {
      styles[value] = 0;
    });

    for (Map<String, dynamic> boulder in data) {
      styles[_styleMapper.getStyleFromId(boulder[DBController.styleId])] += 1;
    }

    return styles;
  }
}

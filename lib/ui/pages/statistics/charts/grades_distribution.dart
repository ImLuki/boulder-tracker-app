import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/util/grade_mapper/abstract_mapper.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/backend/util/style_mapper.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/bar_chart.dart';
import 'package:flutter/material.dart';

class GradesDistributionChart extends StatelessWidget {
  const GradesDistributionChart({Key key, this.extended}) : super(key: key);

  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }
          return BarChart(calculateData(snapshot.data), this.extended);
        });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery("SELECT "
        "COUNT(CASE WHEN ${DBController.styleId} = 0 THEN 1 END) as Flash, "
        "COUNT(CASE WHEN ${DBController.styleId} = 1 THEN 1 END) as Top, "
        "COUNT(CASE WHEN ${DBController.styleId} = 2 THEN 1 END) as Project, "
        "${DBController.gradeId} "
        "FROM ${DBController.boulderTable} "
        "GROUP BY ${DBController.gradeId} ");
  }

  List<List<dynamic>> calculateData(List<Map<String, dynamic>> data) {
    AbstractGradeMapper mapper = GradeManager().getGradeMapper();
    List<List<dynamic>> out = [mapper.getLabels().toSet().toList()];

    List<Map<String, int>> results = [];

    for (dynamic _ in StyleMapper().mapping.values.take(5)) {
      results.add(Map());
    }

    for (String grade in out[0]) {
      for (int i = 0; i < results.length; i++) {
        results[i][grade] = 0;
      }
    }

    for (Map<String, dynamic> element in data) {
      for (int i = 0; i < results.length; i++) {
        if (mapper.getRoundedKey(element[DBController.gradeId]) == null) {
          print(element[DBController.gradeId]);
        }
        results[i][mapper.getGradeFromId(mapper.getRoundedKey(element[DBController.gradeId]))] +=
            element[StyleMapper().mapping[i]];
      }
    }

    for (Map value in results) {
      out.add(value.values.toList());
    }

    return out;
  }
}

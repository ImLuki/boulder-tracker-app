import 'dart:math';

import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/data_points.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/line_chart.dart';
import 'package:flutter/material.dart';

class BoulderBestGradeChart extends StatelessWidget {
  const BoulderBestGradeChart({Key key, this.extended}) : super(key: key);
  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.wait([fetchData1(), fetchData2()]),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }

          var data = _calculateData(snapshot.data[0], snapshot.data[1]);
          if (data.isEmpty) {
            return Align(
              child: Text("No Data", style: TextStyle(color: Colors.black26, fontSize: 28)),
              alignment: Alignment.center,
            );
          }
          return LineChart(
            Data().limitCharts
                ? data.getRange(max(0, data.length - Data.MAX_SESSION_AMOUNT), data.length).toList()
                : data,
            extended,
            "Grade:",
            useGradeLabels: true,
          );
        });
  }

  Future<List<Map<String, dynamic>>> fetchData1() async {
    return DBController().rawQuery("SELECT ${DBController.sessionTimeStart}, ${DBController.sessionId} "
        "FROM ${DBController.sessionTable} "
        "WHERE ${DBController.sessionId} IN "
        "(SELECT ${DBController.sessionId} FROM ${DBController.boulderTable}) "
        "ORDER BY ${DBController.sessionTimeStart}");
  }

  Future<List<Map<String, dynamic>>> fetchData2() async {
    return DBController().rawQuery(
        "SELECT ${DBController.gradeId}, ${DBController.sessionId} "
        "FROM ${DBController.boulderTable} "
        "WHERE ${DBController.styleId} <= (?)",
        [1]);
  }

  List<DateTimeDataPoint> _calculateData(List<Map<String, dynamic>> sessions, List<Map<String, dynamic>> data) {
    Map<int, int> grades = Map();
    Map<int, String> dateIDMapper = Map();
    for (Map<String, dynamic> session in sessions) {
      grades[session[DBController.sessionId]] = 0;
      dateIDMapper[session[DBController.sessionId]] = session[DBController.sessionTimeStart];
    }

    for (Map<String, dynamic> element in data) {
      grades[element[DBController.sessionId]] = max(
        element[DBController.gradeId],
        grades[element[DBController.sessionId]],
      );
    }

    List<DateTimeDataPoint> out = [];
    for (int sessionID in grades.keys) {
      double bestGrade =
          grades[sessionID] == 0 ? null : GradeManager().getGradeMapper().normalize(grades[sessionID]).toDouble();
      DateTime start = DateTime.parse(dateIDMapper[sessionID]);
      out.add(DateTimeDataPoint(start, bestGrade));
    }

    return out;
  }
}

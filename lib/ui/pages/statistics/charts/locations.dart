import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/ui/pages/statistics/abstract_charts/pie_chart.dart';
import 'package:flutter/material.dart';

class LocationsChart extends StatelessWidget {
  const LocationsChart({Key key, this.extended}) : super(key: key);

  final bool extended;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: fetchData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }

          return CustomPieChart(this.extended, _calculateData(snapshot.data));
        });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
        "SELECT ${DBController.sessionLocation} FROM ${DBController.sessionTable} "
        "WHERE ${DBController.sessionStatus} != (?) ",
        [Data.SESSION_ACTIVE_STATE]);
  }

  Map<String, double> _calculateData(List<Map<String, dynamic>> data) {
    Map<String, double> locations = Map();
    Data().getLocations().forEach((element) {
      locations[element] = 0;
    });

    for (Map<String, dynamic> session in data) {
      locations[session[DBController.sessionLocation]] += 1;
    }
    stripData(locations, data.length);
    return locations;
  }

  void stripData(Map<String, double> map, int totalEntries) {
    double others = 0;
    List<String> remove = [];
    map.forEach((key, value) {
      if (value / totalEntries < 0.03) {
        others += value;
        remove.add(key);
      }
    });
    remove.forEach((element) {
      map.remove(element);
    });
    if (others > 0) {
      map["Others"] = others;
    }
  }
}

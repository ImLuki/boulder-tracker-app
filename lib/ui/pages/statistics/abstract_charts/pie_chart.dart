import 'package:boulder_track/app/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';

class CustomPieChart extends StatelessWidget {
  final bool expanded;
  final Map<String, double> data;
  final List<Color> colors;

  CustomPieChart(this.expanded, this.data, {this.colors});

  @override
  Widget build(BuildContext context) {
    return createPieChar();
  }

  Widget createPieChar() {
    return PieChart(
        legendOptions:
            LegendOptions(showLegends: expanded, legendPosition: LegendPosition.bottom, showLegendsInRow: true),
        chartValuesOptions: ChartValuesOptions(
            showChartValues: expanded, showChartValuesOutside: true, showChartValuesInPercentage: true),
        dataMap: data,
        chartType: ChartType.ring,
        ringStrokeWidth: expanded ? 60.0 : 15.0,
        colorList: colors == null
            ? (data.containsKey("Others")
                ? AppColors.chart_colors.take(data.length - 1).toList() + [Colors.grey]
                : AppColors.chart_colors.take(data.length).toList())
            : colors);
  }
}

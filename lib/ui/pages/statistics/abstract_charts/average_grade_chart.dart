import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AverageGradeChart extends StatefulWidget {
  final bool expanded;
  final List<double> data;
  final List<DateTime> dates;

  AverageGradeChart(this.expanded, this.data, this.dates);

  @override
  _AverageGradeChartState createState() => _AverageGradeChartState();
}

class _AverageGradeChartState extends State<AverageGradeChart> {
  static const double Y_LABELS_INTERVAL = 4;
  bool _flatPoints = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setState(() => this._flatPoints = false));
  }

  @override
  Widget build(BuildContext context) {
    return createLineChart();
  }

  Widget createLineChart() {
    return Stack(
      children: [
        Container(
            padding: EdgeInsets.only(left: 0, right: 20),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: LineChart(
                LineChartData(
                  lineTouchData: LineTouchData(
                      enabled: widget.expanded,
                      touchTooltipData: LineTouchTooltipData(
                          tooltipBgColor: Colors.grey.withOpacity(0.8),
                          getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
                            return touchedBarSpots.map((barSpot) {
                              return LineTooltipItem(
                                '${GradeManager().getGradeMapper().grades[barSpot.y.round()]}',
                                const TextStyle(color: Colors.white),
                              );
                            }).toList();
                          }),
                      touchCallback: (FlTouchEvent event, LineTouchResponse touchResponse) {}),
                  borderData: FlBorderData(show: false),
                  gridData: FlGridData(
                      show: widget.expanded, drawVerticalLine: false, horizontalInterval: Y_LABELS_INTERVAL - 2),
                  titlesData: FlTitlesData(
                    show: widget.expanded,
                    bottomTitles: SideTitles(
                      interval: 1,
                      showTitles: widget.expanded,
                      reservedSize: 60,
                      rotateAngle: 75,
                      getTextStyles: (context, value) =>
                          const TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 12),
                      getTitles: (value) {
                        return DateFormat("dd.MM.yy").format(widget.dates.reversed.elementAt(value.toInt()));
                      },
                      margin: 15,
                    ),
                    rightTitles: SideTitles(showTitles: false),
                    topTitles: SideTitles(showTitles: false),
                    leftTitles: SideTitles(
                      showTitles: true,
                      getTextStyles: (context, value) => const TextStyle(
                        color: Color(0xff67727d),
                        fontWeight: FontWeight.normal,
                        fontSize: 12,
                      ),
                      interval: 5,
                      getTitles: (value) {
                        return GradeManager().getGradeMapper().getLabels()[value.toInt()];
                      },
                      reservedSize: 20,
                      margin: 12,
                    ),
                  ),
                  minY: 0,
                  maxY: GradeManager().getGradeMapper().maxNormalized.toDouble(),
                  lineBarsData: [
                    LineChartBarData(
                      spots: List.generate(widget.data.length,
                          (i) => FlSpot(i.toDouble(), _flatPoints ? 0 : widget.data.reversed.elementAt(i))),
                      isCurved: false,
                      colors: AppColors.chart_colors.take(2).toList(),
                      barWidth: Y_LABELS_INTERVAL,
                      isStrokeCapRound: true,
                      dotData: FlDotData(
                        show: widget.expanded,
                      ),
                      belowBarData: BarAreaData(
                        show: widget.expanded,
                        colors: AppColors.chart_colors.take(2).map((e) => e.withOpacity(0.3)).toList(),
                      ),
                    ),
                  ],
                ),
                swapAnimationDuration: Duration(milliseconds: 500))),
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }
}

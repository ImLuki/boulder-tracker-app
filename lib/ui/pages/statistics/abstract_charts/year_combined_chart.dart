import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:flutter/widgets.dart';
import 'data_points.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class YearBarChart extends StatefulWidget {
  final List<List<dynamic>> data;
  final bool expanded;
  final String lineLegendOne;
  final String lineLegendTwo;
  final String barLegendOne;
  final String barLegendTwo;
  final bool reorderMonths;

  YearBarChart(
    this.expanded,
    this.data,
    this.lineLegendOne,
    this.barLegendOne, {
    this.lineLegendTwo,
    this.barLegendTwo,
    this.reorderMonths = false,
  });

  @override
  _YearBarChartState createState() => _YearBarChartState();
}

class _YearBarChartState extends State<YearBarChart> {
  List<String> months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  static const String CUSTOM_LINE_RENDERER = "custom_line_renderer";
  static const String SECOND_AXIS = 'secondaryMeasureAxisId';
  List<DataPoint> lineDataOne;
  List<DataPoint> lineDataTwo;
  List<DataPoint> barDataOne;
  List<DataPoint> barDataTwo;
  int currentMonth;

  @override
  void initState() {
    super.initState();
    _calculateDataPoints();
  }

  @override
  Widget build(BuildContext context) {
    return _createBarChart();
  }

  void _calculateDataPoints() {
    lineDataOne = [];
    lineDataTwo = [];
    barDataOne = [];
    barDataTwo = [];
    currentMonth = widget.reorderMonths ? DateTime.now().month : 12;
    for (int i = currentMonth; i < currentMonth + 12; i++) {
      int index = i % 12;

      if (widget.data.length > 2) {
        lineDataOne.add(DataPoint(index.toDouble(), widget.data[0].elementAt(index)));
        lineDataTwo.add(DataPoint(index.toDouble(), widget.data[1].elementAt(index)));
        barDataOne.add(DataPoint(index.toDouble(), widget.data[2].elementAt(index)));
        barDataTwo.add(DataPoint(index.toDouble(), widget.data[3].elementAt(index)));
      } else {
        lineDataOne.add(DataPoint(index.toDouble(), widget.data[0].elementAt(index)));
        barDataOne.add(DataPoint(index.toDouble(), widget.data[1].elementAt(index)));
      }
    }
  }

  List<charts.Series<DataPoint, String>> _getSeriesData() {
    return [
      new charts.Series<DataPoint, String>(
          id: widget.barLegendOne,
          domainFn: (DataPoint value, _) => months[value.xPoint.toInt()],
          measureFn: (DataPoint value, _) => value.yPoint,
          data: barDataOne,
          colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[2])),
      if (widget.data.length > 2) ...[
        new charts.Series<DataPoint, String>(
            id: widget.barLegendTwo,
            domainFn: (DataPoint value, _) => months[value.xPoint.toInt()],
            measureFn: (DataPoint value, _) => value.yPoint,
            data: barDataTwo,
            colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[0]))
      ],
      new charts.Series<DataPoint, String>(
          id: widget.lineLegendOne,
          domainFn: (DataPoint value, _) => months[value.xPoint.toInt()],
          measureFn: (DataPoint value, _) => value.yPoint,
          data: lineDataOne,
          colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[3]))
        ..setAttribute(charts.rendererIdKey, CUSTOM_LINE_RENDERER)
        ..setAttribute(charts.measureAxisIdKey, SECOND_AXIS),
      if (widget.data.length > 2) ...[
        new charts.Series<DataPoint, String>(
            id: widget.lineLegendTwo,
            domainFn: (DataPoint value, _) => months[value.xPoint.toInt()],
            measureFn: (DataPoint value, _) => value.yPoint,
            data: lineDataTwo,
            colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[1]))
          ..setAttribute(charts.rendererIdKey, CUSTOM_LINE_RENDERER)
          ..setAttribute(charts.measureAxisIdKey, SECOND_AXIS)
      ],
    ];
  }

  Widget _createBarChart() {
    final labels = charts.BasicNumericTickFormatterSpec((num value) {
      if (value.toInt() <= 0 || value.toInt() >= GradeManager().getGradeMapper().getLabels().length) {
        return "";
      }
      return GradeManager().getGradeMapper().getLabels()[value.toInt()];
    });
    return charts.OrdinalComboChart(_getSeriesData(),
        layoutConfig: widget.expanded
            ? null
            : charts.LayoutConfig(
                leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                topMarginSpec: charts.MarginSpec.fixedPixel(0),
                rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                bottomMarginSpec: charts.MarginSpec.fixedPixel(0),
              ),
        animate: true,
        defaultInteractions: false,
        behaviors: widget.expanded
            ? [
                charts.SeriesLegend(
                    cellPadding: EdgeInsets.only(right: 10),
                    showMeasures: true,
                    horizontalFirst: false,
                    desiredMaxRows: 2,
                    secondaryMeasureFormatter: (num value) {
                      return value == 0.0 ? '-' : GradeManager().getGradeMapper().grades[value.toInt()];
                    }),
                charts.InitialSelection(selectedDataConfig: [
                  charts.SeriesDatumConfig<String>(widget.barLegendOne, months[currentMonth - 1]),
                  if (widget.data.length > 2) ...[
                    charts.SeriesDatumConfig<String>(widget.barLegendTwo, months[currentMonth - 1])
                  ],
                  charts.SeriesDatumConfig<String>(widget.lineLegendOne, months[currentMonth - 1]),
                  if (widget.data.length > 2) ...[
                    charts.SeriesDatumConfig<String>(widget.lineLegendTwo, months[currentMonth - 1])
                  ],
                ]),
                charts.SelectNearest(eventTrigger: charts.SelectionTrigger.tapAndDrag),
                charts.DomainHighlighter()
              ]
            : [],
        primaryMeasureAxis: new charts.NumericAxisSpec(
            renderSpec: widget.expanded ? charts.GridlineRendererSpec() : charts.NoneRenderSpec()),
        secondaryMeasureAxis: new charts.NumericAxisSpec(
            viewport: charts.NumericExtents.fromValues([0, GradeManager().getGradeMapper().maxNormalized]),
            tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 8),
            tickFormatterSpec: labels,
            renderSpec: widget.expanded
                ? charts.SmallTickRendererSpec(
                    tickLengthPx: 0, axisLineStyle: charts.LineStyleSpec(color: charts.Color.transparent))
                : charts.NoneRenderSpec()),
        domainAxis: new charts.OrdinalAxisSpec(
          renderSpec: widget.expanded ? charts.SmallTickRendererSpec(labelRotation: 40) : charts.NoneRenderSpec(),
        ),
        defaultRenderer: new charts.BarRendererConfig(groupingType: charts.BarGroupingType.stacked),
        customSeriesRenderers: [
          new charts.LineRendererConfig(
              includePoints: true,
              strokeWidthPx: 4.0,
              radiusPx: 5.5,
              symbolRenderer: charts.CircleSymbolRenderer(),
              // ID used to link series to this renderer.
              customRendererId: CUSTOM_LINE_RENDERER)
        ]);
  }
}

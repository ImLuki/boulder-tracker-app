import 'dart:math';

import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'data_points.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DateTimeBarChart extends StatefulWidget {
  final List<List<dynamic>> data;
  final bool expanded;

  DateTimeBarChart(this.data, this.expanded);

  @override
  _DateTimeBarChartState createState() => _DateTimeBarChartState();
}

class _DateTimeBarChartState extends State<DateTimeBarChart> {
  static const int STANDARD_LENGTH = 16;
  List<DateTimeDataPoint> leadData;
  List<DateTimeDataPoint> data;

  @override
  void initState() {
    super.initState();
    _calculateDataPoints();
  }

  @override
  Widget build(BuildContext context) {
    return _createBarChart();
  }

  void _calculateDataPoints() {
    data = [];

    for (int i = Data().limitCharts ? max(0, widget.data[0].length - Data.MAX_SESSION_AMOUNT - 1) : 0;
        i < widget.data[0].length;
        i++) {
      data.add(DateTimeDataPoint(widget.data[0].elementAt(i), widget.data[1].elementAt(i)));
    }
  }

  List<charts.Series<DateTimeDataPoint, String>> _getSeriesData() {
    return [
      new charts.Series<DateTimeDataPoint, String>(
          id: 'count: ',
          domainFn: (DateTimeDataPoint value, _) => DateFormat("dd.MM.yy").format(value.xPoint),
          measureFn: (DateTimeDataPoint value, _) => value.yPoint,
          data: data,
          colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[0])),
    ];
  }

  Widget _createBarChart() {
    return charts.BarChart(_getSeriesData(),
        layoutConfig: widget.expanded
            ? null
            : charts.LayoutConfig(
                leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                topMarginSpec: charts.MarginSpec.fixedPixel(0),
                rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                bottomMarginSpec: charts.MarginSpec.fixedPixel(0),
              ),
        animate: true,
        defaultInteractions: widget.expanded,
        behaviors: widget.expanded
            ? [
                charts.SeriesLegend(
                    cellPadding: EdgeInsets.zero,
                    desiredMaxColumns: 1,
                    desiredMaxRows: 2,
                    horizontalFirst: false,
                    showMeasures: true),
                charts.InitialSelection(selectedDataConfig: [
                  charts.SeriesDatumConfig<String>('count: ', DateFormat("dd.MM.yy").format(data.last.xPoint)),
                ]),
                //charts.SlidingViewport(),
                charts.PanAndZoomBehavior()
              ]
            : [],
        primaryMeasureAxis: new charts.NumericAxisSpec(
            renderSpec: widget.expanded ? charts.GridlineRendererSpec() : charts.NoneRenderSpec()),
        domainAxis: new charts.OrdinalAxisSpec(
          viewport: charts.OrdinalViewport(
              DateFormat("dd.MM.yy").format(widget.data[0][max(0, widget.data[0].length - STANDARD_LENGTH)]),
              min(STANDARD_LENGTH, widget.data[0].length)),
          renderSpec: widget.expanded ? charts.SmallTickRendererSpec(labelRotation: 75) : charts.NoneRenderSpec(),
        ),
        defaultRenderer: new charts.BarRendererConfig(groupingType: charts.BarGroupingType.stacked));
  }
}

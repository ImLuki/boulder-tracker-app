import 'dart:math';

import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/util/style_mapper.dart';
import 'package:flutter/widgets.dart';
import 'data_points.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class BarChart extends StatefulWidget {
  final List<List<dynamic>> data;
  final bool expanded;

  BarChart(this.data, this.expanded);

  @override
  _BarChartState createState() => _BarChartState();
}

class _BarChartState extends State<BarChart> {
  static const int STANDARD_LENGTH = 10;
  List<List<DataPoint>> dataPoints;
  int maxHeight = 0;

  @override
  void initState() {
    super.initState();
    _calculateDataPoints();
  }

  @override
  Widget build(BuildContext context) {
    return _createBarChart();
  }

  void _calculateDataPoints() {
    dataPoints = [];

    for (int j = 1; j < widget.data.length; j++) {
      dataPoints.add([]);
    }

    for (int i = 0; i < widget.data[0].length; i++) {
      int tmp = 0;
      for (int j = 1; j < widget.data.length; j++) {
        dataPoints[j - 1].add(DataPoint(i.toDouble(), widget.data[j][i].toDouble()));
        tmp += widget.data[j][i];
      }
      this.maxHeight = max(this.maxHeight, tmp);
    }
  }

  List<charts.Series<DataPoint, String>> _getSeriesData() {
    return List.generate(
      widget.data.length - 1,
      (index) => new charts.Series<DataPoint, String>(
        id: '${StyleMapper().mapping[index]}: ',
        domainFn: (DataPoint value, _) => widget.data[0][value.xPoint.toInt()],
        measureFn: (DataPoint value, _) => value.yPoint,
        data: dataPoints[index],
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[index]),
      ),
    );
  }

  Widget _createBarChart() {
    return charts.BarChart(_getSeriesData(),
        layoutConfig: widget.expanded
            ? null
            : charts.LayoutConfig(
                leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                topMarginSpec: charts.MarginSpec.fixedPixel(0),
                rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                bottomMarginSpec: charts.MarginSpec.fixedPixel(0),
              ),
        animate: true,
        defaultInteractions: widget.expanded,
        behaviors: widget.expanded
            ? [
                charts.SeriesLegend(
                    cellPadding: EdgeInsets.all(4.0),
                    desiredMaxColumns: 3,
                    desiredMaxRows: 3,
                    horizontalFirst: false,
                    showMeasures: true),
                charts.PanAndZoomBehavior()
              ]
            : [],
        primaryMeasureAxis: charts.NumericAxisSpec(
            viewport: charts.NumericExtents.fromValues([0, this.maxHeight]),
            //tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 8),
            renderSpec: widget.expanded ? charts.GridlineRendererSpec() : charts.NoneRenderSpec()),
        domainAxis: new charts.OrdinalAxisSpec(
          viewport: charts.OrdinalViewport("XX", STANDARD_LENGTH),
          renderSpec: widget.expanded ? charts.SmallTickRendererSpec(labelRotation: 75) : charts.NoneRenderSpec(),
        ),
        defaultRenderer: new charts.BarRendererConfig(groupingType: charts.BarGroupingType.stacked));
  }
}

import 'dart:math';

import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'data_points.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class LineChart extends StatefulWidget {
  final List<DateTimeDataPoint> data1;
  final List<DateTimeDataPoint> data2;
  final bool expanded;
  final String legendOne;
  final String legendTwo;
  final measureFormatting;
  final bool useGradeLabels;

  static const int STANDARD_LENGTH = 16;

  LineChart(
    this.data1,
    this.expanded,
    this.legendOne, {
    this.data2 = const [],
    this.legendTwo,
    this.measureFormatting,
    this.useGradeLabels = false,
  });

  @override
  _LineChartState createState() => _LineChartState();
}

class _LineChartState extends State<LineChart> {
  bool animated;

  @override
  void initState() {
    super.initState();
    animated = true;
  }

  @override
  Widget build(BuildContext context) {
    return _createChart();
  }

  List<charts.Series<DateTimeDataPoint, int>> _getSeriesData() {
    if (widget.data2.isEmpty) {
      return [
        new charts.Series<DateTimeDataPoint, int>(
            id: widget.legendOne,
            domainFn: (DateTimeDataPoint value, _) => widget.data1.indexOf(value),
            measureFn: (DateTimeDataPoint value, _) => value.yPoint,
            data: widget.data1,
            colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[0])),
      ];
    } else {
      return [
        new charts.Series<DateTimeDataPoint, int>(
            id: widget.legendTwo,
            domainFn: (DateTimeDataPoint value, _) => widget.data2.indexOf(value),
            measureFn: (DateTimeDataPoint value, _) => value.yPoint,
            data: widget.data2,
            colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[0])),
        new charts.Series<DateTimeDataPoint, int>(
            id: widget.legendOne,
            domainFn: (DateTimeDataPoint value, _) => widget.data1.indexOf(value),
            measureFn: (DateTimeDataPoint value, _) => value.yPoint,
            data: widget.data1,
            colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.chart_colors[2])),
      ];
    }
  }

  Widget _createChart() {
    final customTickFormatter = charts.BasicNumericTickFormatterSpec((num value) {
      if (value.toInt() < 0 || value.toInt() >= widget.data1.length) {
        return "";
      }
      return DateFormat("dd.MM.yy").format(widget.data1[value.toInt()].xPoint);
    });
    final labels = charts.BasicNumericTickFormatterSpec((num value) {
      if (widget.useGradeLabels) {
        if (value.toInt() <= 0 || value.toInt() >= GradeManager().getGradeMapper().getLabels().length) {
          return "";
        }
        return widget.measureFormatting == null
            ? GradeManager().getGradeMapper().getLabels()[value.toInt()]
            : "${value.toInt()}";
      } else {
        if (value.toInt() <= 0) {
          return "";
        } else {
          return value.toInt().toString();
        }
      }
    });
    return charts.LineChart(_getSeriesData(),
        layoutConfig: widget.expanded
            ? null
            : charts.LayoutConfig(
                leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                topMarginSpec: charts.MarginSpec.fixedPixel(0),
                rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                bottomMarginSpec: charts.MarginSpec.fixedPixel(0),
              ),
        animate: animated,
        defaultInteractions: widget.expanded,
        behaviors: widget.expanded
            ? [
                charts.SeriesLegend(
                    cellPadding: EdgeInsets.zero,
                    desiredMaxColumns: 1,
                    desiredMaxRows: 2,
                    horizontalFirst: false,
                    showMeasures: true,
                    measureFormatter: (num value) {
                      return value == 0.0
                          ? '-'
                          : (widget.measureFormatting == null
                              ? GradeManager().getGradeMapper().grades[value.toInt()]
                              : "${value.toInt()} ${widget.measureFormatting}");
                    }),
                charts.InitialSelection(
                    selectedDataConfig: widget.data2.isEmpty
                        ? [charts.SeriesDatumConfig<int>(widget.legendOne, widget.data1.length - 1)]
                        : [
                            charts.SeriesDatumConfig<int>(widget.legendOne, widget.data1.length - 1),
                            charts.SeriesDatumConfig<int>(widget.legendTwo, widget.data2.length - 1)
                          ]),
                //charts.SlidingViewport(),
                charts.PanAndZoomBehavior()
              ]
            : [],
        primaryMeasureAxis: charts.NumericAxisSpec(
            viewport: widget.data2.isEmpty
                ? null
                : charts.NumericExtents.fromValues([0, GradeManager().getGradeMapper().maxNormalized]),
            tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: 8),
            tickFormatterSpec: labels,
            renderSpec: widget.expanded ? charts.GridlineRendererSpec() : charts.NoneRenderSpec()),
        domainAxis: charts.NumericAxisSpec(
          viewport: charts.NumericExtents(
              max(0, widget.data1.length.toDouble() - LineChart.STANDARD_LENGTH), widget.data1.length.toDouble() - 1),
          tickProviderSpec: charts.BasicNumericTickProviderSpec(desiredTickCount: widget.data1.length),
          tickFormatterSpec: customTickFormatter,
          renderSpec: widget.expanded ? charts.SmallTickRendererSpec(labelRotation: 75) : charts.NoneRenderSpec(),
        ),
        defaultRenderer: new charts.LineRendererConfig(
            includeArea: widget.expanded ? widget.data2.isEmpty : false,
            includePoints: true,
            strokeWidthPx: 4.0,
            radiusPx: 5.5,
            symbolRenderer: charts.CircleSymbolRenderer()));
  }
}

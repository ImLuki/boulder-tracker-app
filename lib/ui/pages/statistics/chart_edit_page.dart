import 'package:boulder_track/app/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'chart_page.dart';

class ChartEditPage extends StatefulWidget {
  const ChartEditPage({Key key, this.elements, this.active, this.order}) : super(key: key);

  @override
  State<ChartEditPage> createState() => _ChartEditPageState(order.map((e) => Item(elements[e], e, active[e])).toList());

  final List<String> elements;
  final List<bool> active;
  final List<int> order;
}

class Item {
  final String title;
  final int number;
  bool active;

  Item(this.title, this.number, this.active);
}

class _ChartEditPageState extends State<ChartEditPage> {
  final List<Item> _items;
  GlobalKey _tipKey = GlobalObjectKey("tipKey");

  _ChartEditPageState(this._items);

  @override
  Widget build(BuildContext context) {
    //showing tooltip
    if (this._tipKey != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTipAsync());
    }

    return WillPopScope(
      onWillPop: () async {
        await _storeNewValues();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: AppColors.accentColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Chart Settings", style: TextStyle(color: Colors.white, fontSize: 22)),
            ],
          ),
          actions: [
            IconButton(
              onPressed: () {
                this._items.sort((a, b) => a.number.compareTo(b.number));
                setState(() {});
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(SnackBar(content: Text("Reset chart order")));
              },
              icon: Icon(Icons.refresh),
            ),
          ],
        ),
        body: _createReorderableList(),
      ),
    );
  }

  Widget _createReorderableList() {
    return ReorderableListView(
      key: _tipKey,
      children: <Widget>[
        for (int index = 0; index < _items.length; index++)
          ListTile(
            key: Key('$index'),
            title: Text(
              _items[index].title,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(fontSize: 20),
            ),
            leading: Checkbox(
              activeColor: Colors.blue,
              onChanged: (bool value) {
                setState(() {
                  _items[index].active = value;
                });
              },
              value: _items[index].active,
            ),
            trailing: Icon(Icons.menu),
          ),
      ],
      onReorder: (int oldIndex, int newIndex) {
        setState(() {
          if (oldIndex < newIndex) {
            newIndex -= 1;
          }
          final Item item = _items.removeAt(oldIndex);
          _items.insert(newIndex, item);
        });
      },
    );
  }

  Future<void> _storeNewValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String order = _items.map((e) => e.number).join("_");
    String active = _items.map((e) => e.active ? "1" : "0").join("");

    prefs.setString(ChartPage.ACTIVE_KEY, active);
    prefs.setString(ChartPage.ORDER_KEY, order);
  }

  void _showToolTip() {
    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = this._tipKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect = Rect.fromCenter(center: markRect.center, width: markRect.width * 0, height: markRect.height * 0);

    coachMarkFAB.show(
        targetContext: this._tipKey.currentContext,
        markRect: markRect,
        markShape: BoxShape.rectangle,
        children: [
          Center(
              child: Text("Tip:\nPress and hold an item\nto select it\nand move it to change\nthe order of the charts.",
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontStyle: FontStyle.italic,
                    color: Colors.white,
                  )))
        ],
        duration: Duration(seconds: 5));
  }

  Future<void> _showToolTipAsync() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("tool_tip_chart_settings") ?? true) {
      prefs.setBool("tool_tip_chart_settings", false);
      _showToolTip();
    }
  }
}

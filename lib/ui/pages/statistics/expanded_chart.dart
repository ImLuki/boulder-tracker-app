import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/pages/statistics/charts/average_grades.dart';
import 'package:boulder_track/ui/pages/statistics/charts/best_grades_bouldering.dart';
import 'package:boulder_track/ui/pages/statistics/charts/grades_distribution.dart';
import 'package:boulder_track/ui/pages/statistics/charts/indoor_outdoor.dart';
import 'package:boulder_track/ui/pages/statistics/charts/last_sessions.dart';
import 'package:boulder_track/ui/pages/statistics/charts/last_year_boulder.dart';
import 'package:boulder_track/ui/pages/statistics/charts/locations.dart';
import 'package:boulder_track/ui/pages/statistics/charts/session_time.dart';
import 'package:boulder_track/ui/pages/statistics/charts/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ExpandedChart extends StatefulWidget {
  const ExpandedChart({Key key, this.index, this.title, this.yearList}) : super(key: key);

  final int index;
  final String title;
  final List<String> yearList;

  @override
  _ExpandedChartState createState() => _ExpandedChartState();
}

class _ExpandedChartState extends State<ExpandedChart> {
  String currentYear = "Past 12 Months";

  List<Widget> _chartListExpanded;

  @override
  Widget build(BuildContext context) {
    DateTime from = currentYear == "Past 12 Months" ? null : DateTime(int.parse(currentYear), 1, 1, 0);
    DateTime to = currentYear == "Past 12 Months" ? null : DateTime(int.parse(currentYear), 12, 31, 23, 59);

    _chartListExpanded = [
      BoulderBestGradeChart(extended: true),
      Container(padding: EdgeInsets.only(left: 50, right: 50, bottom: 20, top: 20), child: StylesChart(extended: true)),
      LastSessionsBoulderCountChart(extended: true),
      Container(
          padding: EdgeInsets.only(left: 50, right: 50, bottom: 20, top: 20), child: IndoorOutdoor(extended: true)),
      SessionTimeChart(extended: true),
      Container(
        padding: EdgeInsets.only(left: 50, right: 50, bottom: 20, top: 20),
        child: LocationsChart(extended: true),
      ),
      AverageGrades(extended: true),
      Container(
          padding: EdgeInsets.only(right: 10, bottom: 20),
          child: BoulderPastYearChart(
            extended: true,
            from: from,
            to: to,
          )),
      Container(
        padding: EdgeInsets.only(right: 10, bottom: 20),
        child: GradesDistributionChart(extended: true),
      ),
    ];

    return Scaffold(
      appBar: AppBar(
        actions: [
          if (widget.title.contains("Past 12 Months")) ...[
            Padding(
              padding: EdgeInsets.only(top: 4.0),
              child: DropdownButton(
                alignment: Alignment.centerRight,
                iconEnabledColor: Colors.white,
                dropdownColor: Colors.black,
                value: this.currentYear,
                underline: Container(),
                style: TextStyle(fontSize: 24, color: Colors.white, fontWeight: FontWeight.w600),
                onChanged: (value) => setState(() {
                  this.currentYear = value;
                }),
                items: widget.yearList.map((String item) {
                  return DropdownMenuItem<String>(
                    child: Text(item),
                    value: item,
                  );
                }).toList(),
              ),
            ),
            const SizedBox(width: Constants.STANDARD_PADDING),
          ]
        ],
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: AppColors.accentColor,
        iconTheme: IconThemeData(color: Colors.white),
        title: widget.title.contains("Past 12 Months")
            ? Container()
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Text(
                      widget.title,
                      style: TextStyle(color: Colors.white, fontSize: 22, overflow: TextOverflow.ellipsis),
                    ),
                  ),
                ],
              ),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 10, top: 15),
        child: _chartListExpanded[widget.index],
        color: Colors.white,
      ),
    );
  }
}

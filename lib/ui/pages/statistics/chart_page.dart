import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/ui/pages/statistics/chart_edit_page.dart';
import 'package:boulder_track/ui/pages/statistics/charts/average_grades.dart';
import 'package:boulder_track/ui/pages/statistics/charts/best_grades_bouldering.dart';
import 'package:boulder_track/ui/pages/statistics/expanded_chart.dart';
import 'package:boulder_track/ui/pages/statistics/charts/last_sessions.dart';
import 'package:boulder_track/ui/pages/statistics/charts/last_year_boulder.dart';
import 'package:boulder_track/ui/pages/statistics/charts/locations.dart';
import 'package:boulder_track/ui/pages/statistics/charts/session_time.dart';
import 'package:boulder_track/ui/pages/statistics/charts/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'charts/grades_distribution.dart';
import 'charts/indoor_outdoor.dart';

class ChartPage extends StatefulWidget {
  ChartPage({Key key}) : super(key: key);

  @override
  _ChartPageState createState() => _ChartPageState();

  static const List<String> TITLE_LIST = [
    "Best Grades",
    "Styles",
    "Boulder Count",
    "Indoor/Outdoor",
    "Session Time",
    "Locations",
    "Average Grade",
    "Past 12 Months",
    "Grade Distribution",
  ];

  //shared prefs parameter
  static const String ORDER_KEY = "chart_order";
  static const String ACTIVE_KEY = "chart_active";

  static String buildStandardOrder() {
    return List<String>.generate(TITLE_LIST.length, (index) => "$index").join("_");
  }

  static String buildStandardActive() {
    return List<String>.generate(TITLE_LIST.length, (index) => "1").join("");
  }
}

class _ChartPageState extends State<ChartPage> {
  static const String FIRST_YEAR_KEY = "first_year";

  List<String> yearList = ["Past 12 Months"];

  final List<Widget> _chartList = [
    BoulderBestGradeChart(extended: false),
    StylesChart(extended: false),
    LastSessionsBoulderCountChart(extended: false),
    IndoorOutdoor(extended: false),
    SessionTimeChart(extended: false),
    LocationsChart(extended: false),
    AverageGrades(extended: false),
    BoulderPastYearChart(extended: false),
    GradesDistributionChart(extended: false),
  ];

  static const List<bool> PADDING = [false, true, false, true, false, true, false, false, false];

  Future futureData;

  @override
  void initState() {
    super.initState();
    this.futureData = _loadSharedPreferences();
  }

  @override
  Widget build(BuildContext context) {
    List<bool> activeList = [];
    List<int> orderListAll = [];

    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(
              icon: Icon(Icons.edit),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute<void>(
                  builder: (BuildContext context) => ChartEditPage(
                    elements: ChartPage.TITLE_LIST,
                    active: activeList,
                    order: orderListAll,
                  ),
                ),
              ).then((value) => setState(() {})),
            ),
          ],
          systemOverlayStyle: SystemUiOverlayStyle.light,
          centerTitle: true,
          backgroundColor: AppColors.accentColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: _getTitle(),
        ),
        body: FutureBuilder(
            future: this.futureData,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Container(
                  child: Align(child: LinearProgressIndicator(), alignment: Alignment.topCenter),
                );
              }

              SharedPreferences prefs = snapshot.data;

              // reset
              //prefs.remove(ChartPage.ORDER_KEY);
              //prefs.remove(ChartPage.ACTIVE_KEY);

              String order = prefs.getString(ChartPage.ORDER_KEY) ?? ChartPage.buildStandardOrder();
              String active = prefs.getString(ChartPage.ACTIVE_KEY) ?? ChartPage.buildStandardActive();

              //TODO this better
              if (active.length != ChartPage.TITLE_LIST.length) {
                prefs.remove(ChartPage.ORDER_KEY);
                prefs.remove(ChartPage.ACTIVE_KEY);
                order = ChartPage.buildStandardOrder();
                active = ChartPage.buildStandardActive();
              }

              orderListAll.addAll(order.split("_").map(int.parse));
              List<int> orderList = List.from(orderListAll);
              activeList.addAll(active.split("").map((e) => e == "1"));

              for (int i = 0; i < activeList.length; i++) {
                if (!activeList[i]) {
                  orderList.remove(i);
                }
              }

              _loadYears(prefs);

              return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
                return Data().sessionCount < 3 ? _createNoDataWarning() : _createBody(context, orderList);
              });
            }));
  }

  Widget _getTitle() {
    return Text("STATISTICS", style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.center);
  }

  Widget _createNoDataWarning() {
    return Align(
        alignment: Alignment.center,
        child: Container(
            width: MediaQuery.of(context).size.width * 0.75,
            alignment: Alignment.center,
            child: Text("NOT ENOUGH DATA AVAILABLE",
                style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.black38),
                textAlign: TextAlign.center)));
  }

  Widget _createBody(BuildContext context, List<int> indices) {
    List<Widget> list = _chartListWithContainer(indices);
    return ScrollablePositionedList.builder(
        itemCount: list.length,
        physics: const AlwaysScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return list[index];
        });
  }

  List<Widget> _chartListWithContainer(List<int> indices) {
    List<Widget> list = List.generate(
      indices.length,
      (index) => _createChartContainer(
        _chartList[indices[index]],
        indices[index],
        ChartPage.TITLE_LIST[indices[index]],
        PADDING[indices[index]],
      ),
    );
    list.add(SizedBox(height: 8.0));
    return list;
  }

  Widget _createChartContainer(Widget chart, int index, String name, bool padding) {
    return InkWell(
      onTap: () => showChart(index),
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        margin: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
        padding: EdgeInsets.all(8.0),
        width: MediaQuery.of(context).size.width,
        height: 100,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black12, width: 2), // added
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Column(
          children: [
            Flexible(
                child: Container(child: chart, padding: EdgeInsets.fromLTRB(50.0, 0, 50.0, 0)), fit: FlexFit.tight),
            SizedBox(height: 10.0),
            Text(name, maxLines: 1, overflow: TextOverflow.visible)
          ],
        ),
      ),
    );
  }

  void showChart(int index) {
    Navigator.of(context).push(createChartPage(index));
  }

  Route createChartPage(int index) {
    return PageRouteBuilder(
        transitionDuration: Duration.zero,
        pageBuilder: (context, animation, secondaryAnimation) {
          return ExpandedChart(index: index, title: ChartPage.TITLE_LIST[index], yearList: this.yearList);
        });
  }

  Future<SharedPreferences> _loadSharedPreferences() async {
    await Future.delayed(Duration(milliseconds: 600)); //to have no white screen effect
    return await SharedPreferences.getInstance();
  }

  void _loadYears(SharedPreferences sharedPreferences) async {
    int lastYear = sharedPreferences.getInt(FIRST_YEAR_KEY);

    DateTime now = DateTime.now();

    if (lastYear == null) {
      List<Map<String, dynamic>> result =
          await DBController().queryRows(DBController.sessionTable, orderBy: DBController.sessionTimeStart, limit: 1);
      if (result.isEmpty) {
        lastYear = now.year;
      } else {
        DateTime dateTime = DateTime.parse(result.first[DBController.sessionTimeStart]);
        lastYear = dateTime.year;
      }

      sharedPreferences.setInt(FIRST_YEAR_KEY, lastYear);
    }
    if (lastYear > now.year) lastYear = now.year;

    List<String> years = List.generate(now.year - lastYear + 1, (index) => "${lastYear + index}");
    years.add("Past 12 Months");
    this.yearList = years.reversed.toList();
  }
}

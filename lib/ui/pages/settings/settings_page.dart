import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/Location.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/backend/util/json_creator.dart';
import 'package:boulder_track/backend/util/json_loader.dart';
import 'package:boulder_track/ui/dialog/confirm_dialog.dart';
import 'package:boulder_track/ui/dialog/edit_dialog.dart';
import 'package:boulder_track/ui/pages/settings/donation_page.dart';
import 'package:boulder_track/util/notifications_handler.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:store_redirect/store_redirect.dart';
import 'dart:io' show Directory, FileSystemEntity, Platform;
import 'package:path/path.dart' show join;

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String text = "";
  bool automaticBackups = false;
  bool automaticBackupsWithDate = false;
  String versionNumber = "";

  @override
  void initState() {
    super.initState();
    _loadAutomaticBackupValue();
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        versionNumber = packageInfo.version;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            centerTitle: true,
            backgroundColor: AppColors.accentColor,
            iconTheme: IconThemeData(color: Colors.white),
            title: _getTitle()),
        body: SingleChildScrollView(child: _createSettings()));
  }

  Widget _getTitle() {
    return Text("SETTINGS", style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.center);
  }

  Widget _createSettings() {
    return Column(children: [
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Text("BOULDER SYSTEMS", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500))),
      createSettingWidget("Boulder System", "Choose the standard grading system for bouldering", () {
        EditDialog().showGradingSystemsDialog(context);
      }),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Expanded System"),
              Text("If activated more grade steps are available",
                  overflow: TextOverflow.clip, style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
            ])),
            const SizedBox(width: 10.0),
            Switch(
                onChanged: (bool value) {
                  setState(() {
                    GradeManager().getGradeMapper().useExtendedSystem = value;
                  });
                },
                value: GradeManager().getGradeMapper().useExtendedSystem)
          ])),

      Divider(thickness: 2.0),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Text("BACKUP", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500))),
      createSettingWidget("Create Backup", "Export data to the device", _createBackupFile, icon: Icons.download_sharp),
      createSettingWidget("Load Backup", "Restoring the data from a backup file", _loadBackupDialog,
          icon: Icons.upload_sharp),
      createSettingWidget("Delete all Data", "Once deleted, data cannot be restored. A previous backup is recommended",
          _deleteDataDialog,
          icon: Icons.delete_forever),
      Padding(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Dash(
              length: MediaQuery.of(context).size.width * 0.3,
              dashColor: Colors.black26,
              dashLength: 30,
              dashGap: 18,
              dashThickness: 1.4)),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Automatic Backups"),
              Text(
                  "A backup is automatically created after a session has ended. "
                  "Permission to read and write external storage are required for this ",
                  overflow: TextOverflow.clip,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
            ])),
            const SizedBox(width: 10.0),
            Switch(
                onChanged: (bool value) async {
                  setState(() {
                    automaticBackups = value;
                  });
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  prefs.setBool("automatic_backups", value);
                  if (value) {
                    _checkStoragePermissions();
                  }
                },
                value: automaticBackups)
          ])),
      createSettingWidget("Restore Backup", "Restore the last automatically generated backup", _loadAutomaticBackup,
          icon: Icons.restore_page_outlined),
      if (automaticBackups) ...[
        Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Flexible(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text("Delete Automatic Backups"),
                Text("To save space, only the latest backups are saved.",
                    overflow: TextOverflow.clip, style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
              ])),
              const SizedBox(width: 10.0),
              Switch(
                  onChanged: (bool value) async {
                    setState(() {
                      automaticBackupsWithDate = value;
                    });
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setBool("overwrite_automatic_backup", value);
                  },
                  value: automaticBackupsWithDate)
            ])),
      ],
      Divider(thickness: 2.0),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Text("LOCATION", style: const TextStyle(color: Colors.blue, fontWeight: FontWeight.w500))),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Enable Location"),
              Text(
                  "Longitude and latitude will be saved for each boulder location. "
                  "You can access location in the session summary screen by clicking the location name",
                  overflow: TextOverflow.clip,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
            ])),
            const SizedBox(width: 10.0),
            Switch(
                onChanged: (bool value) {
                  setState(() {
                    Location().setLocation(value);
                  });
                  if (value) {
                    _checkLocationPermission();
                  }
                },
                value: Location().saveLocation)
          ])),

      Divider(thickness: 2.0),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Text("STATISTICS / CHARTS", style: const TextStyle(color: Colors.blue, fontWeight: FontWeight.w500))),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Limit Chart Entries"),
              Text(
                  "Only the last 16 sessions are shown in the graphics. "
                  "This significantly increases the performance of the app.",
                  overflow: TextOverflow.clip,
                  style: const TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
            ])),
            const SizedBox(width: 10.0),
            Switch(
                onChanged: (bool value) {
                  setState(() {
                    Data().limitCharts = value;
                  });
                },
                value: Data().limitCharts)
          ])),

      Divider(thickness: 2.0),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Text("NOTIFICATIONS", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500))),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Flexible(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Enable Notifications"),
              Text("The app will send push notifications to remind you to close the current session",
                  overflow: TextOverflow.clip, style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
            ])),
            SizedBox(width: 10.0),
            Switch(
                onChanged: (bool value) {
                  setState(() {
                    NotificationHandler().notificationsActivated = value;
                  });
                },
                value: NotificationHandler().notificationsActivated)
          ])),
      InkWell(
          onTap: () => _changeSessionReminderDuration(context),
          child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Flexible(
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text("Session Reminder"),
                  Text("Select the time after you will be reminded to close the session",
                      overflow: TextOverflow.clip, style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
                ])),
                SizedBox(width: 15.0),
                Icon(Icons.access_alarm_outlined, color: Colors.black54)
              ]))),
      Divider(thickness: 2.0),
      Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Text("OTHERS", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500))),
      if (Platform.isAndroid) ...[
        createSettingWidget("Support Development", "Buy me a Coffee", _donate, icon: Icons.favorite_border_outlined)
      ],
      createSettingWidget(
          "Share",
          "Recommend this app to others",
          () => Share.share("Hey, I'm using this boulder tracker app, feel free to try it out:\n"
              "https://play.google.com/store/apps/details?id=freudenmann.boulder_track"),
          icon: Icons.share),
      createSettingWidget("Rate", "Share your opinion", _rateApp, icon: Icons.star_border),

      createSettingWidget("Feedback", "Any Feature that's missing?", _sendFeedBack,
          icon: Icons.local_post_office_outlined),
      createSettingWidget("Reset Tooltips",
          "Tooltips are usually only displayed once. If you click here, the tips will be shown again", _resetTooltips,
          icon: Icons.info_outline),
      //Divider(thickness: 2.0),
      SizedBox(height: 20),
      Text("App Version:", style: TextStyle(color: Colors.black38, fontWeight: FontWeight.normal)),
      Text(versionNumber, style: TextStyle(color: Colors.black38, fontWeight: FontWeight.normal)),
      SizedBox(height: 20)
    ]);
  }

  Widget createSettingWidget(String title, String info, void Function() onTapFunction, {IconData icon}) {
    return InkWell(
        onTap: onTapFunction,
        child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Flexible(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(title),
                Text(info, overflow: TextOverflow.clip, style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
              ])),
              icon == null ? SizedBox(width: 70.0) : SizedBox(width: 15.0),
              icon == null ? Container() : Icon(icon, color: Colors.black54)
            ])));
  }

  void _donate() {
    Navigator.of(context)
        .push(PageRouteBuilder(pageBuilder: (context, animation, secondaryAnimation) => DonationPage()));
  }

  void _createBackupFile() {
    JsonCreator().createBackupFile().then((value) => _shareFile(value));
  }

  void _shareFile(String filePath) {
    Share.shareFiles([filePath]);
  }

  void _rateApp() {
    StoreRedirect.redirect();
  }

  Future<void> _sendFeedBack() async {
    final Email email = Email(
      recipients: ['lukas.freudenmann+bouldertracker@gmail.com'],
      isHTML: false,
    );
    await FlutterEmailSender.send(email);
  }

  void _deleteDataDialog() {
    ConfirmDialog().showDeleteDatabaseDialog(context, _deleteDataBase);
  }

  void _deleteDataBase() {
    DBController().deleteAll();
    Data().reset();
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text("Data has been deleted.")));
  }

  void _loadBackupDialog() {
    ConfirmDialog().showLoadBackupDialog(context, _loadBackup);
  }

  void _loadBackup() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(type: FileType.any);
    if (result == null) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(content: Text("Backup couldn't be loaded")));
      return;
    }
    _loadFile(result.files.single.path).then((value) => {
          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(SnackBar(content: Text(value ? "Backup loaded" : "Backup couldn't be loaded")))
        });
  }

  void _loadAutomaticBackupDialog(List<FileSystemEntity> backupList) {
    ConfirmDialog().showChooseBackupDialog(context, _loadFileWithResponse, backupList);
  }

  void _loadAutomaticBackup() async {
    String dir;
    if (Platform.isAndroid) {
      List<Directory> path = await getExternalStorageDirectories();
      dir = path[0].path;
    } else if (Platform.isIOS) {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      dir = join(documentsDirectory.path, "boulder_tracker");
    } else {
      return;
    }

    // create directory if non-existent
    Directory(dir).createSync();

    List<FileSystemEntity> files = Directory(dir).listSync();
    files.sort((a, b) => b.path.compareTo(a.path));
    if (files.isEmpty) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(content: Text("No backup available to restore"), duration: Duration(seconds: 2)));
      return;
    }

    _loadAutomaticBackupDialog(files);
  }

  void _loadFileWithResponse(String path) {
    _loadFile(path).then((value) => {
          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(SnackBar(
                content: Text(value ? "Backup restored" : "Backup couldn't be restored"),
                duration: Duration(seconds: 2)))
        });
  }

  Future<bool> _loadFile(String value) async {
    if (value == null) {
      return false;
    }
    if (!value.endsWith(".json")) {
      return false;
    }
    Data().reset();
    DBController().deleteAll();
    return await JsonLoader().load(value);
  }

  void _changeSessionReminderDuration(BuildContext context) async {
    Picker(
        itemExtent: 42.0,
        adapter: NumberPickerAdapter(data: [
          NumberPickerColumn(
              initValue: (NotificationHandler().reminderDuration / 60 * 2).toInt(),
              begin: 1,
              end: 19,
              suffix: Text("  h"),
              onFormatValue: (value) => "${(value / 2).toStringAsFixed(1)}"),
        ]),
        hideHeader: true,
        title: Text("Please Select:", style: Theme.of(context).textTheme.headline4.copyWith(color: Colors.black)),
        selectedTextStyle: TextStyle(color: Colors.blue),
        onConfirm: (Picker picker, List value) {
          double result = (picker.getSelectedValues().first / 2) * 60;
          NotificationHandler().reminderDuration = result.toInt();
        }).showDialog(context);
  }

  void _resetTooltips() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("tool_tip_close_session", true);
    prefs.setBool("tool_tip_session_summary", true);
    prefs.setBool("tool_tip_chart_settings", true);
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: const Text("Tooltips reset"), duration: const Duration(seconds: 2)));
  }

  void _checkLocationPermission() async {
    if (await Location().hasPermission()) {
      return;
    }
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text("Location couldn't be enabled. Permission missing.")));
    setState(() {
      Location().setLocation(false);
    });
  }

  void _checkStoragePermissions() async {
    PermissionStatus status = await Permission.storage.request();
    if (status.isGranted) {
      return;
    }

    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text("Automatic backups couldn't be enabled. Permission denied.")));
    setState(() {
      automaticBackups = false;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("automatic_backups", false);
  }

  void _loadAutomaticBackupValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool result = prefs.getBool("automatic_backups") ?? false;
    bool result2 = prefs.getBool("overwrite_automatic_backup") ?? true;
    setState(() => {this.automaticBackups = result, this.automaticBackupsWithDate = result2});
  }
}

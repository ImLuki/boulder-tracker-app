import 'dart:async';
import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inapp_purchase/flutter_inapp_purchase.dart';

class DonationPage extends StatefulWidget {
  @override
  _DonationPageState createState() => _DonationPageState();
}

class _DonationPageState extends State<DonationPage> {
  StreamSubscription _purchaseUpdatedSubscription;
  StreamSubscription _purchaseErrorSubscription;
  StreamSubscription _connectionSubscription;

  final List<String> _productIds = [
    "1_euro_donation",
    "2_euro_donation",
    "5_euro_donation",
    "10_euro_donation",
    "20_euro_donation"
  ];
  final List<String> _productNames = ["1€", "2€", "5€", "10€", "20€"];

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  @override
  void dispose() {
    FlutterInappPurchase.instance.endConnection;
    if (_connectionSubscription != null) {
      _connectionSubscription.cancel();
      _connectionSubscription = null;
      _purchaseErrorSubscription.cancel();
      _purchaseErrorSubscription = null;
      _purchaseUpdatedSubscription.cancel();
      _purchaseUpdatedSubscription = null;
    }
    super.dispose();
  }

  Future<void> initPlatformState() async {
    // prepare
    var result = await FlutterInappPurchase.instance.initConnection;
    print('result: $result');

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    // refresh items for android
    try {
      String msg = await FlutterInappPurchase.instance.consumeAllItems;
      print('consumeAllItems: $msg');
    } catch (err) {
      print('consumeAllItems error: $err');
    }

    _connectionSubscription = FlutterInappPurchase.connectionUpdated.listen((connected) {
      print('connected: $connected');
    });

    _purchaseUpdatedSubscription = FlutterInappPurchase.purchaseUpdated.listen((productItem) {
      FlutterInappPurchase.instance.finishTransaction(productItem);
      print('purchase-updated: $productItem');
    });

    _purchaseErrorSubscription = FlutterInappPurchase.purchaseError.listen((purchaseError) {
      print('purchase-error: $purchaseError');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: AppColors.accentColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Support development", style: TextStyle(color: Colors.white, fontSize: 22)),
          ])),
      body: _createPage(),
    );
  }

  Widget _createPage() {
    return Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
        child: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Why support?", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500)),
                  SizedBox(height: 10),
                  Text(
                      "This app was not developed to earn money, "
                          "but as a student I am always happy about a small donation. "
                          "The donation enables me to program further apps in the future, "
                          "improve this app and add new features. "
                          "In addition, the app can stay free and there will be no advertising! ",
                      overflow: TextOverflow.clip,
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal)),
                  SizedBox(height: 10),
                  Divider(thickness: 2.0),
                  SizedBox(height: 10),
                  Text("DONATE", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500)),
                  SizedBox(height: 10),
                  Text(
                      "Donations are securely handled by Google. "
                          "The price includes all Taxes and will be converted in your local currency.",
                      overflow: TextOverflow.clip,
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal)),
                  SizedBox(height: 20),
                  _createDonateButtons()
                ])));
  }

  Widget _createDonateButtons() {
    return Wrap(
        alignment: WrapAlignment.spaceEvenly,
        runSpacing: Constants.STANDARD_PADDING,
        spacing: Constants.STANDARD_PADDING,
        children: List.generate(
            _productIds.length,
                (index) => TextButton(
              onPressed: () async {
                try {
                  //await FlutterInappPurchase.instance.initialize();
                  List<IAPItem> items = await FlutterInappPurchase.instance.getProducts(_productIds);

                  FlutterInappPurchase.instance.requestPurchase(
                      items.firstWhere((element) => element.productId == _productIds[index]).productId);
                } catch (exception) {
                  showWarningScreen(context);
                }


              },
              style: TextButton.styleFrom(
                backgroundColor: Colors.grey[300],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(48.0),
                ),
              ),
              child: Text(_productNames[index]),
            )));
  }

  Future<bool> showWarningScreen(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              scrollable: true,
              contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
              titleTextStyle: Theme.of(context).textTheme.subtitle1,
              content: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
                SizedBox(width: 14.0),
                Flexible(
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  SizedBox(height: Constants.SMALL_PADDING),
                  Text("WARNING!", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: Constants.SMALL_PADDING),
                  Text("Google Payment Services currently not available. Please try again later.",
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal), overflow: TextOverflow.clip),
                ]))
              ]),
              contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
              actions: <Widget>[
                MaterialButton(
                    color: AppColors.accentColor,
                    onPressed: () => {FocusScope.of(context).unfocus(), Navigator.pop(context)},
                    textColor: Colors.white,
                    child: Text("Back"))
              ]);
        });
  }
}

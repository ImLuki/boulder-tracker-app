import 'dart:io';

import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/dialog/session_edit_dialog.dart';
import 'package:boulder_track/ui/widgets/session_summary.dart';
import 'package:boulder_track/util/screenshot.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:highlighter_coachmark/highlighter_coachmark.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SessionTile extends StatefulWidget {
  const SessionTile({Key key, this.data, this.toolTipKey, this.callBack}) : super(key: key);

  final Map<String, dynamic> data;
  final toolTipKey;
  final Function callBack;

  @override
  _SessionTileState createState() => _SessionTileState();
}

class _SessionTileState extends State<SessionTile> {
  static const double LOADING_CONTAINER_DATE_HEIGHT = 26.0;
  static const double LOADING_CONTAINER_DATE_WIDTH = 110.0;
  static const double LOADING_CONTAINER_VERTICAL_MARGIN = 4.0;
  static const double LOADING_CONTAINER_LOCATION_HEIGHT = 14.0;
  static const double LOADING_CONTAINER_LOCATION_WIDTH = 240;
  static const double LOADING_CONTAINER_LOCATION_TOP_MARGIN = 6.0;
  static const double LOADING_CONTAINER_LOCATION_BOTTOM_MARGIN = 10.0;
  static const double LOADING_CONTAINER_OPACITY = 0.08;

  final ScreenshotController _screenshotController = ScreenshotController();

  Future _futureData;

  @override
  void initState() {
    super.initState();
    this._futureData = fetchData();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.toolTipKey != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTipAsync());
    }
    return InkWell(
      key: widget.toolTipKey,
      onLongPress: () => Navigator.of(context).push(_sessionSummaryRoute(context)),
      child: _createSessionContainer(context),
    );
  }

  Widget _createSessionContainer(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              width: 4.0, color: widget.data[DBController.sessionOutdoor] == 1 ? Colors.green : Colors.orangeAccent),
        ),
        color: Colors.transparent,
      ),
      child: _createExpansionSessionTile(context),
    );
  }

  Widget _createExpansionSessionTile(BuildContext context) {
    return FutureBuilder(
        future: this._futureData,
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return _loadingPlaceHolder(context);
          }
          var boulder = snapshot.data;
          if (snapshot.data == null) {
            boulder = [];
          }
          return Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              textColor: Colors.black,
              title: _getDateTitle(widget.data[DBController.sessionTimeStart]),
              subtitle: _getLocationWidget(widget.data[DBController.sessionLocation]),
              children: _createBoulderChildren(context, boulder),
            ),
          );
        });
  }

  Widget _loadingPlaceHolder(BuildContext context) {
    return ExpansionTile(
      textColor: Colors.black,
      title: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: LOADING_CONTAINER_VERTICAL_MARGIN),
            height: LOADING_CONTAINER_DATE_HEIGHT,
            width: LOADING_CONTAINER_DATE_WIDTH,
            color: Colors.black.withOpacity(LOADING_CONTAINER_OPACITY),
          ),
        ],
      ),
      subtitle: Row(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: LOADING_CONTAINER_LOCATION_TOP_MARGIN,
              bottom: LOADING_CONTAINER_LOCATION_BOTTOM_MARGIN,
            ),
            height: LOADING_CONTAINER_LOCATION_HEIGHT,
            width: LOADING_CONTAINER_LOCATION_WIDTH,
            color: Colors.black.withOpacity(LOADING_CONTAINER_OPACITY),
          )
        ],
      ),
    );
  }

  Widget _getDateTitle(String date) {
    return Text(DateFormat("dd.MM.yy").format(DateTime.parse(date)));
  }

  Widget _getLocationWidget(String location) {
    return Text(
      Util.getLocation(location),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
    );
  }

  List<Widget> _createBoulderChildren(BuildContext context, List boulder) {
    return List<Widget>.generate(
        boulder.length,
        (index) => Padding(
            padding: EdgeInsets.only(left: Constants.STANDARD_PADDING, right: Constants.STANDARD_PADDING),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Flexible(
                  child: Text(Util.getBoulderName(boulder[index][DBController.name]),
                      overflow: TextOverflow.ellipsis, maxLines: 1, style: Theme.of(context).textTheme.bodyText1)),
              SizedBox(width: 14.0),
              Text(
                  Util.getSummary(
                    boulder[index][DBController.gradeId],
                    boulder[index][DBController.styleId],
                    boulder[index][DBController.gradeSystem],
                  ),
                  style: Theme.of(context).textTheme.bodyText1)
            ])));
  }

  Route _sessionSummaryRoute(BuildContext context) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return _createSessionSummaryContainer(context);
      },
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  Route _sessionSummaryRouteWithoutAnimation(BuildContext context) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) {
        return _createSessionSummaryContainer(context);
      },
      transitionDuration: Duration(seconds: 0),
    );
  }

  Widget _createSessionSummaryContainer(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: AppColors.accentColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("SESSION SUMMARY", style: TextStyle(color: Colors.white, fontSize: 22)),
            Row(
              children: [
                IconButton(
                  onPressed: _createScreenShot,
                  icon: Icon(Icons.share),
                ),
                IconButton(
                  onPressed: () {
                    SessionEditDialog()
                        .showSessionDialog(context, widget.data[DBController.sessionId],
                            callBack: widget.callBack, onChanged: widget.callBack)
                        .then((value) {
                      if (value) Navigator.pushReplacement(context, _sessionSummaryRouteWithoutAnimation(context));
                    });
                  },
                  icon: Icon(Icons.edit),
                ),
              ],
            )
          ])),
      body: SingleChildScrollView(
        child: Screenshot(
            controller: _screenshotController,
            child: Container(color: Colors.white, child: SessionSummary(widget.data[DBController.sessionId]))),
      ),
    );
  }

  void _createScreenShot() {
    _screenshotController
        .capture(
            fileName:
                "${DateFormat("yyyy-MM-dd").format(DateTime.parse(widget.data[DBController.sessionTimeStart]))}-Session-Summary")
        .then((File image) {
      Share.shareFiles([image.path]);
    }).catchError((onError) {
      print(onError);
    });
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return await DBController().queryRows(DBController.boulderTable,
        where: "${DBController.sessionId} == ${widget.data[DBController.sessionId]}");
  }

  void _showToolTip() {
    CoachMark coachMarkFAB = CoachMark();
    RenderBox target = widget.toolTipKey.currentContext.findRenderObject();

    Rect markRect = target.localToGlobal(Offset.zero) & target.size;
    markRect =
        Rect.fromCenter(center: markRect.bottomCenter, width: markRect.width * 1.1, height: markRect.height * 2.8);

    coachMarkFAB.show(
        targetContext: widget.toolTipKey.currentContext,
        markRect: markRect,
        markShape: BoxShape.rectangle,
        children: [
          Center(
              child: Text("Tip:\nLong press on a session to see the session summary or edit the session",
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 24.0,
                    fontStyle: FontStyle.italic,
                    color: Colors.white,
                  )))
        ],
        duration: null);
  }

  Future<void> _showToolTipAsync() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool("tool_tip_session_summary") ?? true) {
      prefs.setBool("tool_tip_session_summary", false);
      _showToolTip();
    }
  }
}

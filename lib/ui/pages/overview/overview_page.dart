import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/ui/pages/overview/overview_stats.dart';
import 'package:boulder_track/ui/pages/overview/session_overview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OverviewPage extends StatefulWidget {
  OverviewPage({Key key, this.dataIndoor, this.dataOutdoor, this.sessionData, this.callBack}) : super(key: key);

  final Map<String, dynamic> dataIndoor;
  final Map<String, dynamic> dataOutdoor;
  final List<Map<String, dynamic>> sessionData;
  final Function callBack;

  @override
  _OverviewPageState createState() => _OverviewPageState();
}

class _OverviewPageState extends State<OverviewPage> {
  final PageController _controller = PageController(initialPage: 1);
  int currentPage = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            systemOverlayStyle: SystemUiOverlayStyle.light,
            backgroundColor: AppColors.accentColor,
            iconTheme: IconThemeData(color: Colors.white),
            title: getTitle()),
        body: Center(child: createPageView()));
  }

  Widget createPageView() {
    return PageView(
        onPageChanged: (index) => setState(() {
              this.currentPage = index;
            }),
        controller: _controller,
        children: [
          SingleChildScrollView(padding: EdgeInsets.zero, child: OverViewStats(map: widget.dataIndoor)),
          SessionOverview(data: widget.sessionData, callBack: widget.callBack),
          SingleChildScrollView(padding: EdgeInsets.zero, child: OverViewStats(map: widget.dataOutdoor))
        ]);
  }

  Widget getTitle() {
    switch (this.currentPage) {
      case 0:
        return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Flexible(fit: FlexFit.tight, child: Text("")),
          Flexible(
              fit: FlexFit.tight,
              child: Text("INDOOR", style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.center)),
          Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () =>
                      _controller.animateToPage(1, curve: Curves.easeIn, duration: Duration(milliseconds: 200)),
                  child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    Text("sessions", style: TextStyle(color: Colors.white, fontSize: 16), textAlign: TextAlign.end),
                    Icon(Icons.arrow_right, color: Colors.white)
                  ])))
        ]);
      case 1:
        return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () =>
                      _controller.animateToPage(0, curve: Curves.easeIn, duration: Duration(milliseconds: 200)),
                  child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    Icon(Icons.arrow_left, color: Colors.white),
                    Text("indoor", style: TextStyle(color: Colors.white, fontSize: 16), textAlign: TextAlign.start)
                  ]))),
          Flexible(
              fit: FlexFit.tight,
              child:
                  Text("SESSIONS", style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.center)),
          Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () =>
                      _controller.animateToPage(2, curve: Curves.easeIn, duration: Duration(milliseconds: 200)),
                  child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                    Text("outdoor", style: TextStyle(color: Colors.white, fontSize: 16), textAlign: TextAlign.end),
                    Icon(Icons.arrow_right, color: Colors.white)
                  ])))
        ]);
      case 2:
        return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () =>
                      _controller.animateToPage(1, curve: Curves.easeIn, duration: Duration(milliseconds: 200)),
                  child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    Icon(Icons.arrow_left, color: Colors.white),
                    Text("sessions", style: TextStyle(color: Colors.white, fontSize: 16), textAlign: TextAlign.start)
                  ]))),
          Flexible(
              fit: FlexFit.tight,
              child: Text("OUTDOOR", style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.center)),
          Flexible(fit: FlexFit.tight, child: Text(""))
        ]);
    }
    return Container();
  }
}

import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/pages/overview/session_tile.dart';
import 'package:boulder_track/ui/widgets/empty_list_placeholder.dart';
import 'package:flutter/material.dart';

class SessionOverview extends StatelessWidget {
  SessionOverview({Key key, this.data, this.callBack}) : super(key: key);
  final GlobalKey _toolTipKey = GlobalObjectKey("toolTipKey");
  final data;
  final Function callBack;

  @override
  Widget build(BuildContext context) {
    return _createSessions();
  }

  Widget _createSessions() {
    return data == null || data.isEmpty ? EmptyListPlaceHolder(text: "No sessions found") : _getSessionScroll(data);
  }

  Widget _getSessionScroll(var data) {
    return Scrollbar(
      child: ListView.separated(
        padding: EdgeInsets.all(Constants.STANDARD_PADDING),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          return SessionTile(
            data: data[index],
            toolTipKey: index == 0 ? _toolTipKey : null,
            callBack: callBack,
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 2.0),
      ),
    );
  }
}

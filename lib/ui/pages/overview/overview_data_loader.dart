import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/ui/pages/overview/overview_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OverviewDataLoader extends StatefulWidget {
  const OverviewDataLoader({Key key}) : super(key: key);

  @override
  _OverviewDataLoaderState createState() => _OverviewDataLoaderState();
}

class _OverviewDataLoaderState extends State<OverviewDataLoader> {
  Map<String, dynamic> _dataIndoor = {
    "totalAscends": 0,
    "totalFlash": 0,
    "totalTop": 0,
    "count": 0,
    "boulderFlash": null,
    "boulderTop": null,
    "boulderProject": null,
  };

  Map<String, dynamic> _dataOutdoor = {
    "totalAscends": 0,
    "totalFlash": 0,
    "totalTop": 0,
    "count": 0,
    "boulderFlash": null,
    "boulderTop": null,
    "boulderProject": null,
  };

  var sessionData;

  bool _dataLoaded = false;

  @override
  void initState() {
    super.initState();
    this._calculateData();
  }

  @override
  Widget build(BuildContext context) {
    return _dataLoaded
        ? OverviewPage(
            dataIndoor: this._dataIndoor,
            dataOutdoor: this._dataOutdoor,
            sessionData: this.sessionData,
            callBack: _calculateData)
        : _buildLoadingScreen();
  }

  Widget _buildLoadingScreen() {
    return Scaffold(
      appBar: AppBar(
          systemOverlayStyle: SystemUiOverlayStyle.light,
          backgroundColor: AppColors.accentColor,
          iconTheme: IconThemeData(color: Colors.white),
          title: Center(
              child:
                  Text("SESSIONS", style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.center))),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            SizedBox(height: 25),
            Text("DATA CALCULATING...",
                style: Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.black38),
                textAlign: TextAlign.center)
          ],
        ),
      ),
    );
  }

  void _calculateData() async {
    this._dataLoaded = false;
    var data1 = await fetchDataStatistics(true);
    var data2 = await fetchDataStatistics(false);
    var data3 = await fetchSessionData();
    setState(() {
      this._dataOutdoor = _calculateOverview(data1, this._dataOutdoor);
      this._dataIndoor = _calculateOverview(data2, this._dataIndoor);
      this.sessionData = data3;
      this._dataLoaded = true;
    });
  }

  Future<List<Map<String, dynamic>>> fetchDataStatistics(bool outdoor) async {
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.boulderTable} "
      "JOIN ${DBController.sessionTable} using (${DBController.sessionId})"
      "WHERE ${DBController.sessionOutdoor} == (?)",
      [outdoor ? 1 : 0],
    );
  }

  Map<String, dynamic> _calculateOverview(List<Map<String, dynamic>> boulder, Map<String, dynamic> map) {
    Map sessions = Map();

    for (Map<String, dynamic> ascend in boulder) {
      sessions[ascend[DBController.sessionId]] = 1;
      map["totalAscends"] += 1;

      switch (ascend[DBController.styleId]) {
        case 0:
          map["totalFlash"] += 1;

          if (map["boulderFlash"] == null) {
            map["boulderFlash"] = ascend;
          } else if (map["boulderFlash"][DBController.gradeId] <= ascend[DBController.gradeId]) {
            if (map["boulderFlash"][DBController.gradeId] == ascend[DBController.gradeId] &&
                DateTime.parse(map["boulderFlash"][DBController.sessionTimeStart])
                    .isBefore(DateTime.parse(ascend[DBController.sessionTimeStart]))) {
              break;
            }
            map["boulderFlash"] = ascend;
          }
          continue top;
        top:
        case 1:
          map["totalTop"] += 1;

          if (map["boulderTop"] == null) {
            map["boulderTop"] = ascend;
          } else if (map["boulderTop"][DBController.gradeId] <= ascend[DBController.gradeId]) {
            if (map["boulderTop"][DBController.gradeId] == ascend[DBController.gradeId] &&
                DateTime.parse(map["boulderTop"][DBController.sessionTimeStart])
                    .isBefore(DateTime.parse(ascend[DBController.sessionTimeStart]))) {
              break;
            }
            map["boulderTop"] = ascend;
          }
          continue project;
        project:
        case 2:
          if (map["boulderProject"] == null) {
            map["boulderProject"] = ascend;
          } else if (map["boulderProject"][DBController.gradeId] <= ascend[DBController.gradeId]) {
            if (map["boulderProject"][DBController.gradeId] == ascend[DBController.gradeId] &&
                DateTime.parse(map["boulderProject"][DBController.sessionTimeStart])
                    .isBefore(DateTime.parse(ascend[DBController.sessionTimeStart]))) {
              break;
            }
            map["boulderProject"] = ascend;
          }
      }
    }

    map["count"] = sessions.length;
    return map;
  }

  Future<List<Map<String, dynamic>>> fetchSessionData() async {
    return DBController().rawQuery(
        "SELECT * FROM ${DBController.sessionTable} "
        "WHERE ${DBController.sessionStatus} != (?) "
        "ORDER BY ${DBController.sessionTimeStart} DESC",
        [Data.SESSION_ACTIVE_STATE]);
  }
}

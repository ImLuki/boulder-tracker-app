import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/backend/util/style_mapper.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/dialog/info_dialogs.dart';
import 'package:flutter/material.dart';

class OverViewStats extends StatelessWidget {
  OverViewStats({Key key, this.map}) : super(key: key);

  final Map<String, dynamic> map;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(padding: EdgeInsets.zero, child: _createOverview(context));
  }

  Widget _createOverview(BuildContext context) {
    return Column(children: [
      SizedBox(height: Constants.STANDARD_PADDING),
      _paddingWidget(_createSummaryWidget()),
      SizedBox(height: 15),
      Divider(thickness: 2.0, indent: Constants.STANDARD_PADDING, endIndent: Constants.STANDARD_PADDING),
      SizedBox(height: 10),
      Text("Best Boulders"),
      SizedBox(height: 11),
      _createBoulderAscendWidget("boulder", context),
      SizedBox(height: 15),
    ]);
  }

  Widget _createBoulderAscendWidget(String key, BuildContext context) {
    return Column(children: List.generate(3, (index) => _createBestBoulderContainer(index, key, context)));
  }

  Widget _paddingWidget(Widget child) {
    return Container(
        padding: EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 0, Constants.STANDARD_PADDING, 0),
        alignment: Alignment.center,
        child: child);
  }

  Widget _createSummaryWidget() {
    return Column(children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Total Sessions:"),
                  Text(map["count"].toString(), style: TextStyle(color: AppColors.color1))
                ],
              )),
          SizedBox(width: 30.0),
          Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Total Ascends:"),
                  Text(map["totalAscends"].toString(), style: TextStyle(color: AppColors.color1))
                ],
              ))
        ],
      ),
      SizedBox(height: 15),
      Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Flash\nPercentage:"),
                  Text(
                      map["totalFlash"] == 0
                          ? "0%"
                          : "${(map["totalFlash"] / map["totalAscends"] * 100).toStringAsFixed(2)}%",
                      style: TextStyle(color: AppColors.color1))
                ])),
        SizedBox(width: 30.0),
        Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Top\nPercentage:"),
                  Text(
                      map["totalTop"] == 0
                          ? "0%"
                          : "${(map["totalTop"] / map["totalAscends"] * 100).toStringAsFixed(2)}%",
                      style: TextStyle(color: AppColors.color1))
                ]))
      ])
    ]);
  }

  Widget _createBestBoulderContainer(int index, String key, BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(Constants.STANDARD_PADDING, 4, Constants.STANDARD_PADDING, 4),
      color: index.isEven ? Colors.transparent : Colors.grey.shade200,
      child: _createBestBoulderListTile(
        map,
        "$key${StyleMapper().mapping[index]}",
        StyleMapper().mapping[index],
        context,
      ),
    );
  }

  Widget _createBestBoulderListTile(Map map, String key, String style, BuildContext context) {
    Text styleText = Text("$style:", style: TextStyle(fontWeight: FontWeight.w400));
    if (map[key] == null) {
      return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [styleText, Text("---")]);
    }
    TextStyle textStyle = TextStyle(fontWeight: FontWeight.w500);

    Boulder boulder = Boulder(
      map[key][DBController.gradeId],
      map[key][DBController.gradeSystem],
      map[key][DBController.styleId],
      map[key][DBController.rating],
      map[key][DBController.name],
      DateTime.parse(map[key][DBController.sessionTimeStart]),
      map[key][DBController.sessionLocation],
      map[key][DBController.sessionOutdoor] == 1,
      map[key][DBController.marked] == 1,
      comment: map[key][DBController.comment],
    );

    return InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () => InfoDialog().showBoulderDialog(context, boulder),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          styleText,
          SizedBox(width: 14.0),
          Flexible(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                Row(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.end, children: [
                  Flexible(
                      child: Text(Util.getBoulderName(map[key][DBController.name]),
                          overflow: TextOverflow.ellipsis, style: textStyle)),
                  Text(
                      " (${GradeManager().getGradeMapper().getGradeFromId(
                            map[key][DBController.gradeId],
                          )})",
                      style: textStyle),
                ]),
                Text(
                    Util.getLocationDateString(
                      map[key][DBController.sessionTimeStart],
                      map[key][DBController.sessionLocation],
                    ),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal))
              ]))
        ]));
  }
}

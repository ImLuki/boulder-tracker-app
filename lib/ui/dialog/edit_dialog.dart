import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:boulder_track/ui/pages/session/widgets/scroll_picker/drop_down_grading.dart';
import 'package:boulder_track/ui/pages/session/widgets/scroll_picker/scroll_picker.dart';
import 'package:boulder_track/ui/pages/session/widgets/style_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class EditDialog {
  TextEditingController boulderNameController = TextEditingController();
  TextEditingController commentController = TextEditingController();
  Boulder newBoulder;

  Future<void> showBoulderDialog(BuildContext context, Boulder oldBoulder, int sessionID,
      {Function(dynamic) onChanged}) async {
    this.newBoulder = oldBoulder.copy();
    boulderNameController.text = oldBoulder.name;
    commentController.text = oldBoulder.comment;

    String text = "Edit Boulder";

    return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return GestureDetector(
              onVerticalDragUpdate: (dragDetails) {
                FocusScope.of(context).unfocus();
              },
              onTapDown: (tapDownDetails) {
                FocusScope.of(context).unfocus();
              },
              child: AlertDialog(
                  title: Text(text),
                  titleTextStyle: Theme.of(context).textTheme.subtitle1,
                  content: SingleChildScrollView(
                      physics: BouncingScrollPhysics(), child: getBoulderDialogContent(context, oldBoulder)),
                  contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
                  actions: <Widget>[
                    OutlinedButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text("CANCEL", style: TextStyle(color: AppColors.accentColor)),
                        style: ButtonStyle(
                          overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                            if (states.contains(MaterialState.pressed)) {
                              return AppColors.accentColor.withOpacity(0.3);
                            }
                            return Colors.transparent;
                          }),
                        )),
                    MaterialButton(
                        color: AppColors.accentColor,
                        onPressed: () async {
                          await updateAscendInDatabase(newBoulder);
                          onChanged(newBoulder.toMap(sessionID));
                          Navigator.of(context).pop();
                        },
                        textColor: Colors.white,
                        child: Text("SAVE"))
                  ]));
        });
  }

  Widget getBoulderDialogContent(BuildContext context, Boulder oldBoulder) {
    return Padding(
        padding: EdgeInsets.all(Constants.STANDARD_PADDING),
        child: Wrap(
          alignment: WrapAlignment.center,
          direction: Axis.horizontal,
          runSpacing: Constants.STANDARD_PADDING,
          children: [
            Align(alignment: Alignment.center, child: DropDownGrading()),
            createDifficultyPicker(context, oldBoulder.gradeID),
            Divider(thickness: 2.0),
            createNameTextField(context),
            Divider(thickness: 2.0),
            StylePicker(
              initialValue: oldBoulder.styleID,
              onChanged: (newValue) => this.newBoulder.styleID = newValue,
            ),
            Divider(thickness: 2.0),
            createRatingBar(oldBoulder.rating),
            Divider(thickness: 2.0),
            createCommentField(context),
          ],
        ));
  }

  Widget createNameTextField(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: TextFormField(
            controller: boulderNameController,
            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            onChanged: (String value) => this.newBoulder.name = value.trim(),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
            textCapitalization: TextCapitalization.sentences,
            decoration: new InputDecoration(
                isDense: true,
                labelText: "Boulder Name",
                labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
                fillColor: AppColors.accentColor,
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                  borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                ),
                border: OutlineInputBorder(
                  borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                  borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                ),
                focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                    borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS)))));
  }

  Widget createCommentField(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: TextFormField(
            maxLines: 5,
            controller: commentController,
            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            onChanged: (value) => this.newBoulder.comment = value.trim(),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
            textCapitalization: TextCapitalization.sentences,
            decoration: new InputDecoration(
                alignLabelWithHint: true,
                isDense: true,
                labelText: "Comment",
                labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
                fillColor: AppColors.accentColor,
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                  borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                ),
                border: OutlineInputBorder(
                  borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                  borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                ),
                focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                    borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS)))));
  }

  Widget createDifficultyPicker(BuildContext context, int difficulty) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        height: 40,
        width: MediaQuery.of(context).size.width,
        child: ShaderMask(
          shaderCallback: (Rect rect) {
            return LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: <Color>[Colors.black, Colors.transparent, Colors.transparent, Colors.black],
                stops: [0.001, 0.45, 0.55, 9.9]).createShader(rect);
          },
          blendMode: BlendMode.dstOut,
          child: RotatedBox(
              quarterTurns: 3,
              child: ScrollPicker(
                initialValue: GradeManager().getGradeMapper().getGradeForGradePicker(difficulty),
                onChanged: (String value) => {
                  this.newBoulder.gradeID = GradeManager().getGradeMapper().getIdFromGrade(value),
                },
                boulder: newBoulder,
              )),
        ),
      ),
    );
  }

  Widget createRatingBar(int rating) {
    return RatingBar(
        initialRating: rating.toDouble(),
        minRating: 1,
        glow: false,
        direction: Axis.horizontal,
        allowHalfRating: false,
        itemCount: 5,
        itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        onRatingUpdate: (double value) => this.newBoulder.rating = value.toInt(),
        ratingWidget: RatingWidget(
          empty: Constants.EMPTY_STAR,
          full: Constants.FULL_STAR,
          half: null,
        ));
  }

  Future<void> showGradingSystemsDialog(BuildContext context) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
              title: Text("Boulder Systems", style: TextStyle(fontSize: 32)),
              children: List.generate(
                  GradeManager().getGradeMapper().getGradeSystems().length,
                  (index) => InkWell(
                      child: ListTile(
                          title: Text(GradeManager().getGradeMapper().getGradeSystems()[index],
                              style: TextStyle(fontSize: 18)),
                          leading: Icon(GradeManager().getGradeMapper().getGradeSystems()[index] ==
                                  GradeManager().getGradeMapper().currentGradingSystem
                              ? Icons.radio_button_checked
                              : Icons.radio_button_off)),
                      onTap: () {
                        GradeManager().getGradeMapper().currentGradingSystem =
                            GradeManager().getGradeMapper().getGradeSystems()[index];
                        GradeManager().init();
                        Navigator.of(context).pop();
                      })));
        });
  }

  Future<void> updateAscendInDatabase(Boulder data) async {
    Map<String, dynamic> boulderData = {
      DBController.gradeId: data.gradeID,
      DBController.styleId: data.styleID,
      DBController.gradeSystem: data.gradeSystem,
      DBController.name: data.name,
      DBController.rating: data.rating,
      DBController.marked: data.marked ? 1 : 0,
      DBController.comment: data.comment,
      DBController.tries: data.tries
    };
    DBController dbController = DBController();
    await dbController.updateBoulder(boulderData, data.boulderID);
  }
}

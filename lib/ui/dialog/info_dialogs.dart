import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class InfoDialog {
  Future<bool> showBoulderDialog(BuildContext context, Boulder boulder) async {
    return showDialog<bool>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("Boulder:"),
              titleTextStyle: Theme.of(context).textTheme.subtitle1,
              content: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Wrap(runSpacing: 10.0, children: createBoulderInfo(boulder))),
              contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
              actions: <Widget>[
                MaterialButton(
                    color: AppColors.accentColor,
                    onPressed: () => {FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
                    textColor: Colors.white,
                    child: Text("BACK"))
              ]);
        });
  }

  Future<void> showStyleInfoDialog(BuildContext context) async {
    String text = "Boulder styles";

    return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(text),
              titleTextStyle: Theme.of(context).textTheme.subtitle1,
              content: SingleChildScrollView(
                  physics: BouncingScrollPhysics(), child: Wrap(runSpacing: 10.0, children: _getBoulderStyleInfo())),
              contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
              actions: <Widget>[
                TextButton(
                    child: Text('Back'),
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      Navigator.of(context).pop();
                    })
              ]);
        });
  }

  List<Widget> _getBoulderStyleInfo() {
    return <Widget>[
      Align(
          alignment: Alignment.centerLeft,
          child: Text('Flash', style: TextStyle(fontWeight: FontWeight.bold, color: AppColors.accentColor))),
      Text("A successful ascent at the first attempt, with no prior practice."),
      Divider(thickness: 2.0),
      Align(
          alignment: Alignment.centerLeft,
          child: Text('Top', style: TextStyle(fontWeight: FontWeight.bold, color: AppColors.accentColor))),
      Text("A successful ascent after one or more previous unsuccessful attempts. Completed without falling."),
      Divider(thickness: 2.0),
      Align(
          alignment: Alignment.centerLeft,
          child: Text('Project', style: TextStyle(fontWeight: FontWeight.bold, color: AppColors.accentColor))),
      Text("An unsuccessful attempt because of falling or not topping out."),
    ];
  }

  List<Widget> createBoulderInfo(Boulder boulder) {
    return <Widget>[
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Location:", style: TextStyle(fontWeight: FontWeight.w600)),
            SizedBox(width: Constants.STANDARD_PADDING),
            Flexible(
                child: Text(boulder.location, maxLines: 2, overflow: TextOverflow.ellipsis, textAlign: TextAlign.end))
          ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Date:", style: TextStyle(fontWeight: FontWeight.w600)),
            SizedBox(width: Constants.STANDARD_PADDING),
            Flexible(child: Text("${DateFormat("dd.MM.yy").format(boulder.dateTime)}", overflow: TextOverflow.ellipsis))
          ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Name:", style: TextStyle(fontWeight: FontWeight.w600)),
                SizedBox(width: Constants.STANDARD_PADDING),
                Flexible(
                    child: Text(Util.getNameForFiltering(boulder.name),
                        maxLines: 2, overflow: TextOverflow.ellipsis, textAlign: TextAlign.end))
              ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Difficulty:", style: TextStyle(fontWeight: FontWeight.w600)),
                SizedBox(width: 8.0),
                Flexible(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${Util.getGrade(boulder.gradeID, boulder.gradeSystem)} (${boulder.gradeSystem})"),
                    if (boulder.gradeSystem != GradeManager().getGradeMapper().currentGradingSystem)
                      Text("${Util.getGrade(boulder.gradeID, GradeManager().getGradeMapper().currentGradingSystem)} "
                          "(${GradeManager().getGradeMapper().currentGradingSystem})")
                  ],
                ))
              ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Style:", style: TextStyle(fontWeight: FontWeight.w600)),
            Text(Util.getStyle(boulder.styleID))
          ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Rating:", style: TextStyle(fontWeight: FontWeight.w600)),
            Util.getRating(boulder.rating)
          ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Comment:", style: TextStyle(fontWeight: FontWeight.w600)),
                SizedBox(width: Constants.STANDARD_PADDING),
                Flexible(
                    child:
                        Text(Util.getComment(boulder.comment), overflow: TextOverflow.clip, textAlign: TextAlign.end))
              ])),
    ];
  }
}

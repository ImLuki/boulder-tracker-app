import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:toggle_switch/toggle_switch.dart';

class SessionEditDialog {
  TextEditingController boulderNameController = TextEditingController();
  TextEditingController commentController = TextEditingController();
  Map<String, dynamic> updatedData;

  Future<bool> showSessionDialog(BuildContext context, int sessionID, {Function onChanged, Function callBack}) async {
    List<Map<String, dynamic>> tmpData = await _loadSessionData(sessionID);
    Map<String, dynamic> sessionData = tmpData.first;
    updatedData = Map<String, dynamic>.from(sessionData);
    return showDialog<bool>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return GestureDetector(
              onVerticalDragUpdate: (dragDetails) {
                FocusScope.of(context).unfocus();
              },
              onTapDown: (tapDownDetails) {
                FocusScope.of(context).unfocus();
              },
              child: AlertDialog(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Edit Session"),
                      IconButton(
                          icon: Icon(Icons.delete_forever_rounded),
                          color: AppColors.accentColor,
                          onPressed: () => _showDeleteConfirmation(context, sessionID, callBack))
                    ],
                  ),
                  titleTextStyle: Theme.of(context).textTheme.subtitle1,
                  content: _getContent(context, sessionData),
                  contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
                  actions: <Widget>[
                    OutlinedButton(
                        onPressed: () => Navigator.of(context).pop(false),
                        child: Text("CANCEL", style: TextStyle(color: AppColors.accentColor)),
                        style: ButtonStyle(
                          overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                            if (states.contains(MaterialState.pressed)) {
                              return AppColors.accentColor.withOpacity(0.3);
                            }
                            return Colors.transparent;
                          }),
                        )),
                    MaterialButton(
                        color: AppColors.accentColor,
                        onPressed: () =>
                            {updateSessionInDataBase(updatedData), onChanged(), Navigator.of(context).pop(true)},
                        textColor: Colors.white,
                        child: Text("SAVE"))
                  ]));
        });
  }

  Widget _getContent(BuildContext context, Map<String, dynamic> sessionData) {
    return StatefulBuilder(builder: (context, setState) {
      return SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Wrap(
            alignment: WrapAlignment.center,
            direction: Axis.horizontal,
            runSpacing: Constants.STANDARD_PADDING,
            spacing: Constants.STANDARD_PADDING,
            children: [
              ToggleSwitch(
                initialLabelIndex: sessionData[DBController.sessionOutdoor],
                minWidth: MediaQuery.of(context).size.width * 0.25,
                minHeight: 45.0,
                cornerRadius: 20.0,
                activeBgColor: [Colors.green],
                activeFgColor: Colors.white,
                inactiveBgColor: AppColors.accentColor2,
                inactiveFgColor: Colors.white,
                labels: ['INDOOR', 'OUTDOOR'],
                onToggle: (index) {
                  updatedData[DBController.sessionOutdoor] = index;
                },
                totalSwitches: 2,
              ),
              Divider(thickness: 2.0),
              Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Icon(Icons.calendar_today_outlined),
                SizedBox(width: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(child: Text("Date:"), alignment: Alignment.centerLeft),
                    SizedBox(height: 15.0),
                    InkWell(
                      onTap: () => _selectDate(context, DateTime.parse(updatedData[DBController.sessionTimeStart]))
                          .then((value) => {if (value) setState(() {})}),
                      child: Text(_dateFormatter(DateTime.parse(updatedData[DBController.sessionTimeStart]))),
                    ),
                    SizedBox(height: 4),
                    InkWell(
                        onTap: () => _selectTime(context,
                                TimeOfDay.fromDateTime(DateTime.parse(updatedData[DBController.sessionTimeStart])))
                            .then((value) => {if (value) setState(() {})}),
                        child:
                            Text((DateFormat.jm().format(DateTime.parse(updatedData[DBController.sessionTimeStart]))))),
                  ],
                )
              ]),
              Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Icon(Icons.timer),
                SizedBox(width: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(child: Text("Duration:"), alignment: Alignment.centerLeft),
                    SizedBox(height: 15.0),
                    InkWell(
                        onTap: () => _selectDuration(context).then((value) => {if (value) setState(() {})}),
                        child: Text(
                          _getDuration(
                            DateTime.parse(updatedData[DBController.sessionTimeStart]),
                            DateTime.parse(updatedData[DBController.sessionTimeEnd]),
                          ),
                        )),
                  ],
                )
              ]),
              Divider(thickness: 2.0),
              TextFormField(
                initialValue: sessionData[DBController.sessionLocation],
                textAlign: TextAlign.start,
                textAlignVertical: TextAlignVertical.center,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
                onChanged: (value) {
                  updatedData[DBController.sessionLocation] = value.trim();
                },
                textCapitalization: TextCapitalization.words,
                decoration: new InputDecoration(
                  isDense: true,
                  labelText: "Enter location",
                  labelStyle: TextStyle(fontSize: 18, color: AppColors.accentColor2),
                  fillColor: AppColors.accentColor,
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                    borderSide: new BorderSide(),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                    borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                  ),
                ),
              ),
              Divider(thickness: 2.0),
              Align(child: Text("Ascends:"), alignment: Alignment.centerLeft),
              StatefulBuilder(builder: (context, setState) {
                return FutureBuilder(
                  future: _loadBoulder(sessionData[DBController.sessionId]),
                  builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Container(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return _createAscendList(snapshot.data, setState);
                  },
                );
              })
            ],
          ));
    });
  }

  Widget _createAscendList(List<Map<String, dynamic>> boulder, setState) {
    List<Widget> items = [];
    for (Map<String, dynamic> ascend in boulder) {
      items.add(createItem(ascend, setState));
    }
    return Wrap(
      children: items,
    );
  }

  Widget createItem(Map<String, dynamic> boulder, setState) {
    return Padding(
        padding: EdgeInsets.only(
          left: Constants.STANDARD_PADDING,
          right: Constants.STANDARD_PADDING,
          bottom: 10.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Flexible(child: Text(Util.getBoulderName(boulder[DBController.name]), overflow: TextOverflow.ellipsis)),
            ]),
            Text(
                Util.getSummary(
                  boulder[DBController.gradeId],
                  boulder[DBController.styleId],
                  boulder[DBController.gradeSystem],
                ),
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal)),
          ],
        ));
  }

  Future<void> _showDeleteConfirmation(BuildContext context, int sessionId, Function callBack) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          titleTextStyle: Theme.of(context).textTheme.subtitle1,
          contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 32),
              SizedBox(width: 8.0),
              Text('Confirm deletion'),
            ],
          ),
          content: SingleChildScrollView(
            child: Text('The selected session and all associated data including all ascends will be deleted.'),
          ),
          actions: <Widget>[
            OutlinedButton(
                onPressed: () => {
                      Navigator.of(context).pop(false),
                    },
                child: Text("CANCEL", style: TextStyle(color: AppColors.accentColor)),
                style: ButtonStyle(
                  overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                    if (states.contains(MaterialState.pressed)) {
                      return AppColors.accentColor.withOpacity(0.3);
                    }
                    return Colors.transparent;
                  }),
                )),
            MaterialButton(
                color: AppColors.accentColor,
                onPressed: () => {_deleteSession(sessionId, context, callBack)},
                textColor: Colors.white,
                child: Text("CONFIRM"))
          ],
        );
      },
    );
  }

  void _deleteSession(int sessionId, BuildContext context, Function callBack) async {
    DBController dbController = DBController();
    await dbController.deleteAscends(sessionId);
    await dbController.delete(DBController.sessionTable, sessionId);
    Navigator.of(context).pop(false);
    Navigator.of(context).pop(false);
    Navigator.of(context).pop(false);
    callBack();
  }

  void updateSessionInDataBase(Map<String, dynamic> data) {
    DBController dbController = DBController();
    dbController.updateSessionTable(data, data[DBController.sessionId]);
    Data().loadLocations();
  }

  Future<List<Map<String, dynamic>>> _loadSessionData(int sessionID) {
    return DBController().queryRows(
      DBController.sessionTable,
      where: "${DBController.sessionId} == (?)",
      whereArgs: [sessionID],
    );
  }

  Future<List<Map<String, dynamic>>> _loadBoulder(int sessionID) {
    return DBController().queryBoulderBySessionId(sessionID);
  }

  String _getDuration(DateTime start, DateTime end) {
    final int difference = end.difference(start).inMinutes.abs();
    final int hours = difference ~/ 60;
    final int minutes = difference % 60;
    return "${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')} h";
  }

  String _dateFormatter(DateTime date) {
    dynamic dayData = {"1": "Mon", "2": "Tue", "3": "Wed", "4": "Thur", "5": "Fri", "6": "Sat", "7": "Sun"};

    dynamic monthData = {
      "1": "Jan",
      "2": "Feb",
      "3": "Mar",
      "4": "Apr",
      "5": "May",
      "6": "June",
      "7": "Jul",
      "8": "Aug",
      "9": "Sep",
      "10": "Oct",
      "11": "Nov",
      "12": "Dec"
    };

    return dayData['${date.weekday}'] +
        ", " +
        date.day.toString() +
        " " +
        monthData['${date.month}'] +
        ". " +
        date.year.toString();
  }

  /// updating session start time
  /// returning true if date changes, false otherwise
  Future<bool> _selectDate(BuildContext context, DateTime selectedDate) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1995),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) {
      DateTime endTime = DateTime.parse(updatedData[DBController.sessionTimeEnd]);
      DateTime startTime = DateTime.parse(updatedData[DBController.sessionTimeStart]);
      Duration delta = endTime.difference(startTime);
      updatedData[DBController.sessionTimeStart] = picked.toIso8601String();
      updatedData[DBController.sessionTimeEnd] = picked.add(delta).toIso8601String();
      return true;
    }
    return false;
  }

  /// updating session start time
  /// returning true if date changes, false otherwise
  Future<bool> _selectTime(BuildContext context, TimeOfDay selectedTime) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) {
      DateTime endTime = DateTime.parse(updatedData[DBController.sessionTimeEnd]);
      DateTime startTime = DateTime.parse(updatedData[DBController.sessionTimeStart]);
      Duration delta = endTime.difference(startTime);
      DateTime updatedTime = DateTime(startTime.year, startTime.month, startTime.day, picked.hour, picked.minute);
      updatedData[DBController.sessionTimeStart] = updatedTime.toIso8601String();
      updatedData[DBController.sessionTimeEnd] = updatedTime.add(delta).toIso8601String();
      return true;
    }
    return false;
  }

  /// updating session duration
  /// returning true if date changes, false otherwise
  Future<bool> _selectDuration(BuildContext context) async {
    DateTime endTime = DateTime.parse(updatedData[DBController.sessionTimeEnd]);
    DateTime startTime = DateTime.parse(updatedData[DBController.sessionTimeStart]);
    Duration diff = endTime.difference(startTime);
    TimeOfDay selectedTime = TimeOfDay(hour: diff.inHours, minute: diff.inMinutes % 60);

    final TimeOfDay picked = await showTimePicker(
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
      context: context,
      helpText: "SELECT DURATION",
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) {
      DateTime startTime = DateTime.parse(updatedData[DBController.sessionTimeStart]);
      DateTime updatedTime = startTime.add(Duration(hours: picked.hour, minutes: picked.minute));
      updatedData[DBController.sessionTimeEnd] = updatedTime.toIso8601String();
      return true;
    }
    return false;
  }
}

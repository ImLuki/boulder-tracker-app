import 'dart:io';
import 'dart:ui';
import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/util/util.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'end_session_dialog.dart';

class ConfirmDialog {
  static const double SMALL_FONT_SIZE = 14;
  static final ImageFilter blurFilter = ImageFilter.blur(sigmaX: 6, sigmaY: 6);

  Future<void> showEndSessionDialog(BuildContext context) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, setState) {
            return EndSessionDialog();
          });
        });
  }

  Future<bool> showBoulderDialog(BuildContext context, Boulder boulder) async {
    Boulder existingBoulder;
    if (boulder.styleID == 0) {
      existingBoulder = await Data().findAscendWithSameName(boulder);
    }
    return showDialog<bool>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("Boulder:"),
              titleTextStyle: Theme.of(context).textTheme.subtitle1,
              content: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Wrap(runSpacing: 10.0, children: _createAscendInfo(boulder, existingBoulder))),
              contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
              actions: <Widget>[
                OutlinedButton(
                    onPressed: () => {FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
                    child: Text("CANCEL", style: TextStyle(color: AppColors.accentColor)),
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                        if (states.contains(MaterialState.pressed)) {
                          return AppColors.accentColor.withOpacity(0.3);
                        }
                        return Colors.transparent;
                      }),
                    )),
                MaterialButton(
                    color: AppColors.accentColor,
                    onPressed: () => _saveBoulder(context),
                    textColor: Colors.white,
                    child: Text(existingBoulder == null ? "SAVE" : "DONT CHANGE")),
                if (existingBoulder != null)
                  MaterialButton(
                      color: AppColors.accentColor,
                      onPressed: () => {boulder.styleID = 1, _saveBoulder(context)},
                      textColor: Colors.white,
                      child: Text(existingBoulder == null ? "SAVE" : "CHANGE")),
              ]);
        });
  }

  Future<bool> showDeleteDatabaseDialog(BuildContext context, Function onAccept) async {
    return showDialog<bool>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
              titleTextStyle: Theme.of(context).textTheme.subtitle1,
              scrollable: true,
              content: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                new Container(
                    child: Icon(Icons.delete_forever_rounded, color: Colors.white, size: 42),
                    width: 50.0,
                    height: 50.0,
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.circle,
                    )),
                SizedBox(width: 14.0),
                Flexible(
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  SizedBox(height: Constants.SMALL_PADDING),
                  Text("Delete Data?", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: Constants.SMALL_PADDING),
                  Text("You will permanently lose your:",
                      style: TextStyle(fontSize: SMALL_FONT_SIZE, fontWeight: FontWeight.normal),
                      overflow: TextOverflow.clip),
                  SizedBox(height: Constants.SMALL_PADDING),
                  Text("- sessions",
                      style: TextStyle(fontSize: SMALL_FONT_SIZE, fontWeight: FontWeight.normal),
                      overflow: TextOverflow.clip),
                  SizedBox(height: 3.0),
                  Text("- ascends",
                      style: TextStyle(fontSize: SMALL_FONT_SIZE, fontWeight: FontWeight.normal),
                      overflow: TextOverflow.clip),
                  SizedBox(height: 3.0),
                  Text("- statistics",
                      style: TextStyle(fontSize: SMALL_FONT_SIZE, fontWeight: FontWeight.normal),
                      overflow: TextOverflow.clip)
                ]))
              ]),
              contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
              actions: <Widget>[
                OutlinedButton(
                    onPressed: () => {FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
                    child: Text("Cancel", style: TextStyle(color: Colors.red.shade700)),
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                        if (states.contains(MaterialState.pressed)) {
                          return Colors.red.shade700.withOpacity(0.3);
                        }
                        return Colors.transparent;
                      }),
                    )),
                MaterialButton(
                    color: Colors.red.shade700,
                    onPressed: () => {onAccept(), FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
                    textColor: Colors.white,
                    child: Text("Delete"))
              ]);
        });
  }

  Future<bool> showLoadBackupDialog(BuildContext context, Function onAccept) async {
    return showDialog<bool>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
              titleTextStyle: Theme.of(context).textTheme.subtitle1,
              scrollable: true,
              content: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
                SizedBox(width: 14.0),
                Flexible(
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  SizedBox(height: Constants.SMALL_PADDING),
                  Text("Load Backup?", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: Constants.SMALL_PADDING),
                  Text("Attention! Current data will be deleted. Make sure you have a backup of your data.",
                      style: TextStyle(fontSize: SMALL_FONT_SIZE, fontWeight: FontWeight.normal),
                      overflow: TextOverflow.clip),
                ]))
              ]),
              contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
              actions: <Widget>[
                OutlinedButton(
                    onPressed: () => {FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
                    child: Text("Cancel", style: TextStyle(color: AppColors.accentColor)),
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                        if (states.contains(MaterialState.pressed)) {
                          return AppColors.accentColor.withOpacity(0.3);
                        }
                        return Colors.transparent;
                      }),
                    )),
                MaterialButton(
                    color: AppColors.accentColor,
                    onPressed: () => {FocusScope.of(context).unfocus(), Navigator.pop(context), onAccept()},
                    textColor: Colors.white,
                    child: Text("Load backup"))
              ]);
        });
  }

  Future<bool> showChooseBackupDialog(
      BuildContext context, Function(String) onAccept, List<FileSystemEntity> backupList) async {
    return showDialog<bool>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
              titleTextStyle: Theme.of(context).textTheme.subtitle1,
              content: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(children: [
                    Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
                      SizedBox(width: 14.0),
                      Flexible(
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                        SizedBox(height: Constants.SMALL_PADDING),
                        Text("Load Backup?", style: TextStyle(fontWeight: FontWeight.bold)),
                        SizedBox(height: Constants.SMALL_PADDING),
                        Text(
                            "Attention! Current data will be deleted. Make sure you have a backup of your data."
                            "\nYou can choose one of the following backups: ",
                            style: TextStyle(fontSize: SMALL_FONT_SIZE, fontWeight: FontWeight.normal),
                            overflow: TextOverflow.clip),
                      ])),
                    ]),
                    SizedBox(height: Constants.STANDARD_PADDING),
                    ...List.generate(
                        backupList.length,
                        (index) => InkWell(
                            child: Container(
                                decoration: BoxDecoration(
                                    border: new Border(
                                        bottom: const BorderSide(color: Colors.black26),
                                        top: index == 0
                                            ? const BorderSide(color: Colors.black26)
                                            : const BorderSide(color: Colors.transparent))),
                                child: ListTile(
                                    trailing: Icon(Icons.north_west, size: 18),
                                    visualDensity: VisualDensity.compact,
                                    title: Text(
                                        backupList[index].path.split("/").last.replaceAll("-boulder-tracker.json", ""),
                                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal)),
                                    onTap: () => {
                                          FocusScope.of(context).unfocus(),
                                          Navigator.pop(context),
                                          onAccept(backupList[index].path)
                                        }))))
                  ])),
              contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
              actions: <Widget>[
                OutlinedButton(
                    onPressed: () => {FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
                    child: Text("Cancel", style: TextStyle(color: AppColors.accentColor)),
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                        if (states.contains(MaterialState.pressed)) {
                          return AppColors.accentColor.withOpacity(0.3);
                        }
                        return Colors.transparent;
                      }),
                    )),
              ]);
        });
  }

  void _saveBoulder(BuildContext context) {
    FocusScope.of(context).unfocus();
    Navigator.of(context).pop(true);
    Data().addCurrentBoulder();
  }

  List<Widget> _createAscendInfo(Boulder boulder, Boulder existingBoulder) {
    return <Widget>[
      existingBoulder == null
          ? Container()
          : Row(children: [
              Icon(Icons.warning_amber_outlined, color: Colors.orange, size: 32),
              SizedBox(width: Constants.SMALL_PADDING),
              Flexible(
                  child: Text(
                      "The following boulder may have been entered incorrectly. "
                      "The boulder was already ascended on ${DateFormat("yMMMMEEEEd").format(existingBoulder.dateTime)}.\n"
                      "Change proposal:",
                      overflow: TextOverflow.clip,
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal)))
            ]),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Name:", style: TextStyle(fontWeight: FontWeight.w600)),
                SizedBox(width: Constants.STANDARD_PADDING),
                Flexible(
                    child: Text(
                  Util.getBoulderName(boulder.name),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ))
              ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Difficulty:", style: TextStyle(fontWeight: FontWeight.w600)),
            Text(Util.getGrade(boulder.gradeID, boulder.gradeSystem))
          ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Style:", style: TextStyle(fontWeight: FontWeight.w600)),
            existingBoulder == null
                ? Text(Util.getStyle(boulder.styleID))
                : Row(children: [
                    Text(Util.getStyle(boulder.styleID), style: TextStyle(color: Colors.red)),
                    Text(" -> "),
                    Text("Top", style: TextStyle(color: Colors.green), overflow: TextOverflow.ellipsis)
                  ])
          ])),
      Padding(
          padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text("Rating:", style: TextStyle(fontWeight: FontWeight.w600)),
            Util.getRating(boulder.rating)
          ])),
      Visibility(
          visible: boulder.comment != null && boulder.comment != "",
          child: Padding(
              padding: EdgeInsets.only(right: Constants.STANDARD_PADDING),
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Comment:", style: TextStyle(fontWeight: FontWeight.w600)),
                    SizedBox(width: Constants.STANDARD_PADDING),
                    Flexible(
                        child: Text(boulder.comment ?? "",
                            maxLines: 5, overflow: TextOverflow.ellipsis, textAlign: TextAlign.end))
                  ])))
    ];
  }
}

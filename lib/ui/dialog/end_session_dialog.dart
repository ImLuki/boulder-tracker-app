import 'package:boulder_track/app/app_colors.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/util/notifications_handler.dart';
import 'package:flutter/material.dart';

class EndSessionDialog extends StatefulWidget {
  @override
  _EndSessionDialogState createState() => _EndSessionDialogState(DateTime.now());
}

class _EndSessionDialogState extends State<EndSessionDialog> {
  DateTime endTime;

  _EndSessionDialogState(this.endTime);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        titleTextStyle: Theme.of(context).textTheme.subtitle1,
        content: createContent(context),
        contentTextStyle: Theme.of(context).textTheme.bodyText2.copyWith(fontWeight: FontWeight.w400),
        actions: <Widget>[
          OutlinedButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("CANCEL", style: TextStyle(color: AppColors.accentColor)),
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.resolveWith<Color>((states) {
                  if (states.contains(MaterialState.pressed)) {
                    return AppColors.accentColor.withOpacity(0.3);
                  }
                  return Colors.transparent;
                }),
              )),
          MaterialButton(
              color: AppColors.accentColor,
              onPressed: () => {
                    NotificationHandler().cancelNotification(),
                    if (Data().getCurrentSession().boulderCount <= 0)
                      {
                        Data().deleteCurrentSession(),
                        SessionState().setState(SessionStateEnum.none),
                        Navigator.of(context).pop()
                      }
                    else
                      {Data().endSession(endTime), SessionState().nextState(), Navigator.of(context).pop()}
                  },
              textColor: Colors.white,
              child: Data().getCurrentSession().boulderCount <= 0 ? Text("DELETE") : Text("END"))
        ]);
  }

  Widget createContent(BuildContext context) {
    Duration diff = endTime.difference(Data().getCurrentSession().startDate);
    if (Data().getCurrentSession().boulderCount <= 0) {
      return Text("No boulder has been added. Do you want to delete the session?");
    } else {
      return Wrap(runSpacing: 14.0, children: [
        Align(
            alignment: Alignment.center,
            child: Text("SESSION-TIME:", style: Theme.of(context).textTheme.headline4.copyWith(color: Colors.black))),
        InkWell(
            onTap: () {
              showTimePicker(
                      builder: (BuildContext context, Widget child) {
                        return MediaQuery(
                          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
                          child: child,
                        );
                      },
                      helpText: "Change the duration of the session:",
                      context: context,
                      initialTime: TimeOfDay(hour: diff.inHours, minute: diff.inMinutes % 60))
                  .then((time) => {
                        if (time != null)
                          {
                            endTime = Data()
                                .getCurrentSession()
                                .startDate
                                .add(Duration(hours: time.hour, minutes: time.minute)),
                            setState(() {})
                          }
                      });
            },
            child: Stack(children: [
              Align(alignment: Alignment.centerRight, child: Icon(Icons.more_time, color: Colors.black54)),
              Align(
                  alignment: Alignment.center,
                  child: Text("${Data().getCurrentSession().getDurationD(endTime)} h",
                      style: Theme.of(context).textTheme.subtitle1.copyWith(color: AppColors.accentColor)))
            ])),
      ]);
    }
  }
}

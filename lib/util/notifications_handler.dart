import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/ui/dialog/end_session_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class NotificationHandler {
  NotificationHandler._privateConstructor();

  factory NotificationHandler() {
    return _instance;
  }

  static final NotificationHandler _instance = NotificationHandler._privateConstructor();
  MethodChannel platform = MethodChannel('dexterx.dev/flutter_local_notifications_example');
  BuildContext _context;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  bool _initialized = false;
  bool _notificationsActivated;
  int _reminderDuration;

  void init(BuildContext context) async {
    if (_initialized) {
      return;
    }
    _initialized = true;
    SharedPreferences.getInstance().then((prefs) => {
          _notificationsActivated = prefs.getBool("notifications_activated") ?? true,
          reminderDuration = prefs.getInt("session_reminder_duration")
        });
    this._context = context;
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('mipmap/ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final MacOSInitializationSettings initializationSettingsMacOS = MacOSInitializationSettings();
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS, macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    await _configureLocalTimeZone();
  }

  Future<void> _configureLocalTimeZone() async {
    tz.initializeTimeZones();
    //final String timeZoneName = await platform.invokeMethod('getTimeZoneName');
    //tz.setLocalLocation(tz.getLocation(timeZoneName));
  }

  Future<void> cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> scheduleNotification() async {
    if (!_notificationsActivated) {
      return;
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        "Reminder: The session started ${(prefs.getInt("session_reminder_duration") / 60).toStringAsFixed(1)} hours ago.",
        "Would you like to close it?",
        tz.TZDateTime.now(tz.local).add(Duration(minutes: prefs.getInt("session_reminder_duration"))),
        const NotificationDetails(
            android: AndroidNotificationDetails(
          'full screen channel id',
          'full screen channel name',
          priority: Priority.high,
          importance: Importance.high,
          fullScreenIntent: false,
          enableVibration: true,
        )),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime);
  }

  Future onSelectNotification(String payload) async {
    await _showDialog();
  }

  Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    await _showDialog();
  }

  _showDialog() async {
    if (SessionState().currentState != SessionStateEnum.active) {
      return;
    } else {
      await showDialog<void>(
          context: _context,
          barrierDismissible: false, // user must tap button
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (context, setState) {
              return EndSessionDialog();
            });
          });
    }
  }

  bool get notificationsActivated => _notificationsActivated;

  set notificationsActivated(bool value) {
    SharedPreferences.getInstance().then((prefs) => prefs.setBool("notifications_activated", value));
    _notificationsActivated = value;
  }

  int get reminderDuration => _reminderDuration;

  set reminderDuration(int value) {
    SharedPreferences.getInstance().then((prefs) => prefs.setInt("session_reminder_duration", value));
    _reminderDuration = value;
  }
}

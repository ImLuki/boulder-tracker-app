import 'package:boulder_track/backend/Location.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'backend/search_settings.dart';

class Init {
  static Future initialize() async {
    await _registerServices();
    await _loadSettings();
  }

  static _registerServices() async {}

  static _loadSettings() async {
    print("starting loading settings");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int sessionReminderDuration = prefs.getInt("session_reminder_duration") ?? 180;
    await prefs.setInt("session_reminder_duration", sessionReminderDuration);
    await GradeManager().init();
    Data data = Data();
    await data.loadData();
    await SessionState().init(data.getCurrentSession() != null);
    await Location().init();
    await SearchSettings().init();
    print("finished loading settings");
  }
}

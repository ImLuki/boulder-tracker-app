import 'dart:io';

import 'package:boulder_track/backend/model/data.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBController {
  @protected
  static final _databaseName = "TrackBoulder.db";

  static final sessionTable = 'session';
  static final boulderTable = 'boulders';
  static final locationsTable = 'locations';

  /// session table columns
  static final sessionId = 'session_id';
  static final sessionStatus = 'session_status';
  static final sessionTimeStart = 'time_start';
  static final sessionTimeEnd = 'time_end';
  static final sessionOutdoor = 'outdoor';
  static final sessionLocation = 'location';

  /// boulder table columns
  static final boulderId = "boulder_id";
  static final gradeId = "grade_id";
  static final gradeSystem = "grade_system";
  static final styleId = "style_id";
  static final name = "boulder_name";
  static final rating = "rating";
  static final marked = "marked";
  static final comment = "comment";
  static final tries = "tries";
  static final boulderType = "boulder_type";
  static final boulderStart = "boulder_start";

  //TODO implement these everywhere

  /// location table columns
  static final longitude = 'longitude';
  static final latitude = 'latitude';

  DBController._privateConstructor();

  static final DBController instance = DBController._privateConstructor();

  factory DBController() {
    return instance;
  }

  @protected
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $sessionTable (
            $sessionId INTEGER PRIMARY KEY AUTOINCREMENT,
            $sessionStatus TEXT NOT NULL,
            $sessionTimeStart DATE NOT NULL,
            $sessionTimeEnd DATE,
            $sessionOutdoor BOOLEAN NOT NULL,
            $sessionLocation TEXT
          )
          ''');
    await db.execute('''
          CREATE TABLE $boulderTable (
            $boulderId INTEGER PRIMARY KEY AUTOINCREMENT,
            $sessionId INTEGER NOT NULL,
            $gradeId INTEGER NOT NULL,
            $gradeSystem TEXT,
            $styleId INTEGER NOT NULL,
            $tries INTEGER NOT NULL,
            $name TEXT NOT NULL,
            $rating INTEGER NOT NULL,
            $marked BOOLEAN NOT NULL,
            $comment TEXT
          )
          ''');
    await db.execute('''
          CREATE TABLE $locationsTable (
            $sessionLocation TEXT PRIMARY KEY,
            $longitude DOUBLE,
            $latitude DOUBLE
          )
          ''');
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {}

  Future<int> insert(String table, Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<int> delete(String table, int id) async {
    Database db = await instance.database;
    return await db.delete(table, where: '$sessionId = ?', whereArgs: [id]);
  }

  Future<int> deleteAscend(int id) async {
    Database db = await instance.database;
    return await db.delete(boulderTable, where: '$boulderId = ?', whereArgs: [id]);
  }

  Future<int> deleteAscends(int sessionIDLocal) async {
    Database db = await instance.database;
    return await db.delete(boulderTable, where: '$sessionId == ?', whereArgs: [sessionIDLocal]);
  }

  Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
    Database db = await instance.database;
    return await db.query(table);
  }

  Future<List<Map<String, dynamic>>> rawQuery(String sql, [List<dynamic> arguments]) async {
    Database db = await instance.database;
    return await db.rawQuery(sql, arguments);
  }

  Future<List<Map<String, dynamic>>> queryRows(String table,
      {String where, List<dynamic> whereArgs, String groupBy, String orderBy, int limit}) async {
    Database db = await instance.database;
    return await db.query(
      table,
      where: where,
      whereArgs: whereArgs,
      orderBy: orderBy,
      groupBy: groupBy,
      limit: limit,
    );
  }

  Future<List<Map<String, dynamic>>> queryBoulderBySessionId(int id) async {
    Database db = await instance.database;
    return await db.query(boulderTable, where: '$sessionId = ?', whereArgs: [id]);
  }

  queryCoordinatesByLocation(String location) async {
    Database db = await instance.database;
    return await db.query(locationsTable, where: '$sessionLocation = (?)', whereArgs: [location]);
  }

  Future<void> updateSessionTable(Map<String, dynamic> row, int sessionId) async {
    Database db = await instance.database;
    await db.update(sessionTable, row, where: "${DBController.sessionId} = ?", whereArgs: [sessionId]);
    return;
  }

  Future<void> updateBoulder(Map<String, dynamic> row, int boulderId) async {
    Database db = await instance.database;
    await db.update(boulderTable, row, where: "${DBController.boulderId} = ?", whereArgs: [boulderId]);
  }

  getActiveSessionId() async {
    Database db = await instance.database;
    var result = await db.query(sessionTable, where: "$sessionStatus = ?", whereArgs: [Data.SESSION_ACTIVE_STATE]);
    return result.first[sessionId];
  }

  deleteAll() async {
    Database db = await instance.database;
    db.delete(boulderTable);
    db.delete(sessionTable);
    db.delete(locationsTable);
  }
}

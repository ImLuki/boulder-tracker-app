import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Location {
  Location._privateConstructor();

  static final Location _instance = Location._privateConstructor();

  factory Location() {
    return _instance;
  }

  bool _saveLocation;

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this._saveLocation = prefs.getBool("location_enabled") ?? false;
  }

  bool get saveLocation => _saveLocation;

  Future<void> setLocation(bool value) async {
    _saveLocation = value;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("location_enabled", value);
  }

  Future<bool> hasPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      //return Future.error('Location services are disabled.');
      return false;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      //return Future.error('Location permissions are permanently denied, we cannot request permissions.');
      return false;
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse && permission != LocationPermission.always) {
        //return Future.error('Location permissions are denied (actual value: $permission).');
        return false;
      }
    }

    return true;
  }

  Future<Position> determinePosition() async {
    if (await hasPermission()) {
      return await Geolocator.getCurrentPosition();
    } else {
      return Future.error('Location permissions are denied, cannot request location.');
    }
  }
}

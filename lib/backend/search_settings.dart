import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSettings {
  SearchSettings._privateConstructor();

  @protected
  static SearchSettings _instance = SearchSettings._privateConstructor();

  factory SearchSettings() {
    return _instance;
  }

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this._currentSorting = Sorting.values[prefs.getInt("ascend_list_sorting") ?? 0];
  }

  bool onlyCurrentLocation = false;
  bool indoor = true;
  bool outdoor = true;
  bool onlyFlashes = false;
  bool onlyNotTopped = false;
  bool onlyMarked = false;
  bool onlyBestAscend = false;
  Sorting _currentSorting = Sorting.Newest;
  String currentLocation = "All Locations";

  void reset() {
    onlyCurrentLocation = false;
    indoor = true;
    outdoor = true;
    onlyFlashes = false;
    onlyNotTopped = false;
    onlyMarked = false;
    _currentSorting = Sorting.Newest;
    currentLocation = "All Locations";
  }

  List<dynamic> getSQLConditionString() {
    String sql = "1";
    List<dynamic> arguments = [];
    if (this.onlyCurrentLocation) {
      sql += " AND ${DBController.sessionLocation} == (?)";
      arguments.add(Data().getCurrentSession().locationName);
    } else if (this.currentLocation != "All Locations") {
      sql += " AND ${DBController.sessionLocation} == (?)";
      arguments.add(this.currentLocation);
    }
    if (!this.indoor || !this.outdoor) {
      if (this.indoor) {
        sql += " AND NOT ${DBController.sessionOutdoor}";
      }
      if (this.outdoor) {
        sql += " AND ${DBController.sessionOutdoor}";
      }
    }
    if (onlyFlashes) {
      sql += " AND ${DBController.styleId} == (?)";
      arguments.add(1);
    } else if (onlyNotTopped) {
      sql += " AND ${DBController.styleId} >= (?)";
      arguments.add(2);
    }
    if (onlyMarked) {
      sql += " AND ${DBController.marked}";
    }
    return [sql, arguments];
  }

  Sorting get currentSorting => _currentSorting;

  set currentSorting(Sorting value) {
    _currentSorting = value;
    SharedPreferences.getInstance().then((pref) => pref.setInt("ascend_list_sorting", value.index));
  }
}

enum Sorting { Newest, Oldest, Alphabetic, Easiest, Hardest, Best }

Map sortingKeys = {
  "Newest": "${DBController.sessionTimeStart} DESC",
  "Oldest": DBController.sessionTimeStart,
  "Alphabetic": "${DBController.name} == '', ${DBController.name}",
  "Easiest": DBController.gradeId,
  "Hardest": "${DBController.gradeId} DESC",
  "Best": "${DBController.rating} DESC"
};

class Enum {
  Enum._();

  static String name(value) {
    return value.toString().split('.').last;
  }

  static Sorting toEnum(value) {
    return Sorting.values.firstWhere((e) => e.toString() == value);
  }

  static String getSortingKey(Sorting sorting) {
    return sortingKeys[Enum.name(sorting)];
  }
}

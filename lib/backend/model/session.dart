import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:flutter/material.dart';

class Session with ChangeNotifier {
  int sessionId;
  DateTime startDate;
  DateTime endDate;
  String locationName;
  bool _outdoor;
  int boulderCount;

  Session(this.startDate, this.endDate, this.locationName, this._outdoor, this.boulderCount, {this.sessionId});

  bool start() {
    if (locationName == "") {
      return false;
    } else {
      this.startDate = DateTime.now();
      return true;
    }
  }

  void stop(DateTime end) {
    this.endDate = end;
  }

  String getDuration() {
    return getDurationD(this.endDate);
  }

  String getDurationD(DateTime end) {
    final int difference = end.difference(this.startDate).inMinutes.abs();
    final int hours = difference ~/ 60;
    final int minutes = difference % 60;
    return "${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}";
  }

  void addBoulder() {
    this.boulderCount++;
    notifyListeners();
  }

  int getSecondsSinceStart() {
    return DateTime.now().difference(this.startDate).inSeconds.abs();
  }

  bool get outdoor => _outdoor;

  set outdoor(bool value) {
    _outdoor = value;
    notifyListeners();
  }

  Map<String, dynamic> toMap(String status) {
    var map = {
      DBController.sessionStatus: status,
      DBController.sessionTimeStart: this.startDate == null ? null : this.startDate.toIso8601String(),
      DBController.sessionTimeEnd: this.endDate == null ? null : this.endDate.toIso8601String(),
      DBController.sessionOutdoor: this._outdoor ? 1 : 0,
      DBController.sessionLocation: this.locationName
    };
    if (sessionId != null) map[DBController.sessionId] = sessionId;
    return map;
  }

  Map<String, dynamic> toJson(int sessionId) {
    return {
      DBController.sessionId: sessionId,
      DBController.sessionTimeStart: this.startDate == null ? null : this.startDate.toIso8601String(),
      DBController.sessionTimeEnd: this.endDate == null ? null : this.endDate.toIso8601String(),
      DBController.sessionOutdoor: this._outdoor ? 1 : 0,
      DBController.sessionLocation: this.locationName
    };
  }
}

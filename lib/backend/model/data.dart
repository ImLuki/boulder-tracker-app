import 'dart:io';

import 'package:boulder_track/backend/Location.dart';
import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/model/session.dart';
import 'package:boulder_track/backend/state.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/backend/util/json_creator.dart';
import 'package:boulder_track/backend/util/style_mapper.dart';
import 'package:geolocator/geolocator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart';

class Data {
  Data._privateConstructor();

  factory Data() {
    return _instance;
  }

  static final Data _instance = Data._privateConstructor();

  final DBController dbController = DBController();

  int sessionCount = 0;
  Map<String, List<String>> _locations = {INDOOR_KEY: [], OUTDOOR_KEY: []};

  Session _currentSession;
  int _currentSessionId;
  Boulder currentBoulder;
  bool _limitCharts = true;

  static const int MAX_SESSION_AMOUNT = 16;
  static const String SESSION_ACTIVE_STATE = "active";
  static const String SESSION_CLOSED_STATE = "end";
  static const String INDOOR_KEY = "indoor";
  static const String OUTDOOR_KEY = "outdoor";
  static const int STANDARD_BOULDER_GRADE = 11700;

  Future<bool> loadData() async {
    bool result = await loadSessions();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this._limitCharts = prefs.getBool("limit_charts") ?? true;
    await loadLocations();
    return result;
  }

  void reset() {
    this.sessionCount = 0;
    _locations = {INDOOR_KEY: [], OUTDOOR_KEY: []};
    this._currentSession = null;
    SessionState().setState(SessionStateEnum.none);
  }

  void createSession() {
    this._currentSession = Session(null, null, "", false, 0);
  }

  bool startSession() {
    if (!this._currentSession.start()) {
      return false;
    }
    dbController
        .insert(DBController.sessionTable, _currentSession.toMap(SESSION_ACTIVE_STATE))
        .then((value) => this._currentSession.sessionId = value);
    this.currentBoulder = Boulder(STANDARD_BOULDER_GRADE, GradeManager().getGradeMapper().currentGradingSystem, 2, 3,
        "", _currentSession.startDate, _currentSession.locationName, _currentSession.outdoor, false);
    _getSessionId();
    return true;
  }

  _getSessionId() async {
    this._currentSessionId = await dbController.getActiveSessionId();
  }

  void endSession(DateTime dateTime) {
    _createCoordinates();
    this._currentSession.stop(dateTime);
    this.sessionCount += 1;

    String key = this._currentSession.outdoor ? OUTDOOR_KEY : INDOOR_KEY;
    if (!_locations[key].contains(_currentSession.locationName)) {
      _locations[key].add(_currentSession.locationName);
    }

    dbController.updateSessionTable({
      DBController.sessionTimeEnd: _currentSession.endDate.toIso8601String(),
      DBController.sessionStatus: SESSION_CLOSED_STATE
    }, this._currentSessionId).then((value) => _createAutomaticBackup());
  }

  /// does not update Locations
  void addSession(Session session) {
    dbController.insert(DBController.sessionTable, session.toMap(SESSION_CLOSED_STATE));
    this.sessionCount += 1;
  }

  void deleteCurrentSession() {
    dbController.delete(DBController.boulderTable, _currentSessionId);
    dbController.delete(DBController.sessionTable, _currentSessionId);
    clearSession();
  }

  void clearSession() {
    this._currentSession = null;
  }

  List<String> getIndoorLocations() {
    return this._locations[INDOOR_KEY];
  }

  List<String> getOutdoorLocations() {
    return this._locations[OUTDOOR_KEY];
  }

  List<String> getLocations() {
    return this.getIndoorLocations() + this.getOutdoorLocations();
  }

  Session getCurrentSession() {
    return _currentSession;
  }

  void addCurrentBoulder() {
    Boulder copy = this.currentBoulder.copy();
    dbController
        .insert(DBController.boulderTable, copy.toMap(_currentSessionId))
        .then((value) => {copy.boulderID = value});

    copy.location = _currentSession.locationName;
    copy.outdoor = _currentSession.outdoor;
    copy.dateTime = _currentSession.startDate;
    _currentSession.addBoulder();
    this.currentBoulder.name = "";
    this.currentBoulder.comment = "";
  }

  void insertAscendToCurrentSession(int index, Map boulder) {
    Map<String, dynamic> insertBoulder = new Map<String, dynamic>.from(boulder);
    insertBoulder[DBController.sessionId] = _currentSessionId;
    dbController
        .insert(DBController.boulderTable, insertBoulder)
        .then((value) => insertBoulder[DBController.boulderId] = value);
    _currentSession.addBoulder();
  }

  bool removeAscendFromCurrentSession(Map boulder) {
    if (boulder[DBController.boulderId] == -1) {
      return false;
    }
    dbController.deleteAscend(boulder[DBController.boulderId]);
    this._currentSession.boulderCount--;
    return true;
  }

  /// does not load locations
  Future<bool> loadSessions() async {
    this.sessionCount = 0;
    this._currentSession = null;
    List<Map<String, dynamic>> result = await dbController.queryAllRows(DBController.sessionTable);
    this.sessionCount = result.length;
    for (Map element in result) {
      if (element[DBController.sessionStatus] != SESSION_CLOSED_STATE) {
        int boulderCount = await getBoulderCount(element[DBController.sessionId]);
        _currentSession = Session(
          DateTime.parse(element[DBController.sessionTimeStart]),
          null,
          element[DBController.sessionLocation],
          element[DBController.sessionOutdoor] == 1,
          boulderCount,
          sessionId: element[DBController.sessionId],
        );
        _currentSessionId = element[DBController.sessionId];
        this.currentBoulder = Boulder(
          STANDARD_BOULDER_GRADE,
          GradeManager().getGradeMapper().currentGradingSystem,
          StyleMapper().getDefaultBoulderStyle(),
          3,
          "",
          _currentSession.startDate,
          _currentSession.locationName,
          _currentSession.outdoor,
          false,
        );
      }
    }

    return true;
  }

  Future<void> loadLocations() async {
    Map<String, bool> indoor = Map();
    Map<String, bool> outdoor = Map();
    var result = await DBController().queryAllRows(DBController.sessionTable);
    result.forEach((session) {
      session[DBController.sessionOutdoor] == 1
          ? outdoor[session[DBController.sessionLocation]] = true
          : indoor[session[DBController.sessionLocation]] = true;
    });
    this._locations[INDOOR_KEY] = indoor.keys.toList();
    this._locations[OUTDOOR_KEY] = outdoor.keys.toList();
  }

  Future<int> getBoulderCount(int sessionID) async {
    List<Map<String, dynamic>> result = await dbController.queryBoulderBySessionId(sessionID);
    return result.length;
  }

  Future<Boulder> findAscendWithSameName(Boulder boulder) async {
    if (boulder.name == "") return null;

    var result = await DBController().rawQuery(
        "SELECT * FROM ${DBController.boulderTable} "
        "JOIN ${DBController.sessionTable} using(${DBController.sessionId}) "
        "WHERE LOWER(${DBController.name}) == (?) "
        "AND LOWER(${DBController.sessionLocation}) == (?) "
        "ORDER BY ${DBController.sessionTimeStart} DESC",
        [boulder.name.toLowerCase(), boulder.location.toLowerCase()]);

    if (result == null || result.isEmpty || result.first == null) return null;

    var element = result.first;

    return Boulder(
        element[DBController.gradeId],
        element[DBController.gradeSystem],
        element[DBController.styleId],
        element[DBController.rating],
        element[DBController.name],
        DateTime.parse(element[DBController.sessionTimeStart]),
        element[DBController.sessionLocation],
        element[DBController.sessionOutdoor] == 1,
        element[DBController.marked] == 1,
        comment: element[DBController.comment],
        boulderID: element[DBController.boulderId]);
  }

  void _createCoordinates() async {
    if (Location().saveLocation) {
      List<Map<String, dynamic>> result = await dbController.queryAllRows(DBController.locationsTable);
      if (result.any((element) => element[DBController.sessionLocation] == this._currentSession.locationName)) {
        return;
      } else {
        try {
          if (await Location().hasPermission()) {
            Position position = await Geolocator.getCurrentPosition();
            await dbController.insert(DBController.locationsTable, {
              DBController.sessionLocation: this._currentSession.locationName,
              DBController.latitude: position.latitude,
              DBController.longitude: position.longitude
            });
          }
        } on Exception catch (_) {}
      }
    }
  }

  Future<bool> _createAutomaticBackup() async {
    if (!await Permission.storage.isGranted) return false;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool result = prefs.getBool("automatic_backups") ?? false;

    if (!result) return false;

    bool deleteBackups = prefs.getBool("overwrite_automatic_backup") ?? true;

    String dir;
    if (Platform.isAndroid) {
      List<Directory> path = await getExternalStorageDirectories();
      dir = path[0].path;
    } else if (Platform.isIOS) {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      dir = join(documentsDirectory.path, "boulder_tracker");
    } else {
      return false;
    }

    await JsonCreator().saveBackupFile(dir, deleteBackups);

    return true;
  }

  bool get limitCharts => _limitCharts;

  set limitCharts(bool value) {
    _limitCharts = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool("limit_charts", value);
    });
  }

  int getStandardGrade() {
    return STANDARD_BOULDER_GRADE;
  }
}

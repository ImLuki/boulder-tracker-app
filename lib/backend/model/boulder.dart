import 'package:boulder_track/backend/database/database_controller.dart';

enum Type {
  vertical,
  slab,
  overhang,
  roof
}

enum BoulderStart {
  sit,
  stand
}

class Boulder {
  int boulderID;
  int gradeID;
  String gradeSystem;
  int styleID;
  int rating;
  int tries;

  String name;
  DateTime dateTime;
  String location;
  bool outdoor;
  bool marked;
  String comment;

  Boulder(this.gradeID, this.gradeSystem, this.styleID, this.rating, this.name, this.dateTime, this.location,
      this.outdoor, this.marked,
      {this.comment = "", this.boulderID = -1, this.tries = 1});

  Boulder copy() {
    return Boulder(
      gradeID,
      gradeSystem,
      styleID,
      rating,
      name,
      dateTime,
      location,
      outdoor,
      marked,
      boulderID: boulderID,
      comment: comment,
    );
  }

  Map<String, dynamic> toMap(int sessionId) {
    return {
      DBController.sessionId: sessionId,
      DBController.gradeId: this.gradeID,
      DBController.gradeSystem: this.gradeSystem,
      DBController.styleId: this.styleID,
      DBController.name: this.name,
      DBController.rating: this.rating,
      DBController.marked: this.marked ? 1 : 0,
      DBController.comment: this.comment,
      DBController.tries: this.tries
    };
  }

  Map<String, dynamic> toJson(int sessionId, int boulderId) {
    return {
      DBController.boulderId: boulderId,
      DBController.sessionId: sessionId,
      DBController.gradeId: this.gradeID,
      DBController.gradeSystem: this.gradeSystem,
      DBController.styleId: this.styleID,
      DBController.name: this.name,
      DBController.rating: this.rating,
      DBController.marked: this.marked ? 1 : 0,
      DBController.comment: this.comment,
      DBController.tries: this.tries
    };
  }
}

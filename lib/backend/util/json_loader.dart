import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:boulder_track/backend/model/data.dart';
import 'package:boulder_track/backend/model/session.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/util/json_parser.dart';

class JsonLoader {
  Data data = Data();
  Map<int, Session> sessionMap = Map();

  Future<bool> load(String path) async {
    try {
      JsonParser jsonParser = JsonParser();
      var _result = await jsonParser.parseJsonFile(path);

      // loading Location Position
      final DBController dbController = DBController();

      _loadSessions(_result["sessions"]);
      loadBoulder(_result["boulder"]);
      _loadCoordinates(dbController, _result["positions"]);

      sessionMap.forEach((key, value) {
        data.addSession(value);
      });
      await data.loadLocations();
      await GradeManager().init();
      return true;
    } catch (e) {
      return false;
    }
  }

  void _loadSessions(var sessions) {
    for (var session in sessions) {
      sessionMap[session[DBController.sessionId]] = Session(
        DateTime.parse(session[DBController.sessionTimeStart]),
        DateTime.parse(session[DBController.sessionTimeEnd]),
        session[DBController.sessionLocation],
        session[DBController.sessionOutdoor] == 1,
        0,
      );
      sessionMap[session[DBController.sessionId]].sessionId = session[DBController.sessionId];
    }
  }

  void _loadCoordinates(DBController dbController, var locations) async {
    if (locations == null) {
      return; //For backwards compatibility
    }
    for (var location in locations) {
      dbController.insert(DBController.locationsTable, location);
    }
  }

  void loadBoulder(var boulders) {
    for (var ascend in boulders) {
      Session session = sessionMap[ascend[DBController.sessionId]];
      session.boulderCount += 1;
      DBController().insert(DBController.boulderTable, ascend);
    }
  }
}

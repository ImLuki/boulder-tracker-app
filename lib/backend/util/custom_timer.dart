import 'dart:async';

import 'package:boulder_track/backend/model/data.dart';

class CustomTimer {
  String hoursStr = '00';
  String minutesStr = '00';
  String secondsStr = '00';
  Stream<int> _timerStream;
  var _timerSubscription;

  void startTimer(Function() callBack) {
    int seconds = Data().getCurrentSession().getSecondsSinceStart();
    hoursStr = ((seconds / (60 * 60)) % 60).floor().toString().padLeft(2, '0');
    minutesStr = ((seconds / 60) % 60).floor().toString().padLeft(2, '0');
    secondsStr = (seconds % 60).floor().toString().padLeft(2, '0');
    _timerStream = _stopWatchStream(seconds);
    _timerSubscription = _timerStream.listen((int newTick) {
      hoursStr = ((newTick / (60 * 60)) % 60).floor().toString().padLeft(2, '0');
      minutesStr = ((newTick / 60) % 60).floor().toString().padLeft(2, '0');
      secondsStr = (newTick % 60).floor().toString().padLeft(2, '0');
      callBack();
    });
  }

  void stopTimer() {
    if (_timerSubscription != null) {
      _timerSubscription.cancel();
      _timerStream = null;
    }
  }

  Stream<int> _stopWatchStream(int startTime) {
    StreamController<int> streamController;
    Timer timer;
    Duration timerInterval = Duration(seconds: 1);
    int counter = startTime;

    void stopTimer() {
      if (timer != null) {
        timer.cancel();
        timer = null;
        counter = 0;
        streamController.close();
      }
    }

    void tick(_) {
      counter++;
      streamController.add(counter);
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController.stream;
  }
}

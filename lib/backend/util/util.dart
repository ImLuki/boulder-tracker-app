import 'dart:io';

import 'package:boulder_track/backend/model/boulder.dart';
import 'package:boulder_track/backend/util/grade_mapper/grade_manager.dart';
import 'package:boulder_track/backend/util/style_mapper.dart';
import 'package:boulder_track/resources/Constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Util {
  static const String NULL_REPLACEMENT = "\u{200B}";

  static String getBoulderSummary(Boulder boulder) {
    return getSummary(boulder.gradeID, boulder.styleID, boulder.gradeSystem);
  }

  static String getSummary(int gradeID, int styleID, String gradingSystem) {
    return "${getGrade(gradeID, gradingSystem)} | ${getStyle(styleID)}";
  }

  static String getStyle(int styleID) => StyleMapper().getStyleFromId(styleID);

  static String getGrade(int gradeID, String gradingSystem) =>
      GradeManager().getGradeMapper().getGradeFromId(gradeID, gradingSystem: gradingSystem);

  static Widget getRating(int rating) {
    List<Widget> stars = [];
    for (int i = 0; i < 5; i++) {
      if (i < rating) {
        stars.add(Constants.FULL_STAR);
      } else {
        stars.add(Constants.EMPTY_STAR);
      }
    }
    return Row(children: stars);
  }

  static String getComment(String comment) {
    if (comment == null || comment == "") {
      return "-";
    } else {
      return comment;
    }
  }

  static String getBoulderName(String name) {
    return '${name == "" ? "Name Missing" : name.replaceAll("", NULL_REPLACEMENT)}';
  }

  static String getNameForFiltering(String name) {
    return name == "" ? "Name Missing" : name;
  }

  static String getLocationDateString(String date, String location) {
    return "${DateFormat("dd.MM.yy").format(DateTime.parse(date))} | $location".replaceAll("", NULL_REPLACEMENT);
  }

  static String getLocation(String location) {
    return location.replaceAll("", NULL_REPLACEMENT);
  }

  static Directory findRoot(FileSystemEntity entity) {
    final Directory parent = entity.parent;
    if (parent.path == entity.path) return parent;
    return findRoot(parent);
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:boulder_track/backend/database/database_controller.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class JsonCreator {
  Future<String> _createJsonString() async {
    List<Map<String, dynamic>> sessionList = await _loadTable(table: DBController.sessionTable);
    List<Map<String, dynamic>> boulderList = await _loadTable(table: DBController.boulderTable);
    List<Map<String, dynamic>> positionList = await _loadTable(table: DBController.locationsTable);
    return jsonEncode({"boulder": boulderList, "sessions": sessionList, "positions": positionList});
  }

  Future<List<Map<String, dynamic>>> _loadTable({String table}) async {
    final DBController dbController = DBController();
    List<Map<String, dynamic>> elements = await dbController.queryAllRows(table);
    return elements;
  }

  Future<String> createBackupFile() async {
    final directory = (await getTemporaryDirectory()).path;

    // get current datetime string
    DateFormat dateFormat = DateFormat("yyyy-MM-dd");
    String currentDateTime = dateFormat.format(DateTime.now());

    // create backup file
    String filePath = '$directory/boulder-backup-$currentDateTime.json';
    File file = new File(filePath);
    String jsonString = await _createJsonString();
    file.writeAsString(jsonString);
    return filePath;
  }

  Future<void> saveBackupFile(String directory, bool deleteBackups) async {
    if (deleteBackups) {
      List<FileSystemEntity> files = Directory(directory).listSync();
      files.sort((a, b) => b.path.compareTo(a.path));
      if (files.length > 4) {
        for (FileSystemEntity toDelete in files.getRange(4, files.length))
          try {
            File(toDelete.path).deleteSync();
          } on Exception {}
      }
    }

    Directory(directory).createSync();

    String filePath;
    var format = DateFormat("yyyy-MM-dd");
    String date = format.format(DateTime.now());
    filePath = "$directory/$date-boulder-tracker.json";
    File file = new File(filePath);
    String jsonString = await _createJsonString();
    file.writeAsString(jsonString);
  }
}

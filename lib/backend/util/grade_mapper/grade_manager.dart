import 'package:boulder_track/backend/util/grade_mapper/abstract_mapper.dart';
import 'package:boulder_track/backend/util/grade_mapper/boulder_grade_mapper.dart';

class GradeManager {
  GradeManager._privateConstructor();

  static final GradeManager _instance = GradeManager._privateConstructor();

  factory GradeManager() {
    return _instance;
  }

  AbstractGradeMapper boulderMapper;

  Future<void> init() async {
    this.boulderMapper = BoulderGradeMapper();
    await this.boulderMapper.init();
  }

  AbstractGradeMapper getGradeMapper() {
    return this.boulderMapper;
  }
}

import 'package:boulder_track/backend/util/grade_mapper/abstract_mapper.dart';

class BoulderGradeMapper extends AbstractGradeMapper {
  @override
  String getFile() {
    return "boulder_system.json";
  }

  @override
  List<String> getGradeSystems() {
    return ["Fontainebleau", "V-Scale"];
  }

  @override
  String getGradingSystemKey() {
    return "boulder_grading_system";
  }

  @override
  String getStandardGradeSystem() {
    return "Fontainebleau";
  }

  @override
  String lowestValueKey() {
    return "10000";
  }
}

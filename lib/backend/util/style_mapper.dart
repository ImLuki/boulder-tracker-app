class StyleMapper {
  StyleMapper._privateConstructor();

  static final StyleMapper _instance = StyleMapper._privateConstructor();

  factory StyleMapper() {
    return _instance;
  }

  Map<int, String> mapping = {
    0: 'Flash',
    1: 'Top',
    2: 'Project',
  };

  String getStyleFromId(int id) {
    return mapping[id];
  }

  int getDefaultBoulderStyle() {
    return 2;
  }

  List<_StyleMapping> getBoulderMapping() {
    return mapping.entries.map((e) => _StyleMapping(e.key, e.value)).toList();
  }
}

class _StyleMapping {
  final int id;
  final String style;

  _StyleMapping(this.id, this.style);
}

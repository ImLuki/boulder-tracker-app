import 'package:flutter/material.dart';
import 'package:boulder_track/app/boulder_track_app.dart';
import 'package:flutter/services.dart';
import 'app/app_theme.dart';
import 'init.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Init.initialize();
  runApp(InitializationApp());
}


class InitializationApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Boulder Tracker',
      theme: AppThemeDataFactory.prepareThemeData(),
      home: BoulderTrackApp(),
    );
  }
}

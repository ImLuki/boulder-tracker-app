import 'package:flutter/material.dart';
import 'package:boulder_track/app/app_colors.dart';

class AppThemeDataFactory {
  static ThemeData prepareThemeData() => ThemeData(
        brightness: Brightness.light,
        primaryColor: AppColors.primary,
        backgroundColor: AppColors.background,
        //fontFamily: "Roboto",
        textTheme: TextTheme(
          headline6: TextStyle(
            fontSize: 72,
            fontWeight: FontWeight.w500,
            color: AppColors.textPrimary,
          ),
          subtitle2: TextStyle(
            fontSize: 28,
            fontWeight: FontWeight.w600,
            color: AppColors.textPrimary,
          ),
          subtitle1: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.w600,
            color: AppColors.textPrimary,
          ),
          bodyText2: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: AppColors.textPrimary,
          ),
          headline3: TextStyle(
            fontSize: 36,
            color: AppColors.textSecondary,
          ),
          headline4: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: AppColors.textSecondary,
          ),
        ),
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: AppColors.textPrimary),
      );
}

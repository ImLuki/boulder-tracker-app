import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color primary = Colors.white;
  static const Color background = Colors.white;

  static const Color textPrimary = Colors.black;
  static const Color textSecondary = Colors.white;

  //static const Color accentColor = Color(0xFF012A5C);
  //static const Color accentColor = Color(0xFF4f6d7a);
  static const Color accentColor = Color(0xFF053c5e);
  static const Color accentColor2 = Color(0xFF878C8F);
  static const Color accentColor3 = Color(0xFF2A4849);

  static const Color color3 = Color(0xFF2E7647);
  static const Color color2 = Color(0xFF3D5A80);
  static const Color color1 = Color(0xFFE28413);

  static const List<Color> chart_colors = [
    Color(0xFFa62546),
    Color(0xFFf26522),
    Color(0xFF47acb1),
    Color(0xFFffcd34),
    Color(0xFFffe8af),
    Color(0xFFadd5d7),
    Color(0xFFa5a8aa),
    Color(0xFFf9aa7b)
  ];
}

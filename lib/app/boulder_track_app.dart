import 'package:boulder_track/ui/pages/overview/overview_data_loader.dart';
import 'package:boulder_track/ui/pages/statistics/chart_page.dart';
import 'package:boulder_track/ui/pages/ascends/ascends_pages.dart';
import 'package:boulder_track/ui/pages/session/session_page.dart';
import 'package:boulder_track/ui/pages/settings/settings_page.dart';
import 'package:boulder_track/util/notifications_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

class BoulderTrackApp extends StatefulWidget {
  @override
  _BoulderTrackAppState createState() => _BoulderTrackAppState();
}

class _BoulderTrackAppState extends State<BoulderTrackApp> with SingleTickerProviderStateMixin {
  static const int MAIN_PAGE_INDEX = 2;
  int _selectedIndex = MAIN_PAGE_INDEX;
  bool canBack = false;

  @override
  Widget build(BuildContext context) {
    NotificationHandler().init(context);
    return KeyboardVisibilityProvider(
        child: Scaffold(
      body: createBody(),
      bottomNavigationBar: createBottomNavigationBar(),
    ));
  }

  void _onItemTapped(int index) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget createBody() {
    return Stack(
      children: <Widget>[
        Positioned.fill(
          // child: Container(
          //  color: Colors.blueGrey,
          //),
          child: Image(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.fill,
          ),
        ),
        [OverviewDataLoader(), AscendsPage(), SessionPage(), ChartPage(), SettingsPage()][_selectedIndex],
      ],
    );
  }

  Widget createBottomNavigationBar() {
    return Container(
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black12,
              spreadRadius: 1.0,
              blurRadius: 10,
            ),
          ],
        ),
        child: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.person_search),
              label: 'Overview',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list_alt),
              label: 'Ascends',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.adjust),
              label: 'Session',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.bar_chart),
              label: 'Statistics',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          unselectedItemColor: Colors.black45,
          onTap: _onItemTapped,
          showUnselectedLabels: false,
          unselectedFontSize: 14.0,
        ));
  }
}

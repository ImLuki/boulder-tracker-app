# Boulder Tracker

This app lets you log all of your bouldering activities. Enjoy to use it!

## Features

- Log boulder sessions
Log all of your climbing activities. Easily save your ascends in this app. Specify the boulder grade, ascending style, name, and give it a rating. Clear statistics are created from the data, so you always have an optimal overview of your progress.

- Session summaries
After each session, a summary with the most important key points is created to give you a simple overview of your performance. You can easily share your summary directly with your friends.

- Find boulder you've already climbed
Who does not know it, you are bouldering and wondering whether you have already climbed this boulder? An overview of all your climbed boulder promises help.

- Statistics and Graphics
View your previous successes in clear graphics. Compare yourself to friends. See your progress in great charts and see your most difficult boulder at a glance.

- Data protection
Your data is only stored locally, so your data cannot fall into the wrong hands. Of course, you can still create a backup and save it in your favorite cloud.